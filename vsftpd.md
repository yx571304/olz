# 本地用户模式

## 服务端配置

> 创建用户 && 配置密码

```
useradd -d /app/fshh fshh
echo 'admin:admin' | chpasswd
```

> 创建配置文件

```
cat <<EOF  > /etc/vsftpd/vsftpd.conf
anonymous_enable=NO
local_enable=YES
write_enable=YES
local_umask=022
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
xferlog_std_format=YES
listen=NO
listen_ipv6=YES
pam_service_name=vsftpd
userlist_enable=YES
# tcp_wrappers=YES
EOF
```

> 启动服务 && 跟随系统启动

```
systemctl restart vsftpd
systemctl enable vsftpd
```

## 客户端

```
yum install vsftpd
ftp admin@192.168.2.73
```