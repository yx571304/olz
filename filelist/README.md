# filelist

filelist.cn 简易的企业文件管理(网盘)服务

# 快速上手

[https://xabcloud.com/#/filelist](https://xabcloud.com/#/filelist)

# 构建镜像

    docker build -t filelist:latest .

# 启动服务

    docker-compose up -d

# 验证

    http://0.0.0.0:9000    admin/123456

# 使用已经构建好的镜像

    bookgh/filelist:latest 
