
0. 构建镜像

```
docker build -t pxe:latest .
```

1. 挂载数据 

```
mkdir /cd
mount CentOS-7-x86_64-Minimal-2003.iso /cd
```

2. 修改参数 DHCP地址段/宿主机IP

```
environment:
  IP: 192.168.15.73

command: "--dhcp-range=192.168.15.20,192.168.15.60,255.255.255.0,24h"
```

3. 启动容器

```
docker-compose up -d
```
