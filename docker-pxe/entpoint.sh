#!/bin/sh

# nginx pid
[ -d '/run/nginx' ] || mkdir /run/nginx

# start nginx
/usr/sbin/nginx

# dnsmasq gateway
default="$(echo $IP | cut -d '.' -f 1-3).1"
gateway=${GATEWAY:-$default}
sed -i "s#dhcp-option=3.*#dhcp-option=3,${gateway}#" /etc/dnsmasq.conf 
sed -i "s#192.168.0.100#$IP#g" /var/lib/tftpboot/pxelinux.cfg/default 

# nfs
sub="$(echo $IP | cut -d '.' -f 1-3).0"
echo "/var/lib/tftpboot/centos $sub/24(rw,no_root_squash)" > /etc/exports
#  yum groups -y install "Server with GUI" --releasever=7 --installroot=/var/lib/tftpboot/centos

# ks
cat /etc/centos.ks | sed "s#URL#http://$IP/centos#" > /var/lib/tftpboot/local-mini.ks
cat /etc/centos.ks | sed "s#URL#https://mirrors.aliyun.com/centos/7.8.2003/os/x86_64#" > /var/lib/tftpboot/aliyun-7mini.ks
cat /etc/centos.ks | sed "s#URL#https://mirrors.aliyun.com/centos/8.2.2004/os/x86_64#" > /var/lib/tftpboot/aliyun-8mini.ks


# start dnsmasq
exec dnsmasq --no-daemon "$@"
