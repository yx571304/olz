# create new
install

# automatically proceed for each steps
autostep

# reboot after installing
reboot

# encrypt algorithm
auth --enableshadow --passalgo=sha512

# Root password (redhat)
rootpw --iscrypted $6$82awXtn/U.QPjSoN$nY72AC7LRuuchkP2ZrpJbDGRKJRkQnUQGFEBXDYY7W1e6AD2GTe1QJPKeWdZmw/twf1PUoraAqoMhUtRwWCxb1

# installation source
url --url=URL

# Run the Setup Agent on first boot
firstboot --enable

# install disk
ignoredisk --only-use=sda

# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
autopart --type=lvm

# Partition clearing information
clearpart --none --initlabel

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang en_US.UTF-8

# Network information
# network  --bootproto=static --device=eth0 --gateway=192.168.0.1 --ip=192.168.0.70 --nameserver=223.5.5.5,3.3.3.3 --netmask=255.255.255.0 --ipv6=auto --activate
network --bootproto=dhcp --activate --hostname=localhost

# selinux
selinux --disabled

# System timezone
timezone Asia/Shanghai --isUtc --nontp

# soft package
%packages
@^minimal
@core

%end

%post
# 系统安装后执行的脚本

# 禁用防火墙
systemctl stop firewalld
systemctl disable firewalld

# 优化 ssh 连接慢
sed -i "s/#UseDNS yes/UseDNS no/" /etc/ssh/sshd_config
sed -i "s/GSSAPIAuthentication .*/GSSAPIAuthentication no/" /etc/ssh/sshd_config

# 禁用IPv6
echo '' >> /etc/sysctl.conf
echo '# 禁用 IPv6' >> /etc/sysctl.conf
echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> /etc/sysctl.conf
echo 'net.ipv6.conf.default.disable_ipv6 = 1' >> /etc/sysctl.conf
echo 'net.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.conf
sysctl -p > /dev/null 2>&1

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
