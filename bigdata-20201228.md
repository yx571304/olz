# 大数据集群部署

系统版本: CentOS-8.1 Mini

服务器IP(3台): 10.8.10.61-63

## 创建配置文件

```
cat > /tmp/.env <<'EOF'
# IP与主机名对应
SERVERS=(10.8.10.61 10.8.10.62 10.8.10.63)
HOSTS=(node01 node02 node03)

# 软件存放路径 && 安装路径
PACKAGE_DIR=/home/software
SOFT_INSTALL_DIR=/home/hadoop

# 配置主机名变量 IP 为变量值
let SER_LEN=${#SERVERS[@]}-1
for ((i=0;i<=$SER_LEN;i++)); do
    export ${HOSTS[i]}=${SERVERS[i]}
done

# 获取当前主机IP
NET_NAME=ens192
function get_lan_ip () {
    host_if=$(ip route | grep default | cut -d' ' -f5)
    NET_NAME=${NET_NAME:-$host_if}
    ip addr | grep "$NET_NAME$" | awk '{print $2}' | cut -d'/' -f1
}
LOCAL_IP=$(get_lan_ip)

# zookeeper 安装节点
ZOO_SERVER='node01 node02 node03'

# kafka 安装节点
KAFKA_SERVER='node01 node02 node03'

# hadoop 安装节点
NameNode='node01'
DataNode='node02 node03'

# hbase 安装节点
HBASE_MASTER='node01'
HBASE_SLAVE='node02 node03'

# opentsdb 安装节点
TSDB_SERVER='node02 node03'

# systemctl 写入 rc.local
function add_rclocal () {
    local name=$1
    grep -qr "# start ${name}" /etc/rc.local || echo -e "\n# start ${name}\nsystemctl start ${name}" >> /etc/rc.local
}

# version
ZOOKEEPER_VER=3.6.2
EOF
```

## 初始化系统

### 1. 读取环境变量

```
source /tmp/.env
```

### 2. 初始化系统

```
# 关闭禁用 selinux
setenforce 0 &> /dev/null
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

# 设置时区为: Asia/Shanghai
timedatectl set-timezone Asia/Shanghai

# 设置主机名
let SER_LEN=${#SERVERS[@]}-1
for ((i=0;i<=$SER_LEN;i++)); do
    if [ "${SERVERS[i]}" == "$LOCAL_IP" ]; then
        hostnamectl set-hostname ${HOSTS[i]}
        echo "设置本机主机名为: ${HOSTS[i]}"
    fi
done

# 配置 hosts 解析
if [ "$(echo ${SERVERS[@]} | grep $LOCAL_IP)" ]; then
    sed -i '3,$d' /etc/hosts
    echo -e "\n# hadoop" >> /etc/hosts
    let SER_LEN=${#SERVERS[@]}-1
    for ((i=0;i<=$SER_LEN;i++)); do
        echo "${SERVERS[i]}  ${HOSTS[i]}" >> /etc/hosts
    done
    cat /etc/hosts
fi

# 文件/进程 限制
cat > /etc/security/limits.conf <<EOF
# mango
* soft nofile 65535
* hard nofile 65535
* soft nproc 65535
* hard nproc 65535
* soft  memlock  unlimited
* hard memlock  unlimited
EOF

# 内核参数
cat > /etc/sysctl.conf  <<EOF
vm.swappiness = 70
net.ipv4.neigh.default.gc_stale_time = 120
net.ipv4.conf.all.rp_filter = 0
net.ipv4.conf.default.rp_filter = 0
net.ipv4.conf.default.arp_announce = 2
net.ipv4.conf.lo.arp_announce = 2
net.ipv4.conf.all.arp_announce = 2
net.ipv4.tcp_max_tw_buckets = 5000
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_rmem = 8192 262144 4096000
net.ipv4.tcp_wmem = 4096 262144 4096000
net.ipv4.tcp_tw_reuse = 1
net.ipv4.ip_local_port_range = 1025 65535
net.ipv4.tcp_max_syn_backlog = 100000
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_max_tw_buckets = 5000
kernel.msgmnb = 65536
kernel.msgmax = 65536
kernel.sysrq = 1
net.ipv4.icmp_echo_ignore_all = 0
net.ipv4.tcp_max_orphans = 3276800
fs.file-max = 800000
net.core.somaxconn=32768
net.core.rmem_default = 12697600
net.core.wmem_default = 12697600
net.core.rmem_max = 873800000
net.core.wmem_max = 655360000

net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
EOF

# 立即生效
modprobe br_netfilter
sysctl -p
```

## 3. 安装 JDK

```
# 解压
mkdir -p /usr/java/ $SOFT_INSTALL_DIR && cd $PACKAGE_DIR
tar zxf jdk-8u211-linux-x64.tar.gz -C /usr/java/

# 配置环境变量
config=/etc/profile.d/jdk.sh
echo '#!/bin/bash' > $config
echo 'export JAVA_HOME=/usr/java/jdk1.8.0_211' >> $config
echo 'export JRE_HOME=${JAVA_HOME}/jre' >> $config
echo 'export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH' >> $config
echo 'export PATH=$JAVA_HOME/bin:$PATH' >> $config
chmod +x $config

# 验证
source $config
which java
```

##  4. 安装 zookeeper

```
#下载解压
ZOOKEEPER_VER=3.6.2
mkdir -p $SOFT_INSTALL_DIR
cd $PACKAGE_DIR
curl -O https://opentuna.cn/apache/zookeeper/zookeeper-3.6.2/apache-zookeeper-3.6.2-bin.tar.gz
tar zxf apache-zookeeper-${ZOOKEEPER_VER}-bin.tar.gz -C $SOFT_INSTALL_DIR

# 创建配置文件
mkdir -p ${SOFT_INSTALL_DIR}/apache-zookeeper-${ZOOKEEPER_VER}-bin/{logs,data}
config=${SOFT_INSTALL_DIR}/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf/zoo.cfg
echo 'tickTime=2000' > $config
echo 'initLimit=10' >> $config
echo 'syncLimit=5' >> $config
echo "dataDir=$SOFT_INSTALL_DIR/apache-zookeeper-${ZOOKEEPER_VER}-bin/data" >> $config
echo "dataLogDir=$SOFT_INSTALL_DIR/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs" >> $config
echo 'clientPort=2181' >> $config
echo 'autopurge.purgeInterval=5' >> $config
echo 'autopurge.snapRetainCount=20' >> $config
count=1
for node in $ZOO_SERVER; do
    echo "server.$count=$node:2888:3888"  >> $config
    [ "$node" == "`hostname`" ] && echo "$count" > ${SOFT_INSTALL_DIR}/apache-zookeeper-${ZOOKEEPER_VER}-bin/data/myid
    let count++
done
echo '4lw.commands.whitelist=*' >> $config
echo 'admin.serverPort=3080' >> $config

# 配置服务
cat > /usr/lib/systemd/system/zookeeper.service <<EOF
[Unit]
Description=zookeeper
After=network.target

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=$SOFT_INSTALL_DIR/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start
ExecStop=$SOFT_INSTALL_DIR/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh stop

[Install]
WantedBy=multi-user.target
EOF

# 添加到 rc.local
add_rclocal zookeeper
```

## 安装 hadoop

### 解压 && 配置

```
HADOOP_VER=2.9.2
mkdir -p $SOFT_INSTALL_DIR
cd $PACKAGE_DIR
tar zxf hadoop-${HADOOP_VER}.tar.gz -C ${SOFT_INSTALL_DIR}
mkdir -p ${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/{logs,tmp,name,data,journal}

# 配置环境变量
config=/etc/profile.d/hadoop.sh
echo '#!/bin/bash' > $config
echo "export HADOOP_HOME=$SOFT_INSTALL_DIR/hadoop-${HADOOP_VER}" >> $config
echo 'export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin' >> $config

# 读取环境变量
chmod +x $config
source $config

# 创建 core-site.xml
cat <<EOF > ${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/etc/hadoop/core-site.xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://${NameNode}:9000</value>
    </property>
    <property>
        <name>io.file.buffer.size</name>
        <value>13107200</value>
    </property>
    <property>
        <name>hadoop.tmp.dir</name>
        <value>file:${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/tmp</value>
    </property>
</configuration>
EOF


 # 创建 hdfs-site.xml
DATANODE="$(for i in $DataNode ; do echo $i:8485; done)"
DATANODE="$(echo $DATANODE | sed 's# #;#g')"
cat <<EOF > ${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/etc/hadoop/hdfs-site.xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<configuration>
    <property>
        <name>dfs.namenode.secondary.http-address</name>
        <value>${NameNode}:50090</value>
    </property>
    <property>
        <name>dfs.replication</name>
        <value>2</value>
    </property>
    <property>
        <name>dfs.namenode.name.dir</name>
        <value>file:${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/name</value>
    </property>
    <property>
        <name>dfs.datanode.data.dir</name>
        <value>file:${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/data</value>
    </property>
</configuration>
EOF

# 修改yarn-site.xml配置文件
cat <<EOF > ${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/etc/hadoop/yarn-site.xml
<?xml version="1.0"?>

<configuration>
    <property>
        <name>yarn.nodemanager.aux-services</name>
        <value>mapreduce_shuffle</value>
    </property>
    <property>
        <name>yarn.resourcemanager.address</name>
        <value>${NameNode}:8032</value>
    </property>
    <property>
        <name>yarn.resourcemanager.scheduler.address</name>
        <value>${NameNode}:8030</value>
    </property>
    <property>
        <name>yarn.resourcemanager.resource-tracker.address</name>
        <value>${NameNode}:8031</value>
    </property>
    <property>
        <name>yarn.resourcemanager.admin.address</name>
        <value>${NameNode}:8033</value>
    </property>
    <property>
        <name>yarn.resourcemanager.webapp.address</name>
        <value>${NameNode}:8088</value>
    </property>
</configuration>
EOF

# 创建 mapred-site.xml
cat <<EOF > ${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/etc/hadoop/mapred-site.xml
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<configuration>
    <property>
        <name>mapreduce.framework.name</name>
        <value>yarn</value>
    </property>
    <property>
        <name>mapreduce.jobhistory.address</name>
        <value>${NameNode}:10020</value>
    </property>
    <property>
        <name>mapreduce.jobhistory.address</name>
        <value>${NameNode}:19888</value>
    </property>
</configuration>
EOF
```


### hadoop NameNode 服务配置 (node01)

```
cat > /usr/lib/systemd/system/hdp-namenode.service  <<EOF
[Unit]
Description=hadoop namenode
After=syslog.target network.target

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
User=root
Group=root
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/hadoop-daemon.sh start namenode
ExecStop=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/hadoop-daemon.sh stop namenode

[Install]
WantedBy=multi-user.target
EOF

add_rclocal hdp-namenode

cat > /usr/lib/systemd/system/hdp-resourcemanager.service <<EOF
[Unit]
Description=hadoop resourcemanager
After=syslog.target network.target
Wants=hdp-namenode.service

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
User=root
Group=root
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/yarn-daemon.sh start resourcemanager
ExecStop=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/yarn-daemon.sh stop resourcemanager

[Install]
WantedBy=multi-user.target
EOF

add_rclocal hdp-resourcemanager

cat > /usr/lib/systemd/system/hdp-secondarynamenode.service <<EOF
[Unit]
Description=hadoop secondarynamenode
After=syslog.target network.target
Wants=hdp-namenode.service

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
User=root
Group=root
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/hadoop-daemon.sh start secondarynamenode
ExecStop=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/hadoop-daemon.sh stop secondarynamenode

[Install]
WantedBy=multi-user.target
EOF

add_rclocal hdp-secondarynamenode
```

### hadoo DataNode 服务配置 (node02 node03)

```
cat > /usr/lib/systemd/system/hdp-datanode.service <<EOF
[Unit]
Description=hadoop datanode
After=syslog.target network.target

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
User=root
Group=root
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/hadoop-daemon.sh start datanode
ExecStop=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/hadoop-daemon.sh stop datanode

[Install]
WantedBy=multi-user.target
EOF

add_rclocal hdp-datanode

cat > /usr/lib/systemd/system/hdp-nodemanager.service <<EOF
[Unit]
Description=hadoop nodemanager
After=syslog.target network.target
Wants=hdp-datanode.service

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
User=root
Group=root
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/yarn-daemon.sh start nodemanager
ExecStop=${SOFT_INSTALL_DIR}/hadoop-${HADOOP_VER}/sbin/yarn-daemon.sh stop nodemanager

[Install]
WantedBy=multi-user.target
EOF

add_rclocal hdp-nodemanager
```

### 初始化 NameNode (node01)

```
source /etc/profile.d/hadoop.sh
hdfs namenode -format -force
```

### 启动 NameNode (node01)

```
systemctl daemon-reload
systemctl start hdp-namenode
systemctl start hdp-resourcemanager
systemctl start hdp-secondarynamenode
```

### 启动 DataNode (node02 node03)

```
systemctl daemon-reload
systemctl start hdp-datanode
systemctl start hdp-nodemanager
```

## 安装 Hbase

### 解压

```
HBASE_VER=1.6.0
mkdir -p $SOFT_INSTALL_DIR
cd $PACKAGE_DIR
tar zxf hbase-${HBASE_VER}-bin.tar.gz -C ${SOFT_INSTALL_DIR}/
```

### 创建配置文件

```
sed -i 's/# export HBASE_MANAGES_ZK=true/export HBASE_MANAGES_ZK=false/' ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf/hbase-env.sh
ZOOK_SERVER_LIST="$(echo $ZOO_SERVER | sed 's/ /,/g')"
cat <<EOF    > ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf/hbase-site.xml
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<configuration>
<property>
    <name>hbase.rootdir</name>
    <value>hdfs://${NameNode}:9000/hbase</value>
</property>
<property>
    <name>hbase.cluster.distributed</name>
    <value>true</value>
</property>
<property>
    <name>hbase.zookeeper.quorum</name>
    <value>${ZOOK_SERVER_LIST}</value>
</property>
<property>
    <name>hbase.zookeeper.property.clientPort</name>
    <value>2181</value>
</property>
<property>
    <name>hbase.zookeeper.property.dataDir</name>
    <value>${SOFT_INSTALL_DIR}/apache-zookeeper-${ZOOKEEPER_VER}-bin/data</value>
</property>
<property>
    <name>hbase.tmp.dir</name>
    <value>${SOFT_INSTALL_DIR}/apache-zookeeper-${ZOOKEEPER_VER}-bin/tmp</value>
</property>
<property>
    <name>hbase.client.scanner.timeout.period</name>
    <value>180000</value>
</property>
<property>
    <name>zookeeper.session.timeout</name>
    <value>120000</value>
</property>
<property>
    <name>hbase.rpc.timeout</name>
    <value>300000</value>
</property>
<property>
    <name>hbase.hregion.majorcompaction</name>
    <value>0</value>
</property>
<property>
    <name>hbase.regionserver.thread.compaction.large</name>
    <value>5</value>
</property>
<property>
    <name>hbase.regionserver.thread.compaction.small</name>
    <value>5</value>
</property>
<property>
    <name>hbase.regionserver.thread.compaction.throttle</name>
    <value>10737418240</value>
</property>
<property>
    <name>hbase.regionserver.regionSplitLimit</name>
    <value>150</value>
</property>
<property>
    <name>hfile.block.cache.size</name>
    <value>0</value>
</property>

<property>
    <name>hbase.unsafe.stream.capability.enforce</name>
    <value>false</value>
</property>
</configuration>
EOF

mkdir -p ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/{data,logs,tmp}
rm -f ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf/regionservers
for node in $HBASE_SLAVE; do echo "$node" >> ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf/regionservers ;done

# 添加环境变量
config=/etc/profile.d/hbase.sh
echo '#!/bin/bash' > $config
echo "export HBASE_HOME=${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}" >> $config
echo 'export PATH=$HBASE_HOME/bin:$PATH' >> $config

# 读取环境变量
chmod +x $config
source $config
```

### 配置 Master 服务 (node01)

```
grep -qr '/etc/profile' ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh || sed -ie '2a\. /etc/profile\n' ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh
cat > /usr/lib/systemd/system/hbase-master.service <<EOF
[Unit]
Description=hbase
After=network.target
Wants=zookeeper.service hdp-namenode.service hdp-resourcemanager.service

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh --config ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf start master
ExecStop=${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh --config ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf stop master

[Install]
WantedBy=multi-user.target
EOF

add_rclocal hbase-master
```

### 配置 Regionserver 服务 (node02 node03)

```
grep -qr '/etc/profile' ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh || sed -ie '2a\. /etc/profile\n' ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh
cat > /usr/lib/systemd/system/hbase-regionserver.service <<EOF
[Unit]
Description=hbase
After=network.target
Wants=zookeeper.service hdp-datanode.service hdp-nodemanager.service

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStartPre=/usr/bin/rm -f /tmp/hbase-root-master.pid
ExecStart=${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh --config ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf start regionserver
ExecStop=${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/bin/hbase-daemon.sh --config ${SOFT_INSTALL_DIR}/hbase-${HBASE_VER}/conf stop regionserver

[Install]
WantedBy=multi-user.target
EOF

add_rclocal hbase-regionserver

# 启动服务
systemctl start hbase-master
systemctl start hbase-regionserver
```

## 安装opentsdb （node02 node03）

### 1. 安装

```
yum install opentsdb-2.4.0.noarch
```

### 创建配置文件

```
ZOOK_SERVER_LIST="$(echo $ZOO_SERVER | sed 's/ /,/g')"
confile=/etc/opentsdb/opentsdb.conf
echo 'tsd.core.preload_uid_cache = true' > $confile
echo 'tsd.core.auto_create_metrics = true' >> $confile
echo 'tsd.storage.enable_appends = true' >> $confile
echo 'tsd.core.enable_ui = true' >> $confile
echo 'tsd.core.enable_api = true' >> $confile
echo 'tsd.network.port = 14242' >> $confile
echo 'tsd.http.staticroot = /usr/share/opentsdb/static' >> $confile
echo "tsd.http.cachedir = ${SOFT_INSTALL_DIR}/opentsdb/tmp" >> $confile
echo 'tsd.http.request.enable_chunked = true' >> $confile
echo 'tsd.http.request.max_chunk = 65535' >> $confile
echo "tsd.storage.hbase.zk_quorum = ${ZOOK_SERVER_LIST}" >> $confile
echo 'tsd.query.timeout = 0' >> $confile
echo 'tsd.query.filter.expansion_limit = 65535' >> $confile
echo 'tsd.network.keep_alive = true' >> $confile
echo 'tsd.network.backlog = 3072' >> $confile
echo 'tsd.storage.fix_duplicates=true' >> $confile
mkdir -p ${SOFT_INSTALL_DIR}/opentsdb/{data,logs,tmp}
chown -Rf opentsdb:opentsdb ${SOFT_INSTALL_DIR}/opentsdb
```

### 初始化数据库

```
cp /usr/share/opentsdb/tools/create_table.sh /tmp/create_table.sh
sed -i 's#\$TSDB_TTL#2147483647#' /tmp/create_table.sh
env COMPRESSION=SNAPPY HBASE_HOME=${SOFT_INSTALL_DIR}/hbase-${HBASE_VER} /tmp/create_table.sh
```

### 配置服务

```
grep -qr '/etc/profile' /usr/bin/tsdb || sed -ie '2a\. /etc/profile\n' /usr/bin/tsdb
cat > /usr/lib/systemd/system/opentsdb.service <<EOF
[Unit]
Description=OpenTSDB on port
After=network-online.target
Before=shutdown.target
Wants=zookeeper.service hbase-regionserver.service

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=simple
User=opentsdb
Group=opentsdb
LimitNOFILE=65535
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
Environment='JVMARGS=-Xmx6000m -DLOG_FILE=/var/log/opentsdb/%p_%i.log -DQUERY_LOG=/var/log/opentsdb/%p_%i_queries.log -XX:+ExitOnOutOfMemoryError -enableassertions -enablesystemassertions'
ExecStart=/usr/bin/tsdb tsd --config /etc/opentsdb/opentsdb.conf
StandardOutput=journal
EOF

add_rclocal opentsdb
```

### 测试

```
# 写入数据
curl -X POST -H "Content-Type: application/json" http://node02:14242/api/put -d '{"metric": "check.tsdb", "timestamp": 1606446000, "value": 1234567890, "tags": {"name": "opentsdb"}}'

# 读取数据
curl -s -X POST -H "Content-Type: application/json" http://node02:14242/api/query -d '{"start": 1606446000, "queries": [{"metric": "check.tsdb", "aggregator": "avg", "tags": {"name": "opentsdb"}}]}'
```

## kafka 安装

### 解压

```
KAFKA_VER=2.13-2.6.0
mkdir -p $SOFT_INSTALL_DIR
cd $PACKAGE_DIR
tar zxf kafka_${KAFKA_VER}.tgz -C ${SOFT_INSTALL_DIR}
mkdir -p ${SOFT_INSTALL_DIR}/kafka_${KAFKA_VER}/{data,logs,tmp}
```

### 配置

```
ID=$(echo $LOCAL_IP | awk -F '.' '{print $NF}')
ZOO_LIST="$(for i in $ZOO_SERVER; do echo $i:2181; done)"
ZOO_LIST="$(echo $ZOO_LIST | sed 's# #,#g')"
config=${SOFT_INSTALL_DIR}/kafka_${KAFKA_VER}/config/server.properties
echo "broker.id=$ID" > $config
echo "listeners=PLAINTEXT://${LOCAL_IP}:9092" >> $config
echo 'num.network.threads=3' >> $config
echo 'num.io.threads=8' >> $config
echo '#auto.create.topics.enable =true' >> $config
echo 'socket.send.buffer.bytes=102400' >> $config
echo 'socket.receive.buffer.bytes=102400' >> $config
echo 'socket.request.max.bytes=104857600' >> $config
echo "log.dirs=${SOFT_INSTALL_DIR}/kafka_${KAFKA_VER}/logs" >> $config
echo 'num.partitions=1' >> $config
echo 'num.recovery.threads.per.data.dir=1' >> $config
echo 'offsets.topic.replication.factor=1' >> $config
echo 'transaction.state.log.replication.factor=1' >> $config
echo 'transaction.state.log.min.isr=1' >> $config
echo 'log.retention.hours=168' >> $config
echo 'log.segment.bytes=1073741824' >> $config
echo 'log.retention.check.interval.ms=300000' >> $config
echo "zookeeper.connect=$ZOO_LIST" >> $config
echo 'zookeeper.connection.timeout.ms=60000' >> $config
echo 'group.initial.rebalance.delay.ms=0' >> $config

# 配置环境变量
config=/etc/profile.d/kafka.sh
echo '#!/bin/bash' > $config
echo "export KAFKA_HOME=${SOFT_INSTALL_DIR}/kafka_${KAFKA_VER}" >> $config
echo 'export PATH=$KAFKA_HOME/bin:$PATH' >> $config

# 读取环境变量
chmod +x $config
source $config
```

### 配置服务

```
cat > /usr/lib/systemd/system/kafka.service <<EOF
[Unit]
Description=kafka
After=network.target
Wants=zookeeper.service

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}"
ExecStart=${SOFT_INSTALL_DIR}/kafka_${KAFKA_VER}/bin/kafka-server-start.sh -daemon ${SOFT_INSTALL_DIR}/kafka_${KAFKA_VER}/config/server.properties
ExecStop=${SOFT_INSTALL_DIR}/kafka_${KAFKA_VER}/bin/kafka-server-stop.sh

[Install]
WantedBy=multi-user.target
EOF

add_rclocal kafka
systemctl start kafka
```

## 安装 Spark

### 下载解压

```
SPARK_VER=2.4.7
mkdir -p $SOFT_INSTALL_DIR
cd $PACKAGE_DIR
# curl -O https://mirrors.tuna.tsinghua.edu.cn/apache/spark/spark-2.4.7/spark-2.4.7-bin-without-hadoop.tgz
tar zxf spark-${SPARK_VER}-bin-without-hadoop.tgz -C ${SOFT_INSTALL_DIR}

echo "export SPARK_HOME=${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop" > /etc/profile.d/spark.sh
echo 'export PATH=$PATH:$SPARK_HOME/bin' >> /etc/profile.d/spark.sh
source /etc/profile.d/spark.sh
```

### 配置

```
cat <<EOF > ${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/conf/spark-env.sh
#!/bin/bash

export JAVA_HOME=$JAVA_HOME
export SPARK_MASTER_IP=10.8.10.62
export MASTER=spark://10.8.10.62
export SPARK_DIST_CLASSPATH=$(hadoop classpath)
EOF

chmod +x  ${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/conf/spark-env.sh
echo "$HOSTNAME" >  ${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/conf/slaves
cp ${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/conf/log4j.properties.template ${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/conf/log4j.properties
```

### 配置服务

```
# master
cat <<EOF  >/usr/lib/systemd/system/spark-master.service
[Unit]
Description=Spark Master
After=syslog.target network.target

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
User=root
Group=root
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME}" "SPARK_DIST_CLASSPATH=$(hadoop classpath)"
ExecStart=${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/sbin/spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $HOSTNAME --port 7077 --webui-port 8080
ExecStop=${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/sbin/stop-slave.sh

[Install]
WantedBy=multi-user.target
EOF

add_rclocal spark-master

# slave
cat <<EOF  >/usr/lib/systemd/system/spark-slave.service
[Unit]
Description=Spark Slave
After=syslog.target network.target

[Service]
LimitNOFILE=100000
LimitNPROC=100000
Restart=always
RestartSec=3
TimeoutSec=10
Type=forking
User=root
Group=root
Environment="JAVA_HOME=${JAVA_HOME}" "JRE_HOME=${JRE_HOME} "SPARK_DIST_CLASSPATH=$(hadoop classpath)""
ExecStart=${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/sbin/spark-daemon.sh start org.apache.spark.deploy.worker.Worker 1 --webui-port 8081 spark://$HOSTNAME:7077
ExecStop=${SOFT_INSTALL_DIR}/spark-${SPARK_VER}-bin-without-hadoop/sbin/stop-slave.sh

[Install]
WantedBy=multi-user.target
EOF

add_rclocal spark-slave
```

### 启动服务 && 跟随系统启动

```
systemctl daemon-reload
systemctl start spark-master
systemctl start spark-slave

systemctl status spark-master
systemctl status spark-slave

```

## 配置服务开机自启

### 创建服务启动关闭脚本

```
sed '14,$d' /etc/rc.local > /etc/init
echo -e "\n# hc $(date '+%F %T')\n" >> /etc/init
echo -e 'active=$1' >> /etc/init
echo -e 'systemctl daemon-reload\n' >> /etc/init
chmod +x /etc/init /etc/rc.local /etc/rc.d/rc.local

echo 'case $active in' >> /etc/init
echo '  start)' >> /etc/init
echo "$(grep -r systemctl /etc/rc.local | sed 's#start#$active#' | sed 's#^#    #')" >> /etc/init
echo '    ;;' >> /etc/init
echo '  stop)' >> /etc/init
echo "$(grep -r systemctl /etc/rc.local | sed 's#start#$active#' | sed 's#^#    #' | tac)" >> /etc/init
echo '    ;;' >> /etc/init
echo '  *)' >> /etc/init
echo '    echo "$0 {stop|start}"' >> /etc/init
echo '    ;;' >> /etc/init
echo 'esac' >> /etc/init
```

### 配置服务

```
cp /usr/lib/systemd/system/rc-local.service{,.bak}
cat > /usr/lib/systemd/system/rc-local.service <<EOF
#  SPDX-License-Identifier: LGPL-2.1+
#
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

# This unit gets pulled automatically into multi-user.target by
# systemd-rc-local-generator if /etc/init is executable.
[Unit]
Description=/etc/init Compatibility
Documentation=man:systemd-rc-local-generator(8)
ConditionFileIsExecutable=/etc/init
After=network.target

[Service]
Type=forking
ExecStart=/etc/init start
ExecStop=/etc/init stop
TimeoutSec=0
RemainAfterExit=yes
GuessMainPID=no

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable rc-local
```

## 安装 mysql

### 配置变量

```
# 安装包路径
PACKAGE_DIR=/home/software

# mysql
# 配置数据库目录
datadir=/home/mysql         # 数据库数据存储目录
basedir=/usr/local/mysql    # 数据库安装目录

# 添加用户,组
groupadd -g 1200 mysql
useradd -r -g mysql -u 1200 -s /sbin/nologin mysql

# 创建数据库存储目录
mkdir -p $datadir
```

### 解压

```
cd $PACKAGE_DIR
tar xvf mysql-5.7.32-linux-glibc2.12-x86_64.tar.gz
mv mysql-5.7.32-linux-glibc2.12-x86_64 $basedir
```

### 配置服务 && 跟随系统启动

```
# 配置启动脚本
cp $basedir/support-files/mysql.server /etc/init.d/mysqld
sed -i "s@^basedir=.*@basedir=$basedir@" /etc/init.d/mysqld
sed -i "s@^datadir=.*@datadir=$datadir@" /etc/init.d/mysqld
chmod +x /etc/init.d/mysqld

# 添加服务 && 跟随系统启动
chkconfig --add mysqld
chkconfig mysqld on

# 配置mysql库文件
rm -rf /etc/ld.so.conf.d/mariadb-x86_64.conf
echo "$basedir/lib" > /etc/ld.so.conf.d/mysql.conf
ldconfig
```

### 创建 mysql 配置文件

```
cat <<EOF  > /etc/my.cnf
[mysqld]
datadir=/home/data/mysql/data
basedir=/home/data/mysql
socket=/tmp/mysql.sock
user=mysql
port=3306
character-set-server=utf8
# 取消密码验证
# skip-grant-tables

# Disabling symbolic-links is recommended to prevent assorted security risks
# symbolic-links=0

# [mysqld_safe]
# log-error=/var/log/mysqld.log
# pid-file=/var/run/mysqld/mysqld.pid
EOF
```

### 初始化数据库

```
# 初始化
$basedir/bin/mysqld --initialize-insecure --user=mysql --basedir=$basedir --datadir=$datadir
chown mysql.mysql -R $datadir $basedir
systemctl start mysqld

# 添加环境变量
echo "export PATH=$basedir/bin:\$PATH" >> /etc/profile
. /etc/profile
```

### 初始化 root密码, 权限

```
dbrootpwd=DE56#OIwerW    # 数据库root密码
mysql -e "grant all privileges on *.* to root@'127.0.0.1' identified by \"${dbrootpwd}\" with grant option;"
mysql -e "grant all privileges on *.* to root@'localhost' identified by \"${dbrootpwd}\" with grant option;"
mysql -uroot -p${dbrootpwd} -e "reset master;"

mysql -uroot -p${dbrootpwd} -e "grant all privileges on *.* to root@'%' identified by \"${dbrootpwd}\" with grant option;"
mysql -uroot -p${dbrootpwd} -e "SELECT DISTINCT CONCAT('User: ''',user,'''@''',host,''';') AS query FROM mysql.user;"
mysql -uroot -p${dbrootpwd} -e "reset master;"

```

## 配置 hadoop namenode 数据备份

```
cat <<'EOF'  >/script/backup.sh
#!/bin/bash

# file
file=$(date +'%w-%H')

# backup dir
cd /home/hadoop/hadoop-2.9.2
tar czvf /home/backup/hadoop-${file}.tar.gz name
EOF

chmod +x /script/backup.sh
```

## 防火墙配置

### 仅对指定主机开放所有端口

```
# 大数据集群之间
firewall-cmd --add-rich-rule 'rule family="ipv4" source address=10.8.10.61 accept' --permanent
firewall-cmd --add-rich-rule 'rule family="ipv4" source address=10.8.10.62 accept' --permanent
firewall-cmd --add-rich-rule 'rule family="ipv4" source address=10.8.10.63 accept' --permanent
firewall-cmd --reload
firewall-cmd --list-rich-rules

# 允许指定网段局域网访问
firewall-cmd --add-rich-rule='rule family="ipv4" source address="192.168.2.0/24" accept'  --permanent
firewall-cmd --reload
firewall-cmd --list-rich-rules
```