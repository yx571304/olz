title: OpenStack集群安装-05_Keystone验证服务群集
tags:
  - openstack
  - keystone
copyright: true
categories:
  - openstack
abbrlink: openstack-05
date: 2018-07-25 05:51:00
---
### 集群安装

#### 创建 keystone 数据库, 授权

    source ~/PASS                            # 读取数据库密码
    mysql -u root -p$DBPass -e "
    create database keystone;
    grant all privileges on keystone.* to 'keystone'@'localhost' identified by 'keystone';
    grant all privileges on keystone.* to 'keystone'@'%' identified by 'keystone';"

#### 安装 httpd, keystone, memcached

    for HOST in controller{1..3}; do
        echo "--------------- $HOST ---------------"
        ssh -T $HOST <<EOF
        # 安装软件
        yum install -y openstack-keystone httpd mod_wsgi memcached python-memcached

        # 配置Memcached
        cp /etc/sysconfig/memcached{,.bak}                       # 备份默认配置
        sed -i 's/127.0.0.1/0.0.0.0/' /etc/sysconfig/memcached   # 修改监听地址
        systemctl enable memcached                               # 跟随系统启动
        systemctl start memcached                                # 启动服务
    EOF
    done

### 配置Keystone

#### 备份配置,生成密码

    cp /etc/keystone/keystone.conf{,.bak}                        # 备份默认配置
    Keys=$(openssl rand -hex 10)                                 # 生成随机密码
    echo $Keys
    echo "kestone  $Keys">~/keystone.key

#### 创建 keystone 配置文件

    cat <<EOF  >/etc/keystone/keystone.conf
    [DEFAULT]
    admin_token = $Keys
    verbose = true

    [database]
    connection = mysql+pymysql://keystone:keystone@controller/keystone

    [memcache]
    servers = controller1:11211,controller2:11211,controller3:11211

    [token]
    provider = fernet
    driver = memcache
    # expiration = 86400
    # caching = true
    # cache_time = 86400

    [cache]
    enabled = true
    backend = oslo_cache.memcache_pool
    memcache_servers = controller1:11211,controller2:11211,controller3:11211
    EOF

#### 初始化Fernet keys

    keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
    keystone-manage credential_setup --keystone-user keystone --keystone-group keystone

#### 同步配置到其它节点(用scp会改变文件权限)

    rsync -avzP -e 'ssh -p 22' /etc/keystone/* controller2:/etc/keystone/
    rsync -avzP -e 'ssh -p 22' /etc/keystone/* controller3:/etc/keystone/

### 初始化Keystone

    su -s /bin/sh -c "keystone-manage db_sync" keystone        # 初始化keystone数据库
    mysql -h controller -ukeystone -pkeystone -e "use keystone;show tables;"    # 验证


### 群集设置

> 修改默认端口5000,35357(默认端口给群集vip使用)

    for HOST in controller{1..3}; do
        ssh -T $HOST <<EOF    
        # 链接 keystone 配置文件
        ln -s /usr/share/keystone/wsgi-keystone.conf /etc/httpd/conf.d/

        # 修改默认端口5000,35357
        [ -f /usr/share/keystone/wsgi-keystone.conf.bak ] || cp /usr/share/keystone/wsgi-keystone.conf{,.bak}
        sed -i 's/5000/4999/' /usr/share/keystone/wsgi-keystone.conf
        sed -i 's/35357/35356/' /usr/share/keystone/wsgi-keystone.conf

        #Apache HTTP 重启并设置开机自启动
        systemctl enable httpd
        systemctl restart httpd
    EOF
    done

### 配置Haproxy

#### 添加 Keystone 代理

    cat <<EOF >>/etc/haproxy/haproxy.cfg
    
    ############ Keystone ############
    listen keystone_admin_cluster
      bind controller:35357
      #balance source
      option tcpka
      option httpchk 
      option tcplog
      server controller1 controller1:35356 check inter 2000 rise 2 fall 5
      server controller2 controller2:35356 check inter 2000 rise 2 fall 5
      server controller3 controller3:35356 check inter 2000 rise 2 fall 5

    listen keystone_public_cluster
      bind controller:5000
      #balance source
      option tcpka
      option httpchk 
      option tcplog
      server controller1 controller1:4999 check inter 2000 rise 2 fall 5
      server controller2 controller2:4999 check inter 2000 rise 2 fall 5
      server controller3 controller3:4999 check inter 2000 rise 2 fall 5
    EOF


#### 同步 haproxy 配置文件

    scp /etc/haproxy/haproxy.cfg controller2:/etc/haproxy/haproxy.cfg
    scp /etc/haproxy/haproxy.cfg controller3:/etc/haproxy/haproxy.cfg

#### 重启服务

    systemctl restart haproxy
    ssh controller2 "systemctl restart haproxy"
    ssh controller3 "systemctl restart haproxy"

#### 验证

    curl http://controller1:35356/v3
    curl http://controller2:35356/v3
    curl http://controller3:35356/v3
    curl http://controller:35357/v3


### 初始化keystone

#### 创建服务实体和API端点, 服务实体和API端点

    keystone-manage bootstrap --bootstrap-password admin \
      --bootstrap-admin-url http://controller:35357/v3/ \
      --bootstrap-internal-url http://controller:5000/v3/ \
      --bootstrap-public-url http://controller:5000/v3/ \
      --bootstrap-region-id RegionOne

#### 创建 OpenStack 客户端环境脚本

      cat <<EOF  >~/admin-openstack.sh
      export OS_PROJECT_DOMAIN_NAME=default
      export OS_USER_DOMAIN_NAME=default 
      export OS_PROJECT_NAME=admin 
      export OS_USERNAME=admin
      export OS_PASSWORD=admin
      export OS_AUTH_URL=http://controller:35357/v3
      export OS_IDENTITY_API_VERSION=3
      export OS_IMAGE_API_VERSION=2
      EOF

> 测试脚本是否生效

    source ~/admin-openstack.sh
    openstack token issue

### 创建项目

> 添加环境中每个服务包含独有用户的service 项目。：

#### 创建 service 项目

    openstack project create --domain default --description "Service Project" service

#### 创建 demo 项目,角色 授权

    openstack project create --domain default --description "Demo Project" demo
    openstack user create --domain default --password=demo demo
    openstack role create user
    openstack role add --project demo --user demo user

#### 创建demo环境脚本

    cat <<EOF >~/demo-openstack.sh
    export OS_PROJECT_DOMAIN_NAME=default
    export OS_USER_DOMAIN_NAME=default
    export OS_PROJECT_NAME=demo
    export OS_USERNAME=demo
    export OS_PASSWORD=demo
    export OS_AUTH_URL=http://controller:5000/v3
    export OS_IDENTITY_API_VERSION=3
    export OS_IMAGE_API_VERSION=2
    EOF

> 测试脚本是否生效

    source ~/demo-openstack.sh
    openstack token issue

### 验证

> 浏览器打开 http://192.168.0.11:1080/admin 查看 >keystone_admin_cluster,keystone_public_cluster 状态

>获取节点信息
curl http://controller1:35356/v3
curl http://controller2:35356/v3
curl http://controller3:35356/v3
curl http://controller:35357/v3

### 使用脚本

    site='http://home.onlycloud.xin'
    wget $site/code/openstack-05_keystone-cluster.sh -O openstack-keystone-cluster.sh
    sh openstack-keystone-cluster.sh
