---
title: '主域控Down机不可恢复,辅助域控如何抢占角色'
tags:
  - windows
  - AD
categories:
  - windows
abbrlink: 389a
date: 2018-01-17 19:01:04
---
>活动目录（Active Directory）是面向`Windows Standard Server`、`Windows Enterprise Server`以及 `Windows Datacenter Server`的目录服务。活动目录服务是`Windows Server 2000`操作系统平台的中心组件之一。理解活动目录对于理解`Windows Server 2000`的整体价值是非常重要的。这篇关于活动目录服务所涉及概念和技术的介绍文章描述了活动目录的用途，提供了对其工作原理的概述，并概括了该服务为不同组织和机构提供的关键性商务及技术便利

#### `Active Directory`结构
>Active Directory 定义了五种操作主机角色（又称ＦＳＭＯ）： 

	1.架构主机 schema master
	2..域命名主机 domain naming master
	3.相对标识号 (RID) 主机 RID master
	4.主域控制器模拟器 (PDCE) 
	5.基础结构主机 infrastructure master
    
>每种操作主机角色负担不同的工作，具有不同的功能：

1.架构主机
具有架构主机角色的 DC 是可以更新目录架构的唯一 DC。这些架构更新会从架构主机复制到目录林中的所有其它域控制器中。 架构主机是基于目录林的，整个目录林中只有一个架构主机。

2.域命名主机 
具有域命名主机角色的 DC 是可以执行以下任务的唯一 DC： 
向目录林中添加新域。 
从目录林中删除现有的域。 
添加或删除描述外部目录的交叉引用对象。 

3.相对标识号 (RID) 主机
此操作主机负责向其它 DC 分配 RID 池。只有一个服务器执行此任务。在创建安全主体（例如用户、
组或计算机）时，需要将 RID 与域范围内的标识符相结合，以创建唯一的安全标识符 (SID)。 每一个 Windows 2000 DC 都会收到用于创建对象的 RID 池（默认为 512）。RID 主机通过分配不同的池来确保这些 ID 在每一个 DC 上都是唯一的。通过 RID 主机，还可以在同一目录林中的不同域之间移动所有对象。

域命名主机是基于目录林的，整个目录林中只有一个域命名主机。相对标识号（RID）主机是基于域的，目录林中的每个域都有自己的相对标识号（RID）主机

4.PDCE 
主域控制器模拟器提供以下主要功能： 
向后兼容低级客户端和服务器，允许 Windows NT4.0 备份域控制器 (BDC) 加入到新的 Windows 2000 环境。 本机 Windows 2000 环境将密码更改转发到 PDCE。每当 DC 验证密码失败后，它会与 PDCE 取得联系，以查看该密码是否可以在那里得到验证，也许其原因在于密码更改还没有被复制到验证 DC 中。 
时间同步 — 目录林中各个域的 PDCE 都会与目录林的根域中的 PDCE 进行同步。
PDCE是基于域的，目录林中的每个域都有自己的PDCE。 

5.基础结构主机
基础结构主机确保所有域间操作对象的一致性。当引用另一个域中的对象时，此引用包含该对象的
全局唯一标识符 (GUID)、安全标识符 (SID) 和可分辨的名称 (DN)。如果被引用的对象移动，则在域中担
当结构主机角色的 DC 会负责更新该域中跨域对象引用中的 SID 和 DN。
基础结构主机是基于域的，目录林中的每个域都有自己的基础结构主机
默认，这五种ＦＭＳＯ存在于目录林根域的第一台DC（主域控制器）上，而子域中的相对标识号 (RID) 主机、PDCE 、基础结构主机存在于子域中的第一台DC。

#### 实验环境
>系统环境`windows Server 2008/2012`

	Master计算机全名: owone.test.com
	Slave计算机全名: owtwo.test.com


#### 前提条件

1.主从域DNS必须与AD集成
2.主从域DC类型必须都是全局编录


#### 夺取角色
>`WIN+R`输入CMD然后点击确定

##### 查看五大角色

    netdom query fsmo

##### 进入ntdsutil模式

    ntdsutil

##### 管理 NTDS 角色所有者令牌

    roles

##### 连接到一个特定 AD DC/LDS 实例

    connection

##### 连接到服务器、DNS
>当前能正常运行的域控

    connect to server owtwo.test.com

##### 返回上一个菜单

    quit

##### 在已连接的服务器上覆盖 RID 角色

    seize rid master

##### 在已连接的服务器上覆盖 PDC 角色

    seize pdc

##### 覆盖已连接的服务器上的命名主机角色

    seize naming master

##### 在已连接的服务器上覆盖结构角色

    seize infrastructure master

##### 在已连接的服务器上覆盖架构角色

    seize schema master

##### 返回上一个菜单,退出

    quit
    quit

##### 查看五大角色

    netdom query fsmo

#### 删除残余服务器信息
>`WIN+R`输入CMD然后点击确定

##### 进入ntdsutil模式

    ntdsutil

##### 清理不使用的服务器的对象

    metadata cleanup

##### 选择的站点，服务器，域，角色和命名上下文

    select operation target

##### 连接到一个特定 AD DC/LDS 实例

    connections

##### 连接到服务器、DNS
>当前能正常运行的域控

    connect to server owtwo.test.com

##### 返回上一个菜单

    quit

##### 在企业中列出站点

    list sites

##### 将编号 0 站点定为所选站点

    select site 0

##### 列出所选站点中的域

    List domains in site

##### 将编号 0 域定为所选域

    select domain 0

##### 列出所选域和站点中的服务器

    List servers for domain in site

##### 将编号 0 服务器定为所选服务器
>选择需要删除的服务器

    select server 0

##### 返回上一个菜单

    quit

##### 删除所选定的服务器

    Remove selected server

##### 返回上一个菜单,退出

    quit
    quit

#### code

##### 夺取5大角色

```bat
netdom query fsmo
ntdsutil
roles
connection
connect to server owone.test.com
quit
seize rid master
seize pdc
seize naming master
seize infrastructure master
seize schema master
quit
quit
netdom query fsmo
```

##### 删除残余服务器信息
>不可直接复制粘贴(注意选择的服务器)

```bat
ntdsutil
metadata cleanup
select operation target
connections
connect to server owtwo.test.com
quit
list sites
select site 0
List domains in site
select domain 0
List servers for domain in site
select server 0
quit
Remove selected server
quit
quit
```