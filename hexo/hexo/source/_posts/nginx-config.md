title: Nginx 配置
author: 亦 漩
abbrlink: e30a
tags:
  - nginx
  - nginx配置
categories:
  - nginx
date: 2018-07-11 09:51:00
---
## Nginx配置

> 不定期更新

---

### Nginx实现浏览器实时查看访问日志

#### Nginx配置文件的Server标签

>/etc/nginx/nginx.cinf

    location /logs {
        # Nginx日志目录
        alias /application/nginx-1.9.7/logs;

        # 打开目录浏览功能
        autoindex on;

        # 默认为on，显示出文件的确切大小，单位是bytes
        # 显示出文件的大概大小，单位是kB或者MB或者GB        
        autoindex_exact_size off;

        #默认为off，显示的文件时间为GMT时间。
        #改为on后，显示的文件时间为文件的服务器时间
        autoindex_localtime on;

        #让浏览器不保存临时文件
        add_header Cache-Control no-store;
    }

#### 添加mime.types

>/etc/nginx/mime.types [mime参考](http://www.w3school.com.cn/media/media_mimeref.asp)

    sed "3a\    text/log                              log;" /etc/nginx/mime.types

#### 重载配置/验证

    nginx -s reload

>浏览器打开 http://ServerIP/logs


### Nginx用户认证

#### 安装验证工具

    yum -y install httpd-tools

#### 创建认证的账号

    htpasswd -c /etc/nginx/loguser loguser
    New password:
    Re-type new password:
    Adding password for user loguser
    #密码需要输入两次

#### 编辑nginx配置文件

>在需要验证的路径中配置以下内容例如：

    location /logs {
        alias PATH;
        autoindex on;
        autoindex_exact_size off;
        autoindex_localtime on;
        add_header Cache-Control no-store;
        # Nginx认证
        auth_basic "Restricted";
        #认证账号密码保存的文件   
        auth_basic_user_file  /var/log/nginx/

    }
