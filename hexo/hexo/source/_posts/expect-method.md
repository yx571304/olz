---
title: 'ssh,scp使用expect自动处理密码'
tags:
  - linux
  - expect
  - 自动应答
categories:
  - expect
abbrlink: 6e0e
date: 2018-01-17 16:12:23
---
#### ssh免密码登陆

>`Expect`是一个免费的编程工具语言，用来实现自动和交互式任务进行通信，而无需人的干预。`Expect`的作者`Don Libes`在1990年开始编写`Expect`时对`Expect`做有如下定义：`Expect`是一个用来实现自动交互功能的软件套件(`Expect [is a] software suite for automating interactive tools`)。使用它，系统管理员可以创建脚本来对命令或程序进行输入，而这些命令和程序是期望从终端（terminal）得到输入，一般来说这些输入都需要手工输入进行的。`Expect`则可以根据程序的提示模拟标准输入提供给程序需要的输入来实现交互程序执行

##### 安装expect

    yum -y install expect

##### 使用说明
>新建`ssh.exp`
>`chmod +x ssh.exp`
>使用方法 `./ssh.exp ip`
>文件中`passwd`变量为登陆ssh主机的密码

```bash
#!/usr/bin/expect
set timeout 10
set passwderror 0
set passwd 123456
spawn ssh root@192.168.3.10

expect {
    "*assword:*" {
        if { $passwderror == 1 } {
        puts "passwd is error"
        exit 2
        }
        set timeout 1000
        set passwderror 1
        send "$passwd\r"
        exp_continue
    }
    "*es/no)?*" {
        send "yes\r"
        exp_continue
    }
    timeout {
        puts "connect is timeout"
        exit 3
    }
}
```


#### scp免密码传书文件

>新建`scp.exp`
>`chmod +x scp.exp`
>使用方法`./scp.exp 源文件 目标文件`

```bash
#!/usr/bin/expect
set timeout 10
set passwderror 0
set passwd redhat
set spath [lindex $argv 0]
set tpath [lindex $argv 1]

spawn scp $spath $tpath

expect {
    "*assword:*" {
        if { $passwderror == 1 } {
        puts "passwd is error"
        exit 2
        }
        set timeout 1000
        set passwderror 1
        send "$passwd\r"
        exp_continue
    }
    "*es/no)?*" {
        send "yes\r"
        exp_continue
    }
    timeout {
        puts "connect is timeout"
        exit 3
    }
}
```