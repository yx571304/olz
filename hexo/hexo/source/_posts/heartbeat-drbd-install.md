---
title: Heartbeat-BRDB安装
tags:
  - HA
  - Heartbeat
  - rpm-build
copyright: true
categories:
  - heartbeat
abbrlink: dae5
date: 2018-08-12 03:02:23
---
    本文章主要写软件安装及配置过程如需集群配置请访问 http://home.onlycloud.xin/posts/6b7.html

    环境：CentOS 7.5
    MASTER: data-1-1  

      Heartbeat:

        ens33: 10.0.0.11    管理IP, Lan数据转发    VIP：10.0.0.101
        ens37: 10.0.10.11   NFS服务器心跳链接
        ens38: 10.0.11.11   NFS服务器DRDB同步 

      DRBD:

        管理IP        : ens33:10.0.0.11
        DRBD管理名称  : data
        DRBD挂载目录  : /data
        DRBD逻辑设备  : /dev/drbd0
        DRBD对接IP    : ens37:10.0.11.11
        DRBD存储设备  : /dev/sdb1
        DRBDMeta设备  : /dev/sdb2[0]

---

    BACKUP: data-1-2

      Heartbeat:

        ens33: 10.0.0.12    管理IP, Lan数据转发    VIP：10.0.0.102
        ens37: 10.0.10.12   NFS服务器心跳链接
        ens38: 10.0.11.12   NFS服务器DRDB同步

      DRBD:

        管理IP        : ens33:10.0.0.12
        DRBD管理名称  : data
        DRBD挂载目录  : /data
        DRBD逻辑设备  : /dev/drbd0
        DRBD对接IP    : ens37:10.0.11.12
        DRBD存储设备  : /dev/sdb1
        DRBDMeta设备  : /dev/sdb2[0]

## 编译安装Heartbeat

### [软件下载](http://linux-ha.org/wiki/Download)

    wget -O ClusterGlue-1.0.12.tar.bz2 http://hg.linux-ha.org/glue/archive/0a7add1d9996.tar.bz2
    wget -O Heartbeat-3.0.6.tar.bz2 http://hg.linux-ha.org/heartbeat-STABLE_3_0/archive/958e11be8686.tar.bz2
    wget -O resource-agents-3.9.6.tar.gz https://github.com/ClusterLabs/resource-agents/archive/v3.9.6.tar.gz

### 创建服务用户

    groupadd -g 501 haclient
    useradd -u 501 hacluster -g haclient -s /bin/false

### 安装编译环境

    yum install -y gcc gcc-c++ autoconf automake libtool glib2-devel libxml2-devel \
      bzip2 bzip2-devel libtool-ltdl-devel libuuid-devel asciidoc

### 编译安装Glue

    mkdir /usr/local/src
    tar xvf ClusterGlue-1.0.12.tar.bz2 -C /usr/local/src
    cd /usr/local/src/Reusable-Cluster-Components-glue--0a7add1d9996
    ./autogen.sh
    ./configure --prefix=/usr/local/heartbeat --with-daemon-user=hacluster --with-daemon-group=haclient
    make && make install

### 编译安装 Resource Agents

    cd -
    tar xvf resource-agents-3.9.6.tar.gz -C /usr/local/src
    cd /usr/local/src/resource-agents-3.9.6
    ./autogen.sh
    ./configure --prefix=/usr/local/heartbeat
    make && make install

### 编译安装 Heartbeat

    cd -
    tar -xvf Heartbeat-3.0.6.tar.bz2 -C /usr/local/src
    cd /usr/local/src/Heartbeat-3-0-958e11be8686
    export CFLAGS="$CFLAGS -I/usr/local/heartbeat/include -L/usr/local/heartbeat/lib"
    ./bootstrap
    ./configure --prefix=/usr/local/heartbeat --enable-fatal-warnings=no LIBS='/lib64/libuuid.so.1'
    make && make install
    cd

### Heartbeat 配置

```conf
# Hosts (主机名解析到心跳网卡真实IP)
cat <<EOF   >>/etc/hosts
10.0.10.11    data-1-1
10.0.10.12    data-1-2
EOF

# 复制配置文件模板
cp /usr/local/heartbeat/share/doc/heartbeat/{ha.cf,authkeys,haresources} /usr/local/heartbeat/etc/ha.d

# ha.cf 配置文件
cat <<EOF   >/usr/local/heartbeat/etc/ha.d/ha.cf
# 调试日志
debugfile /var/log/ha-debug

# heartbeat 运行日志
logfile /var/log/ha-log

# 在 syslog 服务中配置通过 local1 设备接收日志
logfacility local0

# 心跳间隔时间
keepalive 2

# 备用节点在 30 秒内没有收到主节点心跳信号则立即接管主节点服务资源
deadtime 30

# 心跳延迟时间为 10 秒,当 10 秒内备份节点未收到主节点心跳信号时,将写入警告日志,此时不会切换服务
warntime 10

# heartbeat 服务首次启动,需要等待 60 秒后才启动服务器的资源,该值至少为 deadtime 两倍(单机启动时vip绑定比较慢)
initdead 60

# 指定心跳信号网卡(直连网卡)
#bcast ens37

# 配置多播通信路径(直连网卡)
mcast ens37 239.0.0.1 694 1 0

# 主节点恢复后,是否将服务自动切回
auto_failback on

# 节点主机名(使用IP地址也可以)
node data-1-1
node data-1-2

# 是否开启 CRM 集群管理
crm no
EOF

# authkeys
cat <<EOF  >>/usr/local/heartbeat/etc/ha.d/authkeys

auth 1
1 sha1 cxix9lkzq2aevnxv43yn68yoh2y7zp2cfwufgs7w
EOF
chmod 600 /usr/local/heartbeat/etc/ha.d/authkeys

# haresources
cat <<EOF   >/usr/local/heartbeat/etc/ha.d/haresources
# 虚拟IP 配置到 ens33网卡
data-1-1 IPaddr::10.0.0.101/24/ens33
data-1-2 IPaddr::10.0.0.102/24/ens33
EOF

# 链接功能模块
ln -svf /usr/local/heartbeat/lib64/heartbeat/plugins/RAExec/* /usr/local/heartbeat/lib/heartbeat/plugins/RAExec/  
ln -svf /usr/local/heartbeat/lib64/heartbeat/plugins/* /usr/local/heartbeat/lib/heartbeat/plugins/  

systemctl restart heartbeat
systemctl status heartbeat

# 接管资源
/usr/local/heartbeat/share/heartbeat/hb_takeover           # 接管所有资源
/usr/local/heartbeat/share/heartbeat/hb_takeover local     # 接管本地资源
/usr/local/heartbeat/share/heartbeat/hb_takeover foreign   # 接管对端资源

# 释放资源
/usr/local/heartbeat/share/heartbeat/hb_standby
/usr/local/heartbeat/share/heartbeat/hb_standby local      # 释放本地资源
/usr/local/heartbeat/share/heartbeat/hb_standby foreign    # 释放对端资源

# 链接配置文件(便于修改)
ln -s /usr/local/heartbeat/etc/ha.d/ /etc/
cp /usr/local/src/resource-agents-3.9.6/ldirectord/ldirectord.cf /etc/ha.d/
```


## RPM包安装Heartbeat

### 准备build环境

    yum install -y bzip2 rpm-build

### 下载源码

    wget -O Heartbeat-3.0.6.tar.bz2 http://hg.linux-ha.org/heartbeat-STABLE_3_0/archive/958e11be8686.tar.bz2

### 创建build所需路径

    mkdir -p /root/rpmbuild/SOURCES/
    tar xjvf Heartbeat-3.0.6.tar.bz2 -C /root/rpmbuild/SOURCES/
    cd /root/rpmbuild/SOURCES/
    mv Heartbeat-3-0-958e11be8686 heartbeat
    tar cjvf /root/rpmbuild/SOURCES/heartbeat.tar.bz2 heartbeat
    cd heartbeat


### 安装依赖关系

> 配置cluster-glue-libs-devel 安装源

    cat <<EOF   >/etc/yum.repos.d/gf-epel-7.repo
    [epel-testing]
    name=Extra Packages for Enterprise Linux 7 - $basearch - Testing
    baseurl=http://mirror.ghettoforge.org/distributions/gf/el/7/testing/x86_64
    enabled=1
    gpgcheck=0
    EOF
---

> 安装软件

    yum install -y glib2-devel libtool-ltdl-devel bzip2-devel ncurses-devel openssl-devel libtool \
      zlib-devel mailx cluster-glue-libs-devel libxslt docbook-dtds docbook-style-xsl libuuid-devel
---

### 生成RPM包

    rpmbuild -ba heartbeat-fedora.spec
    ls /root/rpmbuild/RPMS/x86_64/

### 其他节点安装

> 下载安装包

    mkdir /root/heartbeat
    scp 10.0.0.11:/root/rpmbuild/RPMS/x86_64/* /root/heartbeat

    heartbeat='heartbeat-3.0.6-1.el7.centos.x86_64.rpm'
    heartbeat-libs='heartbeat-libs-3.0.6-1.el7.centos.x86_64.rpm' 
    curl -o /root/heartbeat/$heartbeat http://home.onlycloud.xin/soft/heartbeat/$heartbeat --progress
    curl -o /root/heartbeat/$heartbeat-libs http://home.onlycloud.xin/soft/heartbeat/$heartbeat-libs --progress
---

> 配置cluster-glue-libs-devel 安装源

    cat <<EOF   >/etc/yum.repos.d/gf-epel-7.repo
    [epel-testing]
    name=Extra Packages for Enterprise Linux 7 - $basearch - Testing
    baseurl=http://mirror.ghettoforge.org/distributions/gf/el/7/testing/x86_64
    enabled=1
    gpgcheck=0
    EOF
---

> 安装

    cd /root/heartbeat
    yum localinstall heartbeat-*

### 配置Heartbeat

```conf
# 复制配置文件模板
cp /usr/share/doc/heartbeat-3.0.6/{ha.cf,authkeys,haresources} /etc/ha.d/

# ha.cf 配置文件
cat <<EOF   >/etc/ha.d/ha.cf
# 调试日志
debugfile /var/log/ha-debug

# heartbeat 运行日志
logfile /var/log/ha-log

# 在 syslog 服务中配置通过 local1 设备接收日志
logfacility local0

# 心跳间隔时间
keepalive 2

# 备用节点在 30 秒内没有收到主节点心跳信号则立即接管主节点服务资源
deadtime 30

# 心跳延迟时间为 10 秒,当 10 秒内备份节点未收到主节点心跳信号时,将写入警告日志,此时不会切换服务
warntime 10

# heartbeat 服务首次启动,需要等待 60 秒后才启动服务器的资源,该值至少为 deadtime 两倍(单机启动时vip绑定比较慢)
initdead 60

# 指定心跳信号网卡(直连网卡)
#bcast ens37

# 配置多播通信路径(直连网卡)
mcast ens37 239.0.0.1 694 1 0

# 主节点恢复后,是否将服务自动切回
auto_failback on

# 节点主机名(使用IP地址也可以)
node data-1-1
node data-1-2

# 是否开启 CRM 集群管理
crm no
EOF

# 两端认证方式 authkeys
cat <<EOF  >>/etc/ha.d/authkeys

auth 1
1 sha1 cxix9lkzq2aevnxv43yn68yoh2y7zp2cfwufgs7w
EOF
chmod 600 /etc/ha.d/authkeys

# haresources添加资源
# 相当于执行脚本 /etc/ha.d/resource.d/IPaddr 10.0.0.101/24/ens33 stop/start
cat <<EOF   >/etc/ha.d/haresources
# 虚拟IP配置到 ens33 网卡(虚拟IP流量走 ens33网卡)
data-1-1 IPaddr::10.0.0.101/24/ens33
data-1-2 IPaddr::10.0.0.102/24/ens33
EOF
```

## 验证Heartbeat

```conf
# 启动服务,跟随系统启动 验证(两个节点各自执行 ip add 能看到两个 10.0.0网段ip)
systemctl start heartbeat
systemctl enable heartbeat
netstat -ntaulp
ip add | grep 10.0.0

# 关闭一端服务,然后在另一端验证(在正常的一端已经接管了所有虚拟IP ip add 能看到三个 10.0.0网段ip)
systemctl stop heartbeat
ip add | grep 10.0.0

# 启动关闭的服务后验证 虚拟IP已经恢复
systemctl start heartbeat
ip add | grep 10.0.0
```

## DRBD编译安装

### 安装依赖环境

>依赖环境安装(升级内核)

    yum install -y gcc gcc-c++  flex kernel kernel-devel kernel-headers elfutils-libelf-devel
    reboot
---

>依赖环境安装(安装指定版本kernel-devel 无需升级内核)

    yum install -y gcc gcc-c++ flex kernel-devel-$(uname -r) kernel-headers elfutils-libelf-devel
---

### 编译安装 DRBD 内核驱动

>如果 make 立即报错请确定当前使用的内核版本与 kernel-devel 版本一致

    wget http://www.linbit.com/downloads/drbd/9.0/drbd-9.0.14-1.tar.gz
    tar xvf drbd-9.0.14-1.tar.gz -C /usr/local/src
    cd /usr/local/src/drbd-9.0.14-1
    make KDIR=/usr/src/kernels/$(uname -r)/
    make install

>加载模块,验证

    modprobe drbd
    lsmod | grep drbd

    # 跟随系统启动加载模块
    echo 'modprobe drbd' >>/etc/rc.local
    chmod +x /etc/rc.local

### 编译安装 DRBD

    # 安装依赖环境
    yum install -y autoconf automake libxslt docbook-style-xsl
    cd
    wget http://www.linbit.com/downloads/drbd/utils/drbd-utils-9.4.0.tar.gz
    tar xvf drbd-utils-9.4.0.tar.gz -C /usr/local/src
    cd /usr/local/src/drbd-utils-9.4.0
    ./autogen.sh
    ./configure --prefix=/usr/local/drbd --sysconfdir=/etc
    make && make install

### 安装drbdmanage

    wget http://www.linbit.com/downloads/drbdmanage/drbdmanage-0.99.17.tar.gz
    tar xvf drbdmanage-0.99.17.tar.gz -C /usr/local/src
    cd /usr/local/src/drbdmanage-0.99.17
    python setup.py install


### DRBD配置

>DRBD 配置文件(两端配置文件相同)

```conf
cp /etc/drbd.conf{,.bak}
cat <<EOF   >/etc/drbd.d/data.res
# resource data

resource data {
    protocol C;
    
    disk {
        on-io-error detach;
    }
    
    on data-1-1 {
        device     /dev/drbd0;
        disk       /dev/sdb1;
        address    10.0.10.11:7788;
        meta-disk  /dev/sdb2[0];
    }
    
    on data-1-2 {
        device     /dev/drbd0;
        disk       /dev/sdb1;
        address    10.0.10.12:7788;
        meta-disk  /dev/sdb2[0];
    }
}
EOF

# 创建run路径
mkdir -p /usr/local/drbd/var/run/drbd

# 链接脚本文件
ln -s /usr/local/drbd/lib/drbd/* /usr/lib/drbd/

```

### 初始化

> 硬盘分区分区(sdb1为数据分区sdb2为 meta分区用于存储 drdb同步状态信息 )

    echo -e 'n\np\n1\n\n+3G\nw' | fdisk /dev/sdb
    echo -e 'n\np\n2\n\n+1G\nw' | fdisk /dev/sdb
    partprobe
    lsblk

    # 在节点1,2初始化资源
    drbdadm create-md data

    # 在节点1,2启动资源
    drbdadm up data

    # 查看两个节点上data的角色(两个都为：Secondary)
    drbdadm role data

    # 将节点1设置为主节点(执行这条命令的为主节点)
    drbdadm primary data --force

    # 查看节点1 data的角色(此时已经变为：Primary)
    drbdadm role data

    # 查看两个节点各自drbd状态(0:data/0  Connected(2*) Second/Primar Incons/UpToDa)
    /usr/local/drbd/sbin/drbd-overview

    # 待两边同步完成后(0:data/0  Connected(2*) Second/Primar UpToDa/UpToDa)
    /usr/local/drbd/sbin/drbd-overview
    netstat -ntaulp

### 验证

    # 挂载,创建文件
    mkdir /data
    mount /dev/drbd0 /data
    cd /data
    touch {1..10}
    /usr/local/drbd/sbin/drbd-overview

    # 重启drbd资源
    umount /data
    drbdadm down data
    drbdadm up data

    # 从节点获取主身份,验证数据
    drbdadm primary data --force
    mkdir /data
    mount /dev/drbd0 /data
    ls -la /data
    /usr/local/drbd/sbin/drbd-overview

## YUM安装BRBD

> YUM 安装 DRDB84

    rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
    yum install -y kmod-drbd84 drbd84-utils
    modprobe drbd
    echo drbd > /etc/modules-load.d/drbd.conf


> YUM 安装 DRDB90

    rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
    rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
    yum install -y drbd90-utils kmod-drbd90

    modprobe drbd
    echo drbd > /etc/modules-load.d/drbd.conf

