title: php简单验证
tags:
  - 群晖
  - php
  - 登陆验证
categories:
  - php
abbrlink: 3a03
date: 2018-03-27 20:03:47
---
原理：判断输入变量,如果通过则将一个字符串赋值给`$_SESSION`(可在全局读取变量内容) 然后跳转到指定页面,页面头部验证`$_SESSION`变量内容,如果不通过则跳转到登陆页面,销毁`$_SESSION`变量退出登陆

---


#### 示例1

> 登陆页面(login.php)

```php
<?php
    // session开始
    session_start();

    $name = $_POST['name'];
    $password = $_POST['password'];
    // 定义用户名和密码
    $uname = "admin";
    $upassword = "12345";
    // 判断是否是点了提交按钮
    if(isset($_POST['submit'])){
        // 判断账号密码
        if ($password != $upassword || $name != $uname){
            // 如果不匹配则给 $_SESSION['key'] 变量一个随机值
            $msg = "密码错误！";
            $_SESSION['key'] = md5(time().mt_rand(0,1000));

            }
        else if ($password == $upassword && $name == $uname){
            // 通过给 $_SESSION['key'] 赋值指定字符串,跳转到首页进行验证
            $_SESSION['key'] = "51aca69bc8fc0c8c6c654b5fb76bdf62";
            header('location:index.php');
            }
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>示例登陆页</title>
    <style>
        #win10-login {
            background: url('https://picsum.photos/1920/1080/?random') no-repeat fixed;
         /* background: url('./img/wallpapers/login.jpg') no-repeat fixed; */
            width: 100%;
            height: 100%;
            background-size: 100% 100%;
            position: fixed;
            z-index: -1;
            top: 0;
            left: 0;
        }

        #win10-login-box {
            width: 300px;
            overflow: hidden;
            margin: 0 auto;
        }

        .win10-login-box-square {
            width: 105px;
            margin: 0 auto;
            border-radius: 50%;
            background-color: darkgray;
            position: relative;
            overflow: hidden;
        }

        .win10-login-box-square::after {
            content: "";
            display: block;
            padding-bottom: 100%;
        }

        .win10-login-box-square .content {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        input {
            width: 90%;
            display: block;
            border: 0;
            margin: 0 auto;
            line-height: 36px;
            font-size: 20px;
            padding: 0 1em;
            border-radius: 5px;
            margin-bottom: 11px;
        }

        .login-username, .login-password {
            width: 91%;
            font-size: 13px;
            color: #999;
        }

        .login-password {
            width: calc(91% - 54px);
            -webkit-border-radius: 2px 0 0 2px;
            -moz-border-radius: 2px 0 0 2px;
            border-radius: 5px 0 0 5px;
            margin: 0px;
            float: left;
        }

        .login-submit {
            margin: 0px;
            float: left;
            -webkit-border-radius: 0 5px 5px 0;
            -moz-border-radius: 0 5px 5px 0;
            border-radius: 0 5px 5px 0;
            background-color: #009688;
            width: 54px;
            display: inline-block;
            height: 36px;
            line-height: 36px;
            padding: 0 auto;
            color: #fff;
            white-space: nowrap;
            text-align: center;
            font-size: 14px;
            border: none;
            cursor: pointer;
            opacity: .9;
            filter: alpha(opacity=90);

        }
    </style>
</head>
<body>
<div id="win10-login">
    <div style="height: 10%;min-height: 120px"></div>
    <div id="win10-login-box">
        <div class="win10-login-box-square">
            <img src="./img/avatar.jpg" class="content"/>
        </div>
        <p style="font-size: 24px;color: white;text-align: center">游客</p>
        <form target="_self" method="post" action="">
            <!--用户名-->
            <input type="text" name="name" placeholder="请输入登录名" class="login-username">
            <!--密码-->
            <input type="password" name="password"  placeholder="请输入密码" class="login-password">
            <!--登陆按钮-->
            <input type="submit" name="submit"  value="登录" id="btn-login" class="login-submit"/>
        </form>
    </div>
    <!--messig-->
    <div style="width:100px;margin:20px auto;color:#FFFF00">
        <p ><?php echo $msg;?></p>
    </div>
</div>
</body>
</html>
```

---

> 首页添加验证(index.php)

```php
<?php
    // session开始
    session_start();
    // 验证接收到的 $_SESSION['key'] 是否是预定义的,如果不是
    if ($_SESSION['key'] != "51aca69bc8fc0c8c6c654b5fb76bdf62"){
        header('location:login.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>示例登陆页</title>
    <style>
        #win10-login {
            background: url('https://picsum.photos/1920/1080/?random') no-repeat fixed;
         /* background: url('./img/wallpapers/login.jpg') no-repeat fixed; */
            width: 100%;
            height: 100%;
            background-size: 100% 100%;
            position: fixed;
            z-index: -1;
            top: 0;
            left: 0;
        }

        #box {
            width: 200px;
            height: 200px;
            margin: 20% auto;
        }

    </style>
</head>
<body id="win10-login">
    <div id="box">
        <p ><?php echo $msg;?></p>
        <a href="http://www.w3school.com.cn">W3School</a>
        <div><a href='logout.php'>注销</a></div>
    </div>
</div>
</body>
</html>

---

> 注销页面(logout.php)

```php
<?php

    session_start();

    if(isset($_SESSION['key'])){
        header('Content-type:text/html;charset=utf-8');
        session_unset();//free all session variable
        session_destroy();//销毁一个会话中的全部数据
        setcookie(session_name(),'',time()-3600);//销毁与客户端的卡号
        header('location:index.php');
    }
?>
```

---

#### 示例2

> 登陆页面(login.php)

设置 session 有效期 300 秒 (300 秒内没有任何操作和刷新 session 过期需重新登陆)
检测 $_SESSION['username'] 如果值等于预定义则 跳转到首页
如果有 POST 提交检测 输入的值如果是预定义 将 $_POST['username'] 变量值存入 $_SESSION['username'] 否则继续验证

``` php
<?php
    ini_set('session.cookie_lifetime', 300);
    ini_set('session.gc_maxlifetime', 300);
    // 每1次访问页面,检查所有的 session 并将过期的删除
    ini_set('session.gc_divisor', 1);
    session_start();
    header('Content-type:text/html;charset=utf-8');
    if(isset($_SESSION['username']) && $_SESSION['username']==='admin'){
        $msg = "您已经登入了，请不要重新登入！";
        header("Refresh:3;url=index.php");
    }

    if(isset($_POST['submit'])){
        if(isset($_POST['username']) && isset($_POST['password']) && $_POST['username']=='admin' && $_POST['password']=='12345' ){
            $_SESSION['username']=$_POST['username'];
            header('location:index.php');
        }else{
            $msg = "用户名或密码错误！";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>示例登陆页</title>
    <style>
        #win10-login {
            background: url('https://picsum.photos/1920/1080/?random') no-repeat fixed;
         /* background: url('./img/wallpapers/login.jpg') no-repeat fixed; */
            width: 100%;
            height: 100%;
            background-size: 100% 100%;
            position: fixed;
            z-index: -1;
            top: 0;
            left: 0;
        }

        #win10-login-box {
            width: 300px;
            overflow: hidden;
            margin: 0 auto;
        }

        .win10-login-box-square {
            width: 115px;
            margin: 0 auto;
            border-radius: 50%;
            background-color: darkgray;
            position: relative;
            overflow: hidden;
        }

        .win10-login-box-square::after {
            content: "";
            display: block;
            padding-bottom: 100%;
        }

        .win10-login-box-square .content {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        input {
            width: 90%;
            display: block;
            border: 0;
            margin: 0 auto;
            line-height: 36px;
            font-size: 20px;
            padding: 0 1em;
            border-radius: 5px;
            margin-bottom: 11px;
            background:url(0) no-repeat;
            background:rgba(0,0,0,0.2);
        }

        .login-username, .login-password {
            width: 91%;
            font-size: 13px;
            color: #009688;
        }

        .login-password {
            width: calc(91% - 54px);
            -webkit-border-radius: 2px 0 0 2px;
            -moz-border-radius: 2px 0 0 2px;
            border-radius: 5px 0 0 5px;
            margin: 0px;
            float: left;
        }

        .login-submit {
            margin: 0px;
            float: left;
            -webkit-border-radius: 0 5px 5px 0;
            -moz-border-radius: 0 5px 5px 0;
            border-radius: 0 5px 5px 0;
            background-color: #009688;
            width: 54px;
            display: inline-block;
            height: 36px;
            line-height: 36px;
            padding: 0 auto;
            color: #fff;
            white-space: nowrap;
            text-align: center;
            font-size: 14px;
            border: none;
            cursor: pointer;
            opacity: .9;
            filter: alpha(opacity=90);
        }
    </style>
</head>
<body>
<div id="win10-login">
    <div style="height: 10%;min-height: 120px"></div>
    <div id="win10-login-box">
        <div class="win10-login-box-square">
       <!-- <img src="./img/avatar.jpg" class="content"/> -->
            <img src="https://picsum.photos/140/140/?random" class="content"/>
        </div>
        <p style="font-size: 24px;color: white;text-align: center">游客</p>
        <form target="_self" method="post" action="">
            <!--用户名-->
            <input type="text" name="username" autocomplete="new-password" placeholder="请输入登录名" class="login-username">
            <!--密码-->
            <input type="password" name="password" autocomplete="new-password" placeholder="请输入密码" class="login-password">
            <!--登陆按钮-->
            <input type="submit" name="submit"  value="登录" id="btn-login" class="login-submit"/>
        </form>
    </div>
    <!--messig-->
    <div style="width:150px;margin:20px auto;color:#009688">
        <p ><?php echo $msg;?></p>
    </div>
</div>
</body>
</html>

```

---

> 首页(index.php)

检测 $_SESSION['username'] 是否等于预定义值如果是则允许正常访问 否则跳转到登陆页面

```php
<?php
    ini_set('session.cookie_lifetime', 300);
    ini_set('session.gc_maxlifetime', 300);
    ini_set('session.gc_divisor', 1);
    session_start();
    header('Content-type:text/html;charset=utf-8');
    if(isset($_SESSION['username']) && $_SESSION['username']==='admin'){
        $msg = "您好！{$_SESSION['username']},欢迎回来！";
    }  else {
        header('location:login.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>示例登陆页</title>
    <style>
        #win10-login {
            background: url('https://picsum.photos/1920/1080/?random') no-repeat fixed;
         /* background: url('./img/wallpapers/login.jpg') no-repeat fixed; */
            width: 100%;
            height: 100%;
            background-size: 100% 100%;
            position: fixed;
            z-index: -1;
            top: 0;
            left: 0;
        }

        #box {
            width: 200px;
            height: 200px;
            margin: 20% auto;
        }

    </style>
</head>
<body id="win10-login">
    <div id="box">
        <p ><?php echo $msg;?></p>
        <a href="http://www.w3school.com.cn">W3School</a>
        <div><a href='logout.php'>注销</a></div>
    </div>
</div>
</body>
</html>

```

---

> 注销页面(logout.php)

```php
<?php
    session_start();
    header('Content-type:text/html;charset=utf-8');
    if(isset($_SESSION['username']) && $_SESSION['username']==='admin'){
            session_unset();//free all session variable
            session_destroy();//销毁一个会话中的全部数据
            setcookie(session_name(),'',time()-3600);//销毁与客户端的卡号
            header('location:login.php');>II
        }else{
            header('location:index.php');
        }
?>
```