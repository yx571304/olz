title: BT面板shell代码分析
tags:
  - shell
abbrlink: cbbd
date: 2018-03-30 17:45:26
categories:
---
### 选择下载节点

>路径：/www/server/panel/install/public

 - 功能：curl测试node节点挑选出延时最低的服务器
 - 循环nodes：
     - L6 获取Curl前 %s 1970-01-01 00:00:00 UTC到当前秒数 %N 纳秒(000000000-999999999)
     - L7 curl -s 静音模式 -S 显示错误 --connect-timeout 连接超时时间，-m 数据传输的最大允许时间用
     - L8 判断Curl是否成功
     - L9 获取Curl后  %s 1970-01-01 00:00:00 UTC到当前秒数 %N 纳秒(000000000-999999999)
     - L10 获取Curl前 1970-01-01 00:00:00 UTC到当前秒数
     - L11 获取Curl前 纳秒(000000000-999999999)
     - L12 获取Curl后 1970-01-01 00:00:00 UTC到当前秒数
     - L13 获取Curl后 纳秒(000000000-999999999)
     - L14 计算出Curl花费的微秒值
     - L15 转换为毫秒
     - L16 将毫秒值 存储到数组 values[2,3,4]
     - L17 将节点地址 存储到数组 urls[$time_ms] = urls[延时毫秒数]
     - L18 更新毫秒数组
 - 找出延时最低的node 并打印出来 (定义一个最大延时值做比较)
 - 循环 values[2,3,4] = 每个节点的curl延时 数组
     - L24 判断 定义的最大延时值 $j 与 循环的数组 $n 值比较 如果大于 则将 $n 赋值给 $j 循环完成 $j 就是最小值
 - L28判断如果所有节点curl延时都大于定义的最大延时值则 使用 `http://download.bt.cn` 否则使用挑选出来的最低延时节点

 - L35判断如果还没有挑选出节点 则初始化 函数开始挑选


```sh
get_node_url(){
    nodes=(http://125.88.182.172:5880 http://103.224.251.67 http://128.1.164.196);
    i=1;
    for node in ${nodes[@]};
    do
        start=`date +%s.%N`
        result=`curl -sS --connect-timeout 3 -m 60 $node/check.txt`
        if [ $result = 'True' ];then
            end=`date +%s.%N`
            start_s=`echo $start | cut -d '.' -f 1`
            start_ns=`echo $start | cut -d '.' -f 2`
            end_s=`echo $end | cut -d '.' -f 1`
            end_ns=`echo $end | cut -d '.' -f 2`
            time_micro=$(( (10#$end_s-10#$start_s)*1000000 + (10#$end_ns/1000 - 10#$start_ns/1000) ))
            time_ms=$(($time_micro/1000))
            values[$i]=$time_ms;
            urls[$time_ms]=$node
            i=$(($i+1))
        fi
    done
    j=5000
    for n in ${values[@]};
    do
        if [ $j -gt $n ];then
            j=$n
        fi
    done
    if [ $j = 5000 ];then
        NODE_URL='http://download.bt.cn';
    else
        NODE_URL=${urls[$j]}
    fi
}

if [ ! $NODE_URL ];then
    echo '正在选择下载节点...';
    get_node_url
fi
```
----

### 测试

```sh
get_node_url(){
    nodes=(http://125.88.182.172:5880 http://103.224.251.67 http://128.1.164.196);
    i=1;
    for node in ${nodes[@]};
    do
        start=`date +%s.%N`
        result=`curl -sS --connect-timeout 3 -m 60 $node/check.txt`
        if [ $result = 'True' ];then
            end=`date +%s.%N`
            start_s=`echo $start | cut -d '.' -f 1`
            start_ns=`echo $start | cut -d '.' -f 2`
            end_s=`echo $end | cut -d '.' -f 1`
            end_ns=`echo $end | cut -d '.' -f 2`
            time_micro=$(( (10#$end_s-10#$start_s)*1000000 + (10#$end_ns/1000 - 10#$start_ns/1000) ))
            time_ms=$(($time_micro/1000))
            values[$i]=$time_ms;
            urls[$time_ms]=$node
            i=$(($i+1))
            echo -e "节点: $node"
            echo -e "\t 开始_UTC/纳秒: $start"
            echo -e "\t 结束_UTC/纳秒: $end"
            echo -e "\t 开始_UTC: $start_s"
            echo -e "\t 开始_纳秒: $start_ns"
            echo -e "\t 结束_UTC: $end_s"
            echo -e "\t 结束_纳秒: $end_ns"
            echo -e "\t 微秒: $time_micro"
            echo -e "\t 微秒=秒*1000000+纳秒/1000: $(( (10#$end_s-10#$start_s)*1000000 + (10#$end_ns/1000 - 10#$start_ns/1000) ))"
            echo -e "\t 毫秒=微秒*1000: $time_ms"
            echo -e "\t values: values[$i]"
            echo -e "\t urls[$time_ms]: $urls[$time_ms]"
            echo -e "\t 当前节点延时：$time_ms----节点名: $node"
            echo -e "\n--------------------------------------------------------------------------------\n "
        fi
    done
    j=5000
    # 循环上一步获取到的毫秒数组
    for n in ${values[@]};
    do
        if [ $j -gt $n ];then
            j=$n
        fi
    done
    
    if [ $j = 5000 ];then
        NODE_URL='http://download.bt.cn';
        echo $NODE_URL
        echo 当前选择的节点为  $NODE_URL
    else
        NODE_URL=${urls[$j]}
        echo $NODE_URL
        echo 当前选择的节点为  $NODE_URL
    fi

}

if [ ! $NODE_URL ];then
    echo '正在选择下载节点...';
    get_node_url
fi

```
