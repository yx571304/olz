title: apache 日志分析
author: 亦 漩
abbrlink: 8d77
tags:
  - apache logs
  - apache
  - 日志分析
categories:
  - logs
date: 2018-06-27 15:05:00
---
## Apache 日志分析

### 日志路径

> 如果Apache采用默认安装方式安装，那么日志的位置应该在/usr/local/apache/logs/ 

    访问日志文件的位置实际上是一个配置选项。如果我们检查/usr/local/apache/conf/httpd.conf配置文件，可以看到该文件中有如下这行内容：
        
        ErrorLog "/www/wwwlogs/error_log"
        CustomLog "/www/wwwlogs/access_log" common
    
    虚拟主机日志文件 /usr/local/apache/conf/vhost/***.conf


### Apache访问日志格式

    [root@http ~]# cat /data/www/wwwlogs/www.*****.com-access_log_2018-06-21_123002.log  | head -n6
    192.168.0.109 - - [20/Jun/2018:13:15:31 +0800] "PROPFIND /remote.php/**** HTTP/1.1" 207 317 "-" "Mozilla/5.0 (Windows) mirall/2.4.1 (build 9270)"
    192.168.80.61 - - [20/Jun/2018:13:15:32 +0800] "PROPFIND /remote.php/**** HTTP/1.1" 207 286 "-" "Mozilla/5.0 (Windows) mirall/2.4.1 (build 9270)"
    192.168.80.58 - - [20/Jun/2018:13:15:33 +0800] "PROPFIND /remote.php/**** HTTP/1.1" 207 317 "-" "Mozilla/5.0 (Windows) mirall/2.4.1 (build 9270)"
    192.168.0.185 - - [20/Jun/2018:13:15:34 +0800] "PROPFIND /remote.php/**** HTTP/1.1" 207 312 "-" "Mozilla/5.0 (Windows) mirall/2.4.1 (build 9270)"
    192.168.80.69 - - [20/Jun/2018:13:15:34 +0800] "PROPFIND /remote.php/**** HTTP/1.1" 207 288 "-" "Mozilla/5.0 (Windows) mirall/2.4.1 (build 9270)"
    192.168.80.78 - - [20/Jun/2018:13:15:35 +0800] "PROPFIND /remote.php/**** HTTP/1.1" 207 287 "-" "Mozilla/5.0 (Windows) mirall/2.4.1 (build 9270)"

    日志名字段所代表的内容:
    
      1.远程主机        192.168.0.109
      2.空白(E-mail)    -
      3.空白(登录名)    -
      4.请求时间        [20/Jun/2018:13:15:31 +0800]
      5.方法+资源+协议  PROPFIND /remote.php/**** HTTP/1.1
      6.状态代码        207
      7.发送字节数      317

### 统计访问量

> 统计每个IP的访问量

    awk '{print $1}' /www/wwwlogs/***-access_log_2018-06-21_123002.log | sort -n | uniq -c | sort -n
       3 192.168.0.137
       3 192.168.2.51
     958 192.168.80.78
    1122 192.168.80.69
    1130 192.168.0.185
    1164 192.168.80.58
    1175 192.168.80.28
    1221 192.168.0.109
    1343 192.168.80.77
    1445 192.168.80.45
    1455 192.168.80.89
    1754 192.168.80.61
    3012 192.168.0.65
    
    # 1. 打印出所有IP(awk 可以用-F 指定分隔符，如果不指定分隔符，默认就以空白字符 比如空格、Tab 等
    # 2. 使用 sort 排序，-n 选项表示以数字的形式排序，如果不加-n，则以 ASCII 排序，日志中 IP 地址以数字的形式排序更容易区分
    # 3. uniq 命令是用来去重复的，一个文本中如果有多行内容是一模一样的使用 uniq 命令就可以把相同内容的行给删除掉，只留一行。而-c 选项的作用是计算重复的行数，所以在此题中使用 uniq -c 正好可以计算 IP 地址的访问数量，注意 uniq 去重的前提是首先要排序。
    # 3.sort -n 意思是按访问量大小来排序，请求量越大的 IP 排在越后面，如果要想排在前面，可以加一个-r 选项，即 sort –nr

    