---
title: '配置[yum,epel]源'
tags:
  - linux
  - yum配置
categories:
  - linux
abbrlink: fbb0
date: 2017-12-19 10:59:18
---
## 配置阿里云[yum,epel]源

### 备份系统原有yum源

    mkdir /etc/yum.repos.d/OldRepo
    mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/OldRepo/

### 下载yum源文件

    curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

### 下载epel源文件

    curl -o /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo

### [清除，生成]yum缓存

    yum clean all
    yum makecache

## 配置本地yum源

### 创建挂载目录

    mkdir /mnt/cd

### 挂载光盘

    echo "/dev/sr0    /mnt/cd     iso9660     defaults    0 0" >>/etc/fstab
    mount -a

### 创建yum源文件

``` bash
cat <<EOF  >/etc/yum.repos.d/local.repo
[Local-YUM]
name=Linux CentOS 7.4
baseurl=file:///mnt/cd
gpgcheck=0
EOF
```

### [清除，生成]yum缓存
>yum repolist all (列出所有源)

    yum clean all
    yum makecache
