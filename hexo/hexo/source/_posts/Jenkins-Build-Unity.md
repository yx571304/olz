title: Jenkins打包Unity项目
author: 亦 漩
abbrlink: b6fc
tags:
  - Windows server 2008
  - Jenkins
  - Unity
categories: []
date: 2018-05-28 12:45:00
---
### 环境

    系统：Windows Server 2008 R2 Standard (完全安装)
    IP：192.168.2.90
    软件版本：
        Java：1.8.0_171-winx64
        Subversion：1.8.17
        MariaDB：10.3.7-winx64
        Toncat：9.0.8-winx64
        Jenkins：Latest
        Svnadmin：3.0.6
        Unity：2017.3.1f1 (64-bit)
    

#### 安装 [Uinty](https://unity3d.com/cn/get-unity/download/archive)

    :: 默认方式安装
    :: 安装路径：C:\Program Files\Unity
    :: 激活：必须是 pro 版本

#### 安装 [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

    :: 默认方式安装
    :: 配置环境变量
    JAVA_HOME=C:\Program Files\Java\jdk1.8.0_171
    CLASSPATH=%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar
    Path=;%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin

    :: 验证安装 CMD:
    java -version

#### 安装 [Subversion](https://sourceforge.net/projects/win32svn/files/1.8.17/Setup-Subversion-1.8.17.msi)

    :: 默认方式安装
    :: 创建数据目录
    md D:\SvnData
    
    :: 安装服务
    sc create svn binpath= "C:\Program Files (x86)\Subversion\bin\svnserve.exe --service -r D:\SvnData" displayname= "Subversion Server" start= auto depend= Tcpip
    
    :: 启动服务
    net start svn

#### 安装 [Toncat](http://mirrors.hust.edu.cn/apache/tomcat/tomcat-9/v9.0.8/bin/apache-tomcat-9.0.8.exe)

    # 默认方式安装
    # 配置管理界面
    # 添加登录用户账号密码 C:\Program Files\Apache Software Foundation\Tomcat 9.0\conf\tomcat-users.xml
        <user username="admin" password="admin" roles="admin-gui,manager-gui"/>
    
    :: 方法1：修改 C:\Program Files\Apache Software Foundation\Tomcat 9.0\conf\Catalina\localhost
        :: 在上面目录中创建 manager.xml 内容为：
            <Context privileged="true" antiResourceLocking="false"
                  docBase="${catalina.home}/webapps/manager">
                <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="^.*$" />
            </Context>
        :: 在上面目录中创建 host-manager.xml 内容为：
            <Context privileged="true" antiResourceLocking="false"
                  docBase="${catalina.home}/webapps/host-manager">
                <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="^.*$" />
            </Context>
    
    :: 方法2：修改 C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps
        :: 更改 manager\META-INF\context.xml 文件中 allow 字段
            allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
        :: change
            allow="^.*$" />
        :: or
            allow="192.168.0.*" />
        
        :: 更改 host-manager\META-INF\context.xml 文件中 allow 字段
            allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
        :: change
            allow="^.*$" />
        :: or
            allow="192.168.0.*" />
    :: 修改完成后重启服务
    net stop Tomcat9
    net start Tomcat9

#### 安装 [MariaDB](https://mirrors.shu.edu.cn/mariadb//mariadb-10.3.7/winx64-packages/mariadb-10.3.7-winx64.msi)

> 如果需要更改数据库存储路径需要更改注册表(服务启动时指定的配置文件)

    :: 默认方式安装
    :: 添加环境变量
    Path=;C:\Program Files\MariaDB 10.3\bin
    
    :: 停止 Mysql服务
    net stop mysql
    
    :: 创建配置文件 C:\Program Files\MariaDB 10.3\data\my.ini
    :: CMD:
    set config="C:\Program Files\MariaDB 10.3\data\my.ini"
    echo [mysql]> %config%
    echo # 设置mysql客户端默认字符集>> %config%
    echo default-character-set=utf8>> %config%
    echo. >> %config%
    echo [mysqld]>> %config%
    echo # 设置3306端口>> %config%
    echo port=3306>> %config%
    echo character-set-server=utf8>> %config%
    echo # 设置mysql的安装目录>> %config%
    echo basedir=C:/Program Files/MariaDB 10.3>> %config%
    echo # 设置mysql数据库的数据的存放目录>> %config%
    echo datadir=C:/Program Files/MariaDB 10.3/data>> %config%
    echo # 允许最大连接数>> %config%
    echo max_connections=20>> %config%
    echo # 服务端使用的字符集默认为8比特编码的latin1字符集>> %config%
    echo character-set-server=utf8>> %config%
    echo # 创建新表时将使用的默认存储引擎>> %config%
    echo default-storage-engine=INNODB>> %config%
    echo innodb_buffer_pool_size=511M>> %config%
    echo. >> %config%
    echo [client]>> %config%
    echo port=3306>> %config%
    echo default-character-set=utf8>> %config%
    echo plugin-dir=C:/Program Files/MariaDB 10.3/lib/plugin>> %config%
    # END CMD

    :: 启动 Mysql服务
    net start mysql

#### 安装 [Svnadmin](https://github.com/jsvnadmin/jsvnadmin/releases/download/3.0.6/svnadmin-3.0.6.zip)

    :: 解压到当前文件夹然后移动到桌面
    :: 登陆数据库
    mysql -uroot -proot
    
    :: 查看字符集
    show variables like 'character%';

    :: 创建数据库
    create database if not exists svnadmin default character set utf8;

    :: 创建用户
    create user 'svnadmin'@'localhost' identified by 'svnadmin';

    :: 给用户授权
    grant all privileges on svnadmin.* to 'svnadmin'@'localhost';

    :: 选择数据库
    use svnadmin;

    :: 导入svnadmin数据库
    source C:\Users\Administrator\Desktop\svnadmin-3.0.6\db\mysql5.sql

    :: 导入汉化语言
    source C:\Users\Administrator\Desktop\svnadmin-3.0.6\db\lang\en.sql

    :: 复制 war 包到 Tomcat webapps目录
    exit;
    copy C:\Users\Administrator\Desktop\svnadmin-3.0.6\svnadmin.war "C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps\svnadmin.war"
    
    :: 编辑数据库配置文件 C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps\svnadmin\WEB-INF\jdbc.properties
    db=MySQL

    MySQL.jdbc.username=svnadmin
    MySQL.jdbc.password=svnadmin

    
    :: 重启服务
    net stop tomcat9
    net start tomcat9
    
    :: 初始化
    :: 浏览器打开：http://192.168.2.90:8080/svnadmin/  admin/admin 初始化
    
    :: 创建一个测试项目
        项目：test    类型：svn
        路径：D:\SvnData\test
        URL：svn://192.168.2.90/test
        描述：测试
        点击：提交
        
    :: 设置项目权限
        :: 在刚创建好的项目上点击设置权限
        :: 点击选择资源：[test:/]
        :: 点击右侧 账户 admin → >
        :: 权限：可读可写
        :: 点击保存

#### 运行 [Jenkins](http://mirrors.jenkins.io/war-stable/latest/jenkins.war)

    :: 将下载的 jenkins.war 放到桌面
    :: 配置工作目录
    JENKINS_HOME=D:\JenkinsWorkSpace
    
    :: 重启 Toncat
    net stop tomcat9
    net start tomcat9
    
    :: 复制 war 包到 Tomcat webapps目录
    copy C:\Users\Administrator\Desktop\jenkins.war "C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps\jenkins.war"

    :: 浏览器打开 http://192.168.2.90:8080/jenkins 根据提示粘贴解锁密码 Jenkins
    
    :: 配置 [Jenkins]
        :: 1.安装推荐的插件    
        :: 输入初始化账号信息
        :: 安装 Uity3D, Poll SCM 插件
            :: 点击菜单栏 系统管理 → 插件管理 → 可选插件
    
        :: 配置全局工具
            :: 点击菜单栏 系统管理 → 全局工具配置 → Unity3d
                别名：Unity 2017.3.1f1 (64-bit) 
                安装目录：C:\Program Files\Unity
    
    :: 新建任务
        :: 点击菜单栏 新建任务
            任务名称：Unity3D
            选择：构建一个自由风格的软件项目 点击确定
            
            源码管理：Subversion
                Repository URL: svn://192.168.2.90/test
                Credentials: 点击 Add → Jenkins
                    Username: admin
                    password: admin
                    Add
                选择添加的 admin/**** 账号
                
            Check-out Strategy: Emulate clean checkout by first deleting unversioned/ignored files, then 'svn update'
            
            构建环境：
                勾选：Delete workspace before build starts
            
            点击增加构建步骤选择 Invoke Unity3d Editot
                Unity3d installation name: Unity 2017.3.1f1 (64-bit)
                Editor command line arguments:  -quit -batchmode -executeMethod ProjectBuild.BuildForStandaloneWindows
    :: 点击保存 
    
    :: SVN 上传源码
    
    :: 开始构建

#### 构建失败

    :: 查看构建日志(可能是某些服务未启动导致)
    :: Toncat 必须以管理员身份运行