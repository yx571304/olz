---
title: linux常用命令
tags:
  - linux
categories:
  - linux
abbrlink: 767f
date: 2018-01-29 01:55:20
---
>Linux常用命令记录

## 压缩解压 tar

### 常用参数

`-c` 意为 create，表示创建压缩包
`-x` 意为 extract，表示解压
`-t` 表示查看内容
`-r` 给压缩包追加文件
`-u` 意为 update，更新压缩包中的文件

>注意，上面是一定要五选一，不能一个都不选，也不能同时选俩。但是自选的部分就可以按照需要挑选了，比如：

`-z` 使用 gzip 属性
`-j` 使用 bz2 属性
`-Z` 使用 compress 属性
`-v` 意为 verbose，显示详细的操作过程
`-O` 将文件输出到标准输出
`-C` 指定解压目录

### 示例

>假设我们有很多 `.md` 文件需要打包，那么可以使用(`-c` 是创建压缩包)

    tar -cf posts.tar *.md

>然后我们发现还有一些 `.txt` 文件也需要打包进去，那么可以使用(`-r` 是追加文件)

    tar -rf posts.tar *.txt

>然后我们发现 `hello.md` 弄错了，修正后需要更新到压缩包中，可以使用(`-u` 是更新)

    tar -uf post.tar hello.md

>压缩好了，我们来看看压缩包的内容，可以使用(`-t` 是列出文件内容)

    tar -tf posts.tar

>解压，可以使用(`-x` 是解压)

    tar -xf posts.tar
    
>解压到指定位置，可以使用(`-C` 是解压到指定目录 注意指定的目录必须存在否则会报错)

    tar -xf posts.tar -C /data
    

>`tar.gz` 相关(`-c` 压缩 `-x`解压)

    tar -czf posts.tar.gz *.md
    tar -xzf posts.tar.gz
    
>`tar.bz2` 相关(`-c` 压缩 `-x`解压)

    tar -cjf posts.tar.bz2 *.md
    tar -xjf posts.tar.bz2

>`tar.Z` 相关(`-c` 压缩 `-x`解压)

    tar -cZf posts.tar.Z *.md
    tar -xZf posts.tar.Z

### 总结
>遇到不同类型的文件，请用不同的参数来应对：

    *.tar -> tar -xf
    *.tar.gz -> tar -xzf
    *.tar.bz2 -> tar -xjf
    *.tar.Z -> tar -xZf
    *.gz -> gzip -d
    *.rar -> unrar e
    *.zip -> unzip
    
## 空间占用 `du`
>很多时候磁盘被写满了是各种奇怪错误的源头，所以如何快速找到问题所在就很重要了，

    # 显示当前文件下 Top 10 空间占用的文件/目录，
    # s 表示不显示每个子目录或文件的大小
    # h 表示用更加自然的方式显示（比如 K/M/G 这样）
    du -sh * | sort -nr | head
    
## 系统状态 top
>了解系统状态一般少不了 `top`命令，虽然基本上不需要做任何操作，但是还是有一些可以自定义的内容的，比如

    # 进入系统状态显示后，具体值代表的意思是
    # PR    进程优先级，越小优先级越高
    # VIRT  占用的虚拟内存
    # RES   占用的物理内存
    # SHR   占用的共享内存
    # S     进程状态（S - 休眠，R - 正在运行，Z - 僵死状态，N - 优先级为负数）
    # TIME+ 进程启动后占用的总 CPU 时间
    
    # 可以按照不同的指标排序显示，按对应键即可
    # P     按照 CPU 使用率排序
    # T     按照 MITE+ 排序
    # M     按内存使用占比排序

>查看某用户的进程

    top -u root

>查看进程内存分布(`12345` 是进程号)

    pmap -d 12345

>按照内存排序，这里的 `grep` 可以过滤特定的用户

    ps -e -o 'user,uid,pid,pcpu,rsz,vsz,stime,comm,cmd' | grep root | sort -nrk5

## 查找 find
>在命令行界面中进行查找是每个需要在服务器上执行操作的同学都必备的技能，这里用实例来说明常见的实用用法

>找出七天前的文件

    # / 表示从根目录中查找
    # -type f 表示找出系统普通文件，不包含目录
    # -mtime +n 表示寻找 n 天前的数据
    # -print 打印文件名称
    find / -type f -mtime +7 -print
    

>找出并删除七天前的文件

    # -exec 表示后面执行系统命令
    # {} 只有该符号能跟在命令你后面
    # \; 结束符号
    find /Hexo -type f -mtime +7 -print -exec rm -f {} \;
    find /Hexo -type f -mtime +7 -print | xargs rm -f
    
> 其他示例

    # 使用管道和 xargs = -exec
    # 查找 /var 下最大的十个文件
    find /var -type f -ls | sort -k 7 -r -n | head
    
    # 查找 /var/log 下大于 5GB 的文件
    find /var/log/ -type f -size +5120M -exec ls -lh {} \;
    
    # 找出今天所有文件并将它们拷贝到另一个目录
    find /home/wdxtub/ -ctime 0 -print -exec cp {} /mnt/backup/{} \;
    
## 远程登录 ssh

    ssh -vvv username@ip ssh 的 debug 模式
    ssh -i key.pem username@ip 用 pem key 登录 ssh

## 奇技
>这里是一些比较零碎的命令行技巧

>`openssl`产生随机的十六进制数，n 是字符数

    openssl rand -hex n
    
>在当前 shell 里执行一个文件里的命令

    source /path/to/filename
    
>截取变量的前五个字符   

    ${variable:0:5}

>用 wget 抓取完整的网站目录结构，存放到本地目录中

    wget -r --no-parent --reject "index.html*" http://hostname/ -P /home/user/dirs 

>一次创建多个目录

    mkdir -p /home/wdxtub/{test0,test1,test2}
    
>测试硬盘写入速度

    dd if=/dev/zero of=/tmp/output.img bs=8k count=256k; rm -rf /tmp/output.img 

>测试硬盘读取速度

    hdparm -Tt /dev/sda
    
>获取文本的 md5

    echo -n "test" | md5sum

>获取 HTTP 头信息

    curl -I http://wdxtub.com
    
>显示所有 tcp4 监听端口

    netstat -tln4 | awk '{print $4}' | cut -f2 -d: | grep -o '[0-9]*' 

>查看命令的运行时间

    time command
    
>查看所有的环境变量

    export

>文件内容对比

    cmp file1 file2

>内容前面会显示行号

    cat -n file

>查看 22 端口现在运行的程序

    lsof -i:22

>进程现在打开的文件

    lsof -c abc 显示 abc

>看进程号为 12 的进程打开了哪些文件

    lsof -p 12

>设置默认网关(临时)

    route add default gw 192.168.1.4

>设置默认网关(永久)

    echo "GATEWAY=192.168.0.1" >>/etc/sysconfig/network

>增加swap分区

    swapfile="/home/swapfile"
    dd if=/dev/zero of=$swapfile bs=1M count=8192
    /sbin/mkswap $swapfile
    chown root:root $swapfile
    chmod 600 $swapfile
    /sbin/swapon $swapfile
    echo "$swapfile          swap                    swap    defaults        0 0" >>/etc/fstab

本文内容来自[小士刀](http://wdxtub.com/2017/07/10/linux-operation-guide/)
