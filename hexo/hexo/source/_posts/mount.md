title: Linux挂载Windows文件共享
cwd: /Hexo
tags:
  - mount
  - Linux
categories:
  - linux
abbrlink: c34c
date: 2018-01-03 11:53:35
---
### 摘要
>它用于挂载Linux系统外的文件，挂载源可以是块设备(/dev/sdc1)，NFS(nfs.test.com:/dir)等

#### 命令格式：

    mount [-t types] [-o options] <source dir>  <target dir>

>挂载成功后原 dir 里面的 内容/属主/权限 将被屏蔽，直到此设备被卸载。


#### 命令参数：
`-t types` 指定文件系统的类型，通常不必指定。mount 会自动选择正确的类型。常用类型有：

| 参数        |     说明              |
| ----------- | --------------------- |
|`iso9660`    | `光盘或镜像文件`      |
|`msdos`      | `DOS Fat16文件系统`   |
|`vfat`       | `Win 9x Fat32文件系统`|
|`ntfs`       | `Win NT NTFS文件系统` |
|`nfs`        | `UNIX(Linux)网络共享` |
|`cifs`       | `Windows 网络共享`    |

`-o options` 主要用来描述设备或档案的挂接方式。常用的参数有：

| 参数        |     说明                      |
| ----------- | ----------------------------- |
|`ro`         |`只读方式挂载设备`             |
|`rw`         |`可读可写方式挂载设备`         |
|`iocharset`  |`指定访问文件系统所用的字符集` |

#### 使用`shell`命令临时挂载

    mkdir /tmp/mount
    mount -o username=user,password=passwd,domain=domain //192.168.0.6/Shares /tmp/mount/       # 此处的/tmp/mount权限为user用户在domain域/主机上的权限
    mount -o username=user,password=passwd,domain=domain,ro //192.168.0.6/Shares /tmp/mount/        # 只读挂载



#### 开机自动挂载

    echo "//192.168.0.6/Shares /tmp/mount    cifs    auto,username=user,domain=domain,password=passwd,uid=1000,gid=1000,iocharset=utf8,file_mode=0770,dir_mode=0770   0 0" >>/etc/fstab
    mount -a

#### 扩展
>当使用ls命令不能列出挂载的文件夹时重新挂载

    mount -o remount,rw /tmp/mount