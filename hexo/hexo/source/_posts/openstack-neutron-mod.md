title: Neutron 服务的网络模式(Pike)
tags:
  - neutron
  - openstack
copyright: true
categories:
  - openstack
abbrlink: neutron-mod
date: 2018-08-02 15:51:11
---
### 说明

> [官方文档](https://docs.openstack.org/neutron/queens/install/controller-install-option1-rdo.html#configure-the-server-component)

> 网络选项1：部署了最简单的架构，该架构仅支持将实例附加到提供商（外部）网络。没有自助（私有）网络，路由器或浮动IP地址。只有该admin特权用户或其他特权用户才能管理提供商网络。

> 网络选项2：使用支持将实例附加到自助服务网络的第3层服务来增强选项1。该demo非特权用户或其他非特权用户可以管理自助服务网络，包括提供自助服务和提供商网络之间连接的路由器。此外，浮动IP地址提供与使用来自外部网络（如Internet）的自助服务网络的实例的连接。

> 自助服务网络通常使用覆盖网络。诸如VXLAN之类的覆盖网络协议包括额外的报头，这些报头增加了开销并减少了有效载荷或用户数据的可用空间。在不了解虚拟网络基础设施的情况下，实例尝试使用1500字节的默认以太网最大传输单元（MTU）来发送分组。Networking服务自动通过DHCP为实例提供正确的MTU值。但是，某些云映像不使用DHCP或忽略DHCP MTU选项，需要使用元数据或脚本进行配置。

### 安装组件

    yum install openstack-neutron openstack-neutron-ml2 openstack-neutron-linuxbridge ebtables

### 创建用户,数据库,授权(MariaDB)

    mysql -u root -predhat -te "
    create database neutron;
    grant all privileges on neutron.* to 'neutron'@'localhost' identified by 'neutron';
    grant all privileges on neutron.* to 'neutron'@'%' identified by 'neutron';"

### 创建服务凭据

    source ~/admin-openstack.sh
    openstack user create --domain default --password=neutron neutron
    openstack role add --project service --user neutron admin
    openstack service create --name neutron --description "OpenStack Networking" network
    openstack endpoint create --region RegionOne network public http://controller:9696
    openstack endpoint create --region RegionOne network internal http://controller:9696
    openstack endpoint create --region RegionOne network admin http://controller:9696

### 网络选项1：提供商网络

#### neutron配置

    cat <<EOF >/etc/neutron/neutron.conf
    [DEFAULT]
    bind_port = 9696
    bind_host = $(hostname)
    core_plugin = ml2
    service_plugins =
    transport_url = rabbit://openstack:openstack@controller
    auth_strategy = keystone
    notify_nova_on_port_status_changes = true
    notify_nova_on_port_data_changes = true

    [keystone_authtoken]
    auth_uri = http://controller:5000
    auth_url = http://controller:35357
    memcached_servers = controller1:11211
    auth_type = password
    project_domain_name = default
    user_domain_name = default
    project_name = service
    username = neutron
    password = neutron

    [nova]
    auth_url = http://controller:35357
    auth_type = password
    project_domain_id = default
    user_domain_id = default
    region_name = RegionOne
    project_name = service
    username = nova
    password = nova

    [database]
    connection = mysql://neutron:neutron@controller:3306/neutron

    [oslo_concurrency]
    lock_path = /var/lib/neutron/tmp
    EOF

#### 配置2层网络插件

    cat <<EOF  >/etc/neutron/plugins/ml2/ml2_conf.ini
    [ml2]
    tenant_network_types =
    type_drivers = flat,vlan
    mechanism_drivers = linuxbridge
    extension_drivers = port_security
    
    [ml2_type_flat]
    flat_networks = provider
    
    [securitygroup]
    enable_ipset = true
    EOF
---

> 链接配置文件

    ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini
---

#### 配置Linux桥代理

> NetName 为底层提供网络的物理接口(此处一般选公网网卡)

    NetName=ens34

    cat <<EOF  >/etc/neutron/plugins/ml2/linuxbridge_agent.ini
    [linux_bridge]
    physical_interface_mappings = provider:$NetName
    
    [vxlan]
    enable_vxlan = false
    
    [securitygroup]
    enable_security_group = true
    firewall_driver = neutron.agent.linux.iptables_firewall.IptablesFirewallDriver
    EOF
    
#### 开启内核支持网桥过滤器

    cat <<EOF >>/etc/sysctl.conf
    net.bridge.bridge-nf-call-iptables = 1
    net.bridge.bridge-nf-call-ip6tables = 1
    EOF
    sysctl -p

#### 配置DHCP代理

    cat <<EOF  >/etc/neutron/dhcp_agent.ini
    [DEFAULT] 
    interface_driver = linuxbridge 
    dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq 
    enable_isolated_metadata = true
    EOF
---

#### 配置元数据代理

    cat <<EOF  >/etc/neutron/metadata_agent.ini
    [DEFAULT]
    nova_metadata_ip = controller
    metadata_proxy_shared_secret = metadata
    EOF

#### 配置计算服务网络

    cat <<EOF  >>/etc/nova/nova.conf
    
    # Neutron
    [neutron]
    url = http://controller:9696
    auth_url = http://controller:35357
    auth_type = password
    project_domain_name = default
    user_domain_name = default
    region_name = RegionOne
    project_name = service
    username = neutron
    password = neutron
    service_metadata_proxy = true
    metadata_proxy_shared_secret = metadata
    EOF

#### 初始化数据库

    su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf \
      --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron

#### 服务配置

    # 重启 nova-api
    systemctl restart openstack-nova-api
    
    # 启动服务,跟随系统启动
    systemctl enable neutron-server neutron-linuxbridge-agent neutron-dhcp-agent neutron-metadata-agent
    systemctl start neutron-server neutron-linuxbridge-agent neutron-dhcp-agent neutron-metadata-agent

### 网络选项2：自助服务网络

#### neutron 配置

    cat <<EOF >/etc/neutron/neutron.conf
    [DEFAULT]
    bind_port = 9696
    bind_host = $(hostname)
    core_plugin = ml2
    service_plugins = router
    allow_overlapping_ips = true   
    transport_url = rabbit://openstack:openstack@controller
    auth_strategy = keystone
    notify_nova_on_port_status_changes = true
    notify_nova_on_port_data_changes = true


    [keystone_authtoken]
    auth_uri = http://controller:5000
    auth_url = http://controller:35357
    memcached_servers = controller1:11211
    auth_type = password
    project_domain_name = default
    user_domain_name = default
    project_name = service
    username = neutron
    password = neutron

    [nova]
    auth_url = http://controller:35357
    auth_type = password
    project_domain_id = default
    user_domain_id = default
    region_name = RegionOne
    project_name = service
    username = nova
    password = nova

    [database]
    connection = mysql://neutron:neutron@controller:3306/neutron

    [oslo_concurrency]
    lock_path = /var/lib/neutron/tmp
    EOF

#### 配置2层网络插件

    cat <<EOF  >/etc/neutron/plugins/ml2/ml2_conf.ini
    [ml2]
    tenant_network_types = vxlan
    type_drivers = flat,vlan,vxlan
    mechanism_drivers = linuxbridge,l2population
    extension_drivers = port_security
    
    [ml2_type_flat]
    flat_networks = provider
    
    [ml2_type_vxlan]
    vni_ranges = 1:1000
    
    [securitygroup]
    enable_ipset = true
    EOF
---

> 链接配置文件

    ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini
---

#### 配置Linux桥代理

> NetName 为底层提供网络的物理接口(此处一般选公网网卡)

    NetName=ens34
    
    # 获取第二块网卡IP
    IP=$(ip add | grep global | grep -v 'secondary' |awk -F'[ /]+' '{ print $3 }' | tail -1)
    
    cat <<EOF  >/etc/neutron/plugins/ml2/linuxbridge_agent.ini
    [linux_bridge]
    physical_interface_mappings = provider:$NetName
    
    [vxlan]
    enable_vxlan = true
    local_ip = $IP
    l2_population = true
    
    [securitygroup]
    enable_security_group = true
    firewall_driver = neutron.agent.linux.iptables_firewall.IptablesFirewallDriver
    EOF
    
#### 开启内核支持网桥过滤器

    cat <<EOF >>/etc/sysctl.conf
    net.bridge.bridge-nf-call-iptables = 1
    net.bridge.bridge-nf-call-ip6tables = 1
    EOF
    sysctl -p

#### 配置第3层代理

    cat <<EOF  >/etc/neutron/l3_agent.ini
    [DEFAULT] 
    interface_driver  =  linuxbridge
    EOF

#### 配置DHCP代理

    cat <<EOF  >/etc/neutron/dhcp_agent.ini
    [DEFAULT] 
    interface_driver = linuxbridge 
    dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq 
    enable_isolated_metadata = true
    EOF

#### 配置元数据代理

    cat <<EOF  >/etc/neutron/metadata_agent.ini
    [DEFAULT]
    nova_metadata_ip = controller
    metadata_proxy_shared_secret = metadata
    EOF

#### 配置计算服务网络

    cat <<EOF  >>/etc/nova/nova.conf
    
    # Neutron
    [neutron]
    url = http://controller:9696
    auth_url = http://controller:35357
    auth_type = password
    project_domain_name = default
    user_domain_name = default
    region_name = RegionOne
    project_name = service
    username = neutron
    password = neutron
    service_metadata_proxy = true
    metadata_proxy_shared_secret = metadata
    EOF

#### 初始化数据库

    su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf \
      --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron

#### 服务配置

    # 重启 nova-api
    systemctl restart openstack-nova-api
    
    # 启动服务,跟随系统启动
    systemctl enable neutron-server neutron-linuxbridge-agent neutron-dhcp-agent neutron-metadata-agent
    systemctl start neutron-server neutron-linuxbridge-agent neutron-dhcp-agent neutron-metadata-agent
    
    # 启动第3层服务(仅对于网络选项2)
    systemctl enable neutron-l3-agent
    systemctl start neutron-l3-agent