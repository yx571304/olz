title: LVM 分区调整
author: 亦 漩
abbrlink: 8d2e
tags:
  - Lvm
  - ext4
  - ext3
  - 分区扩容
categories:
  - LVM
date: 2018-06-14 17:10:00
---
## Linux LVM逻辑卷分区调整

### 命令说明

    lvextend -L 120G /dev/centos/home     # 增大至120G
    lvextend -L +20G /dev/centos/home     # 增加20G
    lvreduce -L 50G /dev/centos/home      # 减小至50G
    lvreduce -L -8G /dev/centos/home      # 减小8G
    
    # EXT3/4 文件系统(文件系统支持热增大和减小)
    resize2fs /dev/centos/home            # 执行调整
    
    # XFS 文件系统(注意：XFS文件系统在做减小操作时需备份[被减小分区]的数据,做减小分区操作则此分区必须格式化)
    xfs_growfs /dev/centos/home           # 执行调整


### 查看信息

#### 查看分区信息

    lsblk
    
    [root@localhost ~]# lsblk 
    NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    sda               8:0    0 298.1G  0 disk 
    ├─sda1            8:1    0     1G  0 part /boot
    └─sda2            8:2    0 297.1G  0 part 
      ├─centos-root 253:0    0    50G  0 lvm  /      # 根分区
      ├─centos-swap 253:1    0   3.9G  0 lvm  [SWAP] # swap分区
      └─centos-home 253:2    0 243.2G  0 lvm  /home  # home分区
    sdb               8:16   0  55.9G  0 disk 
    └─sdb1            8:17   0  55.9G  0 part /SSD

#### 查看文件系统

    df -hT
    
    [root@localhost ~]# df -hT
    文件系统                类型      容量  已用  可用 已用% 挂载点
    /dev/mapper/centos-root xfs        50G  1.5G   49G    3% /
    devtmpfs                devtmpfs  1.8G     0  1.8G    0% /dev
    tmpfs                   tmpfs     1.9G     0  1.9G    0% /dev/shm
    tmpfs                   tmpfs     1.9G  8.9M  1.9G    1% /run
    tmpfs                   tmpfs     1.9G     0  1.9G    0% /sys/fs/cgroup
    /dev/sdb1               xfs        56G  4.3G   52G    8% /SSD
    /dev/sda1               xfs      1014M  143M  872M   15% /boot
    /dev/mapper/centos-home xfs       244G  320M  243G    1% /home  # 分区类型XFS
    tmpfs                   tmpfs     371M     0  371M    0% /run/user/0

#### 查看LV路径

    lvdisplay
    
    [root@localhost ~]# lvdisplay
			--- Logical volume ---
			LV Path                /dev/centos/swap    # Swap区路径
			LV Name                swap
			VG Name                centos
			LV UUID                8YWTtu-YeqR-Gt0d-bO8L-bBeb-fb1B-nFsoQS
			LV Write Access        read/write
			LV Creation host, time localhost.localdomain, 2018-06-14 13:18:02 +0800
			LV Status              available
			# open                 2
			LV Size                <3.88 GiB
			Current LE             993
			Segments               2
			Allocation             inherit
			Read ahead sectors     auto
			- currently set to     256
			Block device           253:1
			 
			--- Logical volume ---
			LV Path                /dev/centos/home    # home区路径
			LV Name                home
			VG Name                centos
			LV UUID                eUprjm-hGrH-1xO7-FBrC-AEpK-qTcY-NSeTTP
			LV Write Access        read/write
			LV Creation host, time localhost.localdomain, 2018-06-14 13:18:02 +0800
			LV Status              available
			# open                 0
			LV Size                <243.21 GiB
			Current LE             62261
			Segments               3
			Allocation             inherit
			Read ahead sectors     auto
			- currently set to     256
			Block device           253:2
			 
			--- Logical volume ---
			LV Path                /dev/centos/root    # 根分区路径
			LV Name                root
			VG Name                centos
			LV UUID                YZv2zk-pf1A-XwxZ-HGDk-Xftu-TRgo-VoePMp
			LV Write Access        read/write
			LV Creation host, time localhost.localdomain, 2018-06-14 13:18:04 +0800
			LV Status              available
			# open                 1
			LV Size                50.00 GiB
			Current LE             12800
			Segments               1
			Allocation             inherit
			Read ahead sectors     auto
			- currently set to     256
			Block device           253:0

      free -m
      
			[root@localhost ~]# free -m
										total        used        free      shared  buff/cache   available
			Mem:           3709         105        3476           8         127        3409
			Swap:          8067           0        8067


### XFS LVM


> XFS文件系统, xfs因为只能增大, 如果要减小, 只能在减小后将逻辑分区重新格式化(mkfs.xfs)才能挂载
> 示例：备份 home 分区数据, 减小home分区 4G 增加到Swap 分区, 格式化分区, 挂载, 恢复数据(系统：CentOS 7.5)

#### 备份数据

    mkdir -p /home/{1..100}/{1..1000}    # 创建测试文件
    yum -y install xfsdump               # 安装备份软件
    xfsdump -f /home.xfsdump /home       # 将home 分区备份到 根路径下 /home.xfsdump


#### 减小home分区空间

    umount /home                          # 卸载分区
    lvreduce -L -4G /dev/centos/home      # 减小home分区4G
    mkfs.xfs -f /dev/centos/home          # 格式化home分区
    mount /home                           # 挂载分区
    
#### 增加Swap分区空间

    swapoff /dev/centos/swap                  # 关闭swap分区
    lvextend -l +100%FREE /dev/centos/swap    # 将剩余所有空间添加到swap分区
    mkswap /dev/centos/swap                   # 格式化分区
    swapon /dev/centos/swap                   # 启用swap分区

#### 恢复home数据

    xfsrestore -f /home.xfsdump /home


#### 查看结果

> 为了确认分区正常请重启系统

    lsblk
    
    [root@localhost ~]# lsblk 
		NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
		sda               8:0    0 298.1G  0 disk 
		├─sda1            8:1    0     1G  0 part /boot
		└─sda2            8:2    0 297.1G  0 part 
			├─centos-root 253:0    0    50G  0 lvm  /
			├─centos-swap 253:1    0   7.9G  0 lvm  [SWAP]
			└─centos-home 253:2    0 239.2G  0 lvm  /home
		sdb               8:16   0  55.9G  0 disk 
		└─sdb1            8:17   0  55.9G  0 part /SSD

#### 减小Swap分区增加根分区

> 此处不用备份信息(减小分区为swap)

    swapoff /dev/centos/swap                  # 关闭swap分区
    lvreduce -L -4G /dev/centos/swap          # 减小swap分区4G
    mkswap /dev/centos/swap                   # 格式化swap分区4G
    swapon /dev/centos/swap                   # 启用swap分区
    
    
    lvextend -l +100%FREE /dev/centos/root    # 将剩余所有空间添加到根分区
    xfs_growfs /dev/centos/root               # 应用扩容
    
    # 查看信息
    lsblk
    
    [root@localhost ~]# lsblk 
		NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
		sda               8:0    0 298.1G  0 disk 
		├─sda1            8:1    0     1G  0 part /boot
		└─sda2            8:2    0 297.1G  0 part 
			├─centos-root 253:0    0    54G  0 lvm  /
			├─centos-swap 253:1    0   3.9G  0 lvm  [SWAP]
			└─centos-home 253:2    0 239.2G  0 lvm  /home
		sdb               8:16   0  55.9G  0 disk 
		└─sdb1            8:17   0  55.9G  0 part /SSD
