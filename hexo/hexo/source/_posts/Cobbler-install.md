---
title: Cobbler自动化部署
tags:
  - linux
  - cobbler
  - 自动化部署
categories:
  - cobbler
abbrlink: 1ab8
date: 2018-01-17 22:15:31
---
>`Cobbler`是一个`Linux` 配置服务器，可以使用诸如`DHCP`，`TFTP`和`DNS`等服务从中心点简化和自动化多个计算机操作系统的基于网络的系统安装。可以使用`Xen`，`KVM`或`VMware`配置`PXE`，重新安装和虚拟化`guest` 虚拟机。`Cobbler`与程序进行交互以重新安装和虚拟化支持。和`Cobbler`使用`libvirtkoankoan`整合不同的虚拟化软件。`Cobbler`能够管理复杂的网络场景，如绑定以太网链路上的桥接

#### 安装`cobbler`

```bash
yum install cobbler cobbler-web dhcp tftp-server pykickstart httpd xinetd -y
systemctl start xinetd
systemctl enable xinetd
systemctl start httpd
systemctl enable httpd
systemctl start cobblerd
systemctl enable cobblerd
```

#### 配置`cobbler`
>使用`openssl`对密码加密（前面的`admin`是加密随机值，后面的`admin`是密码）

```bash
sed -i "s/^next_server:.*/next_server: 192.168.3.10/" /etc/cobbler/settings
sed -i "s/^server:.*/server: 192.168.3.10/" /etc/cobbler/settings
sed -i "s/^server:.*/server: 192.168.3.10/" /etc/cobbler/settings
sed -i "s/^manage_dhcp:.*/manage_dhcp: 1/" /etc/cobbler/settings
pass=$(openssl passwd -1 -salt 'admin' 'admin')
sed -i "s@^default_password_crypted:.*@default_password_crypted: $pass@" /etc/cobbler/settings
```

#### 配置`tftp`

```bash
sed -i "/disable/s/yes/no/" /etc/xinetd.d/tftp
```

#### 启动`rsync`服务设置开机启动

```bash
systemctl start rsyncd
systemctl enable rsyncd
```

#### 下载`cobbler`相关的包;配置

```bash
cobbler get-loaders
systemctl restart cobblerd
cobbler check
```

#### 配置`DHCP`主要修改`网段`,`DNS`,`网关`等信息 

```bash
sed -i "21,25s/^/#/" /etc/cobbler/dhcp.template
sed -i "25a subnet 192.168.3.0 netmask 255.255.255.0 {" /etc/cobbler/dhcp.template
sed -i "26a\     option routers            192.168.3.1;" /etc/cobbler/dhcp.template
sed -i "27a\     option domain-name-servers 192.168.0.5;" /etc/cobbler/dhcp.template
sed -i "28a\     option subnet-mask         255.255.255.0;" /etc/cobbler/dhcp.template
sed -i "29a\     range dynamic-bootp        192.168.3.30 192.168.3.50;" /etc/cobbler/dhcp.template
```

#### 挂载并导入数据
>导入镜像需要等待一会;`cobbler`导入的镜像路径：`/var/www/cobbler/ks_mirror`

```bash
mkdir /mnt/centos7.4
mount /dev/cdrom /mnt/centos7.4
cobbler import --path=/mnt/centos7.4 --name=CentOS-7.4 --arch=x86_64
ls /var/www/cobbler/ks_mirror
```

#### 查看导入的镜像信息
```bash
cobbler list
cobbler profile report
cobbler profile edit --help
```

#### 指定无人值守文件;修改内核网卡名称
```bash
cp -a /var/lib/cobbler/kickstarts/sample_end.ks /var/lib/cobbler/kickstarts/CentOS-7.4-x86_64.cfg
cobbler profile edit --name=CentOS-7.4-x86_64 --kickstart=/var/lib/cobbler/kickstarts/CentOS-7.4-x86_64.cfg
cobbler profile edit --name=CentOS-7.4-x86_64 --kopts='net.ifnames=0 biosdevname=0'
cobbler profile report
```

#### 重新启动服务
```bash
systemctl restart cobblerd
cobbler sync
systemctl restart xinetd
```


#### web管理

    https://192.168.3.10/cobbler_web
    默认username: cobblerd
    默认password: cobblerd

#### 客户端使用`koan`重装系统
>`--list=profiles` 查看服务器镜像列表
>`--profile=CentOS-7.4-x86_64`使用此镜像重装系统

```bsh
yum install koan
koan --server=192.168.3.10 --list=profiles
koan --server=192.168.3.10 --profile=CentOS-7.4-x86_64 --replace-self
```