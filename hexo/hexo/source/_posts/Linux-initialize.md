title: 实验环境下Linux初始化
tags:
  - Linux初始化
  - Redhat初始化
  - CentOS初始化
categories:
  - linux
abbrlink: e131
date: 2017-12-19 10:46:48
---
### 配置yum源
>备份系统原有yum源

    mkdir /etc/yum.repos.d/OldRepo
    mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/OldRepo/

>配置阿里云yum源

    curl -o /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    curl -o /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo

>生成缓存

    yum makecache

### 设置系统时间及时间同步
>安装ntp软件包

    yum -y install ntpdate

>同步系统时间,将系统时间同步到硬件

    ntpdate pool.ntp.org
    hwclock -w

>自动同步系统/硬件时间(20分钟)

    echo "*/20 * * * * $(which ntpdate) pool.ntp.org > /dev/null 2>&1 && $(which hwclock) -w" >> /var/spool/cron/root
    chmod 600 /var/spool/cron/root

### 关闭防火墙,Selinux
>临时关闭防火墙,selinux

    systemctl stop firewalld
    setenforce 0

>永久关闭防火墙,selinux

    systemctl disable firewalld
    sed -i "s;SELINUX=enforcing;SELINUX=disabled;g" /etc/selinux/config

### 软件

    yum update   
    yum -y install vim lsof htop lrzsz