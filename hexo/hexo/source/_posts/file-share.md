title: Samba NSF 安装及挂载
author: 亦 漩
abbrlink: d4c3
tags:
  - NFS
  - Samba
  - Autofs
categories:
  - FilesService
date: 2018-06-14 18:01:00
---
### Samba安装

> 环境：CentOS 7.5    IP：192.168.2.18

#### 安装配置

    # 安装
    yum -y install samba
    
    # 备份配置文件
    cp /etc/samba/smb.conf{,.bak}
    
    # 创建用户[test:test]
    useradd -m -U test
    echo test | passwd --stdin  test
    
    # 创建数据目录，配置权限
    mkdir /data
    chown -Rf test:test /data
    
    # 添加共享目录
    # comment  # 共享说明
    # path    # 路径
    # public   # 只有登陆后才显示
    # writable  # 允许写入
    
    cat <<'EOF' >>/etc/samba/smb.conf

    [data]
        comment = Share data
        path = /data
        public = no
        writable = yes
    EOF

    # 创建Samba用户[此处密码可与本地用户密码不同]
    pdbedit -a -u test
    
    # 启用服务, 开机自启动服务
    systemctl restart smb
    systemctl enable smb

#### Samba挂载

> Linux 示例:

    # 安装
    yum -y install cifs-utils
    
    # 创建密码文件
    cat <<'EOF'  >auth.smb
    username=test
    password=test
    domain=SAMBA
    EOF
    
    # 设置密码文件权限
    chmod -Rf 600 auth.smb
    
    # 创建挂载目录
    mkdir /data
    
    # 命令行挂载
    mount -t cifs  //192.168.2.18/data /data -o username=test,password=test
    mount -t cifs  //192.168.2.18/data /data -o credentials=/root/auth.smb
    
    # 写入开机自动挂载
    echo "//192.168.2.18/data /data cifs  credentials=/root/auth.smb  0 0" >>/etc/fstab
    mount -a


### NFS 安装

#### 安装配置

    # 安装
    yum -y install nfs-utils
    
    # 创建数据目录,设置权限
    mkdir /SSD
    chmod -Rf 777 /SSD
    
    # 配置连接参数
    # ro	只读
    # rw	读写
    # root_squash       当NFS客户端以root管理员访问时，映射为NFS服务器的匿名用户
    # no_root_squash    当NFS客户端以root管理员访问时，映射为NFS服务器的root管理员
    # all_squash        无论NFS客户端使用什么账户访问，均映射为NFS服务器的匿名用户
    # sync              同时将数据写入到内存与硬盘中，保证不丢失数据
    # async             优先将数据保存到内存，然后再写入硬盘；这样效率更高，但可能会丢失数据
    # 本地路径 /SSD 允许 192.168.2.0网络所有主机访问,权限读写,当NFS客户端以root管理员访问时，映射为NFS服务器的匿名用户
    echo '/SSD 192.168.2.*(rw,sync,root_squash)' > /etc/exports
    
    # 启用服务,开机自启动
    systemctl restart rpcbind
    systemctl enable rpcbind
    systemctl start nfs-server
    systemctl enable nfs-server
    
    # 查看状态信息
    showmount -e 192.168.2.18    # 显示NFS服务器的共享列表
    showmount -a 192.168.2.18    # 显示本机挂载的文件资源的情况NFS资源的情况

#### NFS 挂载

> Linux 示例：

    # 安装
    yum -y install nfs-utils
    
    # 命令行挂载
    mkdir /SSD
    mount -t nfs 192.168.2.18:/SSD /SSD
    
    # 开机自动挂载
    echo '192.168.2.18:/SSD  /SSD  nfs defaults  0 0' >>/etc/fstab
    mount -a

### Autofs 自动挂载

> Autofs与Mount/Umount的不同之处在于，它是一种看守程序。如果它检测到用户正试图访问一个尚未 挂接的文件系统，它就会自动检测该文件系统，如果存在，那么Autofs会自动将其挂接。另一方面，如果它检测到某个已挂接的文件系统在一段时间内没有被使用，那么Autofs会自动将其卸载。因此一旦运行了Autofs后，用户就不再需要手动完成文件系统的挂接和卸载(节省服务器资源不用时刻连接)

    # 安装
    yum -y install autofs
    
    # 添加iso 文件自动挂载 [在主配置文件添加配置文件路径,注意配置文件名不要与现有脚本文件重名]
    sed -i '6a /media   /etc/iso.misc' /etc/auto.master
    
    # 创建挂载配置信息
    cat <<'EOF' >/etc/iso.misc
    iso   -fstype=iso9660,ro,nosuid,nodev :/dev/cdrom
    EOF
    
    # 启动服务,开机自动开启服务
    systemctl start autofs
    systemctl enable autofs
    
    
    # 添加NFS 自动挂载
    sed -i '14a /-   /etc/nfs.net' /etc/auto.master
    
    # 创建挂载配置信息
    cat <<'EOF' >/etc/nfs.net
    /SSD  -fstype=nfs,rw,soft,intr,vers=4.1 192.168.2.18:/SSD
    EOF
    
    # 重启服务
    systemctl restart autofs
    
    
    # 添加Samba 自动挂载
    sed -i '15a /-   /etc/cifs.net' /etc/auto.master
    
    # 创建挂载配置信息
    cat <<'EOF' >/etc/cifs.net
    /data  -fstype=cifs,rw,noperm,credentials=/root/auth.smb ://192.168.2.18/data
    #/data  -fstype=cifs,rw,noperm,user=test,pass=test ://192.168.2.18/data
    EOF
    
    # 重启服务
    systemctl restart autofs

---

### 补充示例

> 允许匿名访问公共资源读写, 允许密码登陆访问家目录,公共资源,及指定目录的读写
> test用户拥有 /samba/data /data/public /home/test 文件夹读写权限
> 匿名用户拥有 /data/public 文件夹读写权限(windows 访问时输入任意字母做用户名密码为空)

#### 配置本地权限

> 创建公共组`sambashare`

    yum -y install samba                 # 安装软件
    useradd -m -U test                   # 创建用户test
    echo test | passwd --stdin  test     # 设置test用户密码为test
    groupadd -g 65530 sambashare         # 创建 sambashare 组
    usermod -a -G sambashare test        # 将 test 用户添加到 sambashare 组
    pdbedit -a -u test                   # 创建samba 用户 [输入 test Samba密码 可与本地密码不同]

#### Samba 配置

    # 备份配置文件
    cp /etc/samba/smb.conf{,.bak}
    
    # 创建数据目录,配置权限
    mkdir -p /samba/data
    mkdir /samba/public
    chown -Rf test:sambashare /samba/data
    chown -Rf nobody:sambashare /samba/public
    chmod -Rf 770 /samba
    ls -la /samba
    
    # 创建测试文件
    touch /samba/data/mount /samba/public/mount
    
    # Samba 配置文件
    cat <<'EOF' >/etc/samba/smb.conf
    [global]
        workgroup = SAMBA
        server string = Samba Server Version %v
        log file = /var/log/samba/log.%m
        max log size = 50
        security = user
        passdb backend = tdbsam
        
        map to guest = bad user
        guest account = nobody
        
        load printers = yes
        cups options = raw
        
    [homes]
        comment = Home Directories
        valid users = %S, %D%w%S
        browseable = No
        read only = No
        inherit acls = Yes
        
    [data]
        comment = Directory: /samba/data
        path = /samba/data
        public = no
        writable = yes
        browseable = no
        
    [public]
        comment = Directory: /samba/public
        path = /samba/public
        public = yes
        writable = yes
        guest ok = yes
        browseable = yes
    EOF
    
    # 启动服务
    systemctl restart smb
    systemctl enable smb

#### 挂载

    # 创建密码文件
    cat <<'EOF'  >/root/auth.smb
    username=test
    password=test
    domain=SAMBA
    EOF
    
    # 设置文件权限
    chmod -Rf 600 auth.smb
    
    # 创建挂载目录
    mkdir -p /samba/public /samba/data
    
    # 创建测试文件
    touch /samba/data/no /samba/public/no
    
    
    # 命令行挂载(匿名访问输入任意字符做密码)
    mount -t cifs  //192.168.2.18/public /samba/public
    mount -t cifs  //192.168.2.18/data /samba/data -o credentials=/root/auth.smb
    mount -t cifs  //192.168.2.18/data /samba/data -o username=test,password=test
    
    
    # 开机自动挂载
    mkdir /data
    echo "//192.168.2.18/data /samba/data cifs  credentials=/root/auth.smb  0 0" >>/etc/fstab
    mount -a
    
    # Autofs 自动挂载
    yum -y install autofs
    sed -i '18a /-   /etc/cifs.net' /etc/auto.master
    
    cat <<'EOF' >/etc/cifs.net
    /samba/data  -fstype=cifs,rw,noperm,credentials=/root/auth.smb ://192.168.2.18/data
    EOF
    
    systemctl restart autofs
     

