title: shell题
author: 亦 漩
abbrlink: '6583'
tags:
  - shell
categories: []
date: 2018-07-25 18:28:00
---
## 随机数

>随机产生 a-z 10 位小写字符串

    head /dev/urandom | tr -dc a-z | head -c 10
    head /dev/urandom | tr -dc a-z | tr -d s-w | head -c 10    # 排除s-w字符串
---

>随机产生 0-9 10 位数字

    head /dev/urandom | tr -dc 0-9 | head -c 10
    head /dev/urandom | tr -dc 0-9 | tr -d 4,5 | head -c 10   # 排除4和5
---

>随机产生数字,字母大小写 20位字符串

    head /dev/urandom | tr -cd a-zA-Z0-9 | head -c 20
---
    
> 创建10个指定文件名例如 [hnzlorcbhm_data.html]要求前十位为随机小写加指定后缀[_data.html]

    for i in {1..10};do 
        touch $(head /dev/urandom | tr -dc a-z | tr -d s-w | head -c 10)_data.html
    done
---
    
## 统计文件中单词,字母

> 统计字符串中出现的单词,字母

    str="the squid project provides a number of resources toassist users design,implement and support squid installations. \
    Please browsethe documentation and support sections for more infomation"
---

> 按单词出现的频率降序排序

    echo $str|sed 's#[^a-zA-Z]#\n#g'|grep -v "^$"|sort|uniq -c|sort -rn -k1
    echo $str|tr ' ' '\n'|sort|uniq -c|sort -rn -k1
---
 
> 按字母出现的频率降序排序

    echo $str|grep -o "."|egrep -v "[^a-zA-Z]"|sort|uniq -c|sort -rn -k1
    echo $str|sed -r 's#(.)#\1\n#g'|egrep -v "[^a-zA-Z]|^$"|sort|uniq -c|sort -rn -k1
---

## 日期

> 打印指定年份

    date '+%Y%m%d' -d '+10 year'       # 10年后日期
    date '+%Y%m%d' -d '-10 year'       # 10年前日期
---

> 打印指定月之后的日期

    date '+%Y%m%d' -d '+1 month'        # 1个月后日期
    date '+%Y%m%d' -d '-1 month'        # 1个月前日期
---

> 打印指定天的日期

    date '+%Y%m%d' -d '+10 day'        # 10 天后日期
    date '+%Y%m%d' -d '-10 day'        # 10 天前日期
---

> 打印指定小时时间

    date '+%H%M%S' -d '+2 hour'        # 2小时后时间
    date '+%H%M%S' -d '-2 hour'        # 2小时前时间
---

> 打印指定分时间

    date '+%H%M%S' -d '+5 minute'      # 5分钟后的时间
    date '+%H%M%S' -d '-5 minute'      # 5分钟前的时间
---

### 打印指定日期

> 打印出 15 天前到当前 的日期(从0开始包含当前日期)

    for i in {0..15}; do
        date -d "+$i day $(date +%Y%m%d)" +%Y%m%d
    done
---

> 打印出当前时间到 15 天后的时间

    for i in {0..15}; do
        date -d "+$i day $(date +%Y%m%d)" +%Y%m%d
    done
---

> 打印 20180101 之后的 15 天

    for i in {0..15}; do
        date -d "+$i day 20180101" +%Y%m%d
    done
---

> 打印 20180101 之前的 15 天

    for i in {0..15}; do
        date -d "-$i day 20180101" +%Y%m%d
    done
---

> 计算某年有多少天(已知每年最后一天为 12/31 %j 获取当前时间是一年中的第几天)

    date -d "20001231" +%j

> 将指定日期时间转换为 unix 时间戳

    date -d '20180727 23:51:00' +%s
---

> 将指定 unix 时间戳转换为日期时间格式

    date -d '@1532706660' +'%Y-%m-%d %H:%M:%S'
    date -d '@1532706660' +%Y                    # 获取时间戳年份
    date -d '@1532706660' +%m                    # 获取时间戳月份
    date -d '@1532706660' +%w                    # 获取时间戳当天礼拜几
    date -d '@1532706660' +%j                    # 获取时间戳是当年的第几天
---

#### 写一个脚本计算某个日期到当前有多少天

    cat <<'EOF'  >./sum_day.sh
    #!/bin/bash
    
    s_Y=$(date -d "$1" +%Y)                     # 获取输入的年份
    e_Y=$(date +%Y)                             # 获取当前年份
    s_j=$(date -d "$1" +%j | sed 's/^0//')      # 取出输入的日期是当年的第几天(若不满100去除首部的0)
    e_j=$(date +%j | sed 's/^0//')              # 获取当前日期是今年的第几天
    a=$s_Y
    
    # for 循环方式
    for ((a=$s_Y; a<=$e_Y; a++ ));do
        let sum=$sum+$(date -d ${a}1231 +%j)    # 累加输入的年份到当前年份天数(已知每年最后一天为 1231 +%j 获取每年的天数)
        echo $a -----  $(date -d ${a}1231 +%j)   ------ $sum
    done
    
    # while 循环方式
    #while ((a<=$e_Y)); do
    #    let sum=$sum+$(date -d ${a}1231 +%j)
    #    echo $a -----  $(date -d ${a}1231 +%j)   ------ $sum
    #    let a++ ;
    #done
    
    let e_d=$(date -d ${s_Y}1231 +%j)-$e_j      # 获取今年剩余天数
    echo  "从 ${s_Y}0101 到 ${e_Y}1231 一共有 $sum 天"
    echo  "输入的日期在当年是第: $s_j 天"
    echo  "今年剩余: $e_d  天"
    let sum=$sum-$s_j-$e_d                      # 用累加天数减 输入日期当年的天数 减去 今年剩余天数
    echo "从 $1 到今天 $(date +%Y%m%d) 一共有 $sum 天"
    EOF
    
    # 使用方法
    sh ./sum_day.sh 19910119
    
    # 测试结果
    date -d "-10052 day $(date +%Y%m%d)" +%Y%m%d        # 请替换10052 为上面计算出来的值