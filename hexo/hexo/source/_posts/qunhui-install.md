---
title: 黑群晖NAS安装
tags:
  - linux
  - NAS
categories:
  - NAS
abbrlink: '1654'
date: 2018-03-12 23:25:33
---
{% raw %}
<style>
img{
    width: 85%;
}
</style>
{% endraw %}


### 简介

[群晖科技](https://www.synology.cn)股份有限公司（品牌名称：Synology，简称群晖或群晖科技）为台湾一家网络附加储存（NAS）服务商，总部位于台北市。截至2017年，群晖以Synology品牌行销全球，在中小企业及家用NAS市场占有率居世界首位。网络连接存储设备（Network Attached Storage，NAS），是一种专门的数据存储技术的名称，它可以直接连接在电脑网络上面，对异质网络用户提供了集中式数据访问服务

### 功能

支持的文件服务: SMB, AFR, NFS, FTP, TFTP, Rsync, ISCSI
支持登陆认证方式: 域, LDAP, OSS, 本地账户
数据备份: Win/linu环境下数据备份,支持增量备份, NAS之间数据备份, 客户端同步NAS软件....
服务器: Active Dirertory Server(AD域), Dirertory Server(LDAP), DSN Server, Proxy Server, RADIUS Server ....
支持大量第三方软件: Apache, PHP, MariaDB, Python, Ruby, Node.js, Java, Perl, Docker, 创建/管理虚拟机....

### 为什么用黑群
> [白群:官方硬件+官方软件]   [黑群:组装硬件+破解官方软件]

白群: 硬件配置低,价格高,可以正常升级不会导致系统无法引导,可以使用群晖自带QC功能实现远程管理(速度慢)
黑群: 自定义硬件(普通PC电脑),升级系统之前做好数据备份,需要使用第三方软件实现远程管理功能

### 下载资源

下载所需软件[百度网盘](https://pan.baidu.com/s/1rIt_JQay2mAKewjcxb5VLg)[密码: t140] 
>目前黑群晖用的较多的有两个版本DS3615xs,DSM3617xs(本文基于DS3615xs做测试)

    文件说明:
      黑群晖3615xs_v613_15152   必须使用使用15152版本安装,文件内的镜像为grub方式引导,安装后不能更新版本
      黑群晖3615xs_v6通用EFI方式    可以使用当前最新版本系统安装,可更新补丁大版本更新貌似不行
      黑群晖 For VMware WorkStation5.1.zip  5.1版本安装教程,包含虚拟机(版本5不带docker等功能)
      Win10_WebUI   一个模仿win10系统的webUI页面可用来做NAS网站首页(通过Web Station创建站点)
      Plex  WEB视频管理软件,功能强大

### 虚拟机安装测试
>使用`VMware Workstation Pro`创建虚拟机,虚拟机启动镜像来自`黑群晖3615xs_v613_15152`目录

#### 配置拟机

    客户机操作系统：Linux
    版本：其他 Linux 3.x 内核 64 位
    内存：2-4G
    网络连接：使用桥接(能通内网)
    磁盘大小：50G左右
    添加CD/DVD驱动器：使用镜像boot_ds3615xs_v613_15152.iso文件(勾选启动时连接)

![](/images/qunhui_install_00.png)

#### 设置从光盘启动
>为什么要BLIOS设置为光盘启动：系统安装完毕后引导还在光盘中,虚拟机默认第一启动项为硬盘,所以需要BIOS中强制设置光盘为第一启动

1.虚拟机名称右键`电源`→`打开电源时进入固件`

![](/images/qunhui_install_01.png)

2.使用键盘方向键选择`boot`→`CD-ROM Drive`,然后按`Shift`＋`=`组合键将`CD-ROM Drive`调整到第一启动,按下`F10`→`Enter`.

![](/images/qunhui_install_02.png)
<hr/>
![](/images/qunhui_install_03.png)

#### 安装NAS系统

1.下载NSA下安装文件[下载地址](https://archive.synology.com/download/DSM/release/6.1.3/15152/) 此处用到的是`DSM_DS3615xs_15152.pat`
<hr/>
2.在物理电脑上安装群`晖助手软件`(SynologyAssistantSetup),运行后在软件内右键`搜索`,这时可以看到发现一台设备,点击发现的设备右键选择`安装`.

![](/images/qunhui_install_04.png)

3.点击`浏览`选择下载好的安装文件`DSM_DS3615xs_15152.pat`.

![](/images/qunhui_install_05.png)

4.点击下一步,输入管理账号:`admin`的密码,点击下一步(此账号密码为web管理员)

![](/images/qunhui_install_06.png)

5.配置网络参数,与当前网络同一个网络能相互访问,点击完成

![](/images/qunhui_install_07.png)

6.等待安装完毕点击`(登陆 DiskStation)`通过浏览器初始化NAS

![](/images/qunhui_install_08.png)

#### 初始化

1.输入管理员账号密码

    user: admin
    passwd: 安装NAS系统[4]中设置的密码

![](/images/qunhui_install_09.png)

2.点击下一步,然后勾选下载DSM更新并进行手动安装(自动安装有可能导致无法引导);设置QC跳过(黑群晖无法验证账号)初始化完成

![](/images/qunhui_install_10.png)


#### 其他设置

1.配置邮箱
>打开`控制面板`→`通知设置`进行邮箱的配置

![](/images/qunhui_install_11.png)

2.开启ssh
>打开`控制面板`→`终端和 SNMP`勾选开启ssh

![](/images/qunhui_install_12.png)

3.将引导写入硬盘
>使用admin连接到服务器,打开终端输入`ssh admin@192.168.0.200`然后输入密码,（必须开启ssh）

![](/images/qunhui_install_13.png)

>输入`sudo -i`切换到root

![](/images/qunhui_install_14.png)

>打开`Everything`点击`工具`勾选`HTTP服务器`开启http服务

![](/images/qunhui_install_15.png)


>将 `boot_ds3615xs_v613_15152.img`,`disk_setboot.sh` 下载到当前目录(在浏览器输入本机IP定位到软件目录复制需要下载的文件链接)


``` bash
wget http://192.168.0.51/E:/黑群晖3615xs_v613_15152/boot_ds3615xs_v613_15152.img
wget http://192.168.0.51/E:/黑群晖3615xs_v613_15152/disk_setboot.sh
chown root:root boot_ds3615xs_v613_15152.img disk_setboot.sh
chmod 777 disk_setboot.sh
chmod 666 boot_ds3615xs_v613_15152.img
```

![](/images/qunhui_install_16.png)

>创建硬盘启动(创建完毕后即可以硬盘启动)

```
# 在硬盘/dev/sda 创建一个新主分区将img镜像写入分区
./disk_setboot.sh /dev/sda boot_ds3615xs_v613_15152.img
```

![](/images/qunhui_install_17.png)


### 使用U盘安装(UEFI)
>此处文件来源于`黑群晖3615xs_v6通用EFI方式`目录

#### 准备工作

1.准备一个U盘;
<hr/>
2.百度网盘软件说明: 

    Win32_Disk_Imager:  镜像写入U盘工具
    DS3615xs-6.1:   引导镜像文件本实验选择的是ds3615xs(注意选择的引导镜像,安装的系统与之对应)
    SynologyAssistantSetup: 局域网搜索群晖NAS设备软件
    ChipGenius: 查看USB设备ID工具

3.群晖系统[下载](https://archive.synology.com/download/DSM/release/6.1.5/15254/) 此处用到的是`DSM_DS3615xs_15254.pat`下载到本地

#### 制作U盘启动

1.插入U盘,运行Win32DiskImager,ImageFile:选择群晖U盘引导镜像`黑群晖3615xs_v6通用EFI方式\DS3615xs-6.1\synoboot.img`目录下的,Device:设备选择到用作启动盘U盘,点击`Write`.

![](/images/qunhui_install_18.png)

2.运行ChipGenius, 记录下U盘中设备ID中VID及PID的值 `VID=0930,PID=6544`

![](/images/qunhui_install_19.png)

3.修改U盘EFI分区中grub目录下的grub.cfg文件中VID,PID替换为上一步记录的值,保存并退出,U盘启动制作完成(分区大小为14.9M的分区,注意: VID,PID的值前面加 0x 表示16进制值)

![](/images/qunhui_install_20.png)

#### 安装系统(UEFI)

1.准备一套键鼠,显示器接入NAS,连接上路由器，路由器打开DHCP,另外一台操作的电脑在同一局域网下.
<hr/>
2.将制作好的引导U盘插入到准备做NAS的设备上并以U盘启动,看到类似下面画面就可以用另外一台电脑操作了.(此处未做截图,引用上面实验图)
![](/images/qunhui_install_03.png)

3.在同一网段内的电脑上安装群晖助手软件(SynologyAssistantSetup),运行后在软件内右键搜索,这时可以看到发现一台设备,点击发现的设备右键选择安装.
![](/images/qunhui_install_04.png)

4.点击`浏览`选择下载好的安装文件`DSM_DS3615xs_15254.pat`.(此处未做截图,引用上面实验图)
![](/images/qunhui_install_05.png)

5.点击下一步,输入管理账号:`admin`的密码,点击下一步
![](/images/qunhui_install_06.png)

6.配置网络参数,与当前网络同一个网络能相互访问,点击完成
![](/images/qunhui_install_07.png)

7.等待安装完毕点击`(登陆 DiskStation)`通过浏览器初始化NAS

![](/images/qunhui_install_08.png)

### 软件推荐
>套件中心搜索

    Active Backup for Server：
        1.实现了 Windows 和 Linux 服务器的集中保护，较大限度降低了部署以及管理成本
        2.能够按天备份文件，而不必在源服务器上安装复杂又昂贵的代理，也不用担心会对客户端性能和代理兼容性造成影响
        3.可使用柱状图工具从时间和任务角度监控所有备份任务
        4.提供多版本、镜像及增量三种模式，可满足大多数常见的备份需求

    Virtual Machine Manager：
        1.在Synology NAS 上创建和运行多个虚拟机
        2.借助Virtual Machine Manager，您可以更灵活地分配硬件资源、构建虚拟化环境
        3.在主机之间迁移Virtual DSM 而不发生中断。

    Directory Server：
        1.Directory Server 提供的LDAP 服务包含集中化的权限控制、认证及帐号管理机制(LDAP Server)
        2.NAS支持LDAP客户端验证

    Active Directory Server：
        1.Active Directory Server 提供了由 Samba 驱动的 Active Directory（AD）域服务
        2.支持常用的 Active Directory 功能，如用户帐户、群组成员、加入域的 Windows、Linux 和 Synology DSM、Kerberos 验证及群组策略等
        3.NAS 支持加入域

    Cloud Station Server：
        类似网盘,可多个客户端之间跨平台同步数据(速度有点慢)

    Docker：
        Docker 是一个轻量级虚拟化应用程序，它可让您运行世界各地的开发人员在 DSM 上创建的数以千计的容器。Docker Hub 是使用广泛的内置映像存储库，可让您从其他优秀开发人员那里找到共享的应用程序

### 总结

使用`grub`方式安装好后可以将引导写入硬盘,不可以更新软件版本只能安装15152
使用`EFI` 方式安装需依赖U盘才能启动,可以更新补丁,可以直接安装最新版系统
<hr/>
参考资料

    [ DSM系统下载](https://archive.synology.com/download/DSM/release/)  [使用官网最新镜像地址替换路径可以达到更快下载速度]
    [教程参考1](http://www.nasyun.com/thread-31655-1-1.html)
    [教程参考2](http://www.nasyun.com/thread-28943-1-1.html)
    [硬件参考2](https://post.smzdm.com/p/120480/p5/)
