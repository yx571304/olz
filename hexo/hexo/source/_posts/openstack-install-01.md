title: OpenStack集群安装-01_系统环境初始化
tags:
  - openstack
  - haproxy
copyright: true
categories:
  - openstack
  - ''
abbrlink: openstack-01
date: 2018-07-18 19:05:00
---
> 所有节点安装前需执行此页面命令初始化环境(免密码登录只需要在 controller1 执行)

### 关闭禁用服务
    
    # 关闭, 禁用 firewalld
    systemctl stop firewalld
    systemctl disable firewalld
    firewall-cmd --state
    
    # 关闭, 禁用 selinux
    setenforce 0
    sed -i '/^SELINUX=.*/c SELINUX=disabled' /etc/selinux/config
    grep --color=auto '^SELINUX' /etc/selinux/config
    

### 配置YUM源

    # 安装 epel-release
    yum -y install epel-release
    
    # 删除默认yum, epel 源
    rm -f /etc/yum.repos.d/*
    
    # 下载阿里云yum, epel 源
    curl -so /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    curl -so /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/Centos-7.repo /etc/yum.repos.d/epel-7.repo
    
    # 手动配置阿里云openstack源(关闭GPG验证 否则软件安装时会报错)
    cat <<'EOF' >/etc/yum.repos.d/centos-openstack.repo
    [centos-openstack-pike]
    name=CentOS-$releasever-openstack-pike
    enabled=1
    failovermethod=priority
    baseurl=http://mirrors.aliyun.com/centos/$releasever/cloud/$basearch/openstack-pike
    gpgcheck=0
    #gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

    [centos-virt-kvm-common]
    name=CentOS-$releasever-virt-kvm-common
    enabled=1
    failovermethod=priority
    baseurl=https://mirrors.aliyun.com/centos/$releasever/virt/$basearch/kvm-common/
    gpgcheck=0
    #gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7
    EOF
    
    # 更新yum 缓存
    yum clean all
    yum makecache


### 常用工具

    yum -y install wget vim ntpdate net-tools lsof


### 时间同步

    /usr/sbin/ntpdate ntp6.aliyun.com 
    echo "*/3 * * * * /usr/sbin/ntpdate ntp6.aliyun.com  &> /dev/null" > /tmp/crontab
    crontab /tmp/crontab


### 配置内核参数

    cat <<EOF >>/etc/security/limits.conf
    * soft nofile 65536  
    * hard nofile 65536 
    EOF

    cat <<EOF >>/etc/sysctl.conf
    fs.file-max=655350  
    net.ipv4.ip_local_port_range = 1025 65000  
    net.ipv4.tcp_tw_recycle = 1 
    EOF

    sysctl -p

### 配置免密码登录

> 仅 controller1 主机执行 controller1 登录其他主机无需密码

    # 添加 HOST 解析
    cat <<EOF >>/etc/hosts
    10.0.0.10      controller    # vip
    192.168.0.11   controller1
    192.168.0.12   controller2
    192.168.0.13   controller3
    192.168.0.20   nfs
    192.168.0.21   cinder1
    192.168.0.31   computer01
    192.168.0.32   computer02
    EOF
    

    # 生成密钥
    [ -f ~/.ssh/id_dsa ] || { ssh-keygen -t dsa -P '' -f ~/.ssh/id_dsa; }

    # 安装 expect
    [ -f /usr/bin/expect ] || { yum -y install expect; }
    
    # 远程主机root密码
    rootpass=redhat
    
    SSH_KEY(){
    /usr/bin/expect <<EOF
    set timeout 30
    spawn ssh-copy-id -i /root/.ssh/id_dsa.pub root@$1
    expect {
        "(yes/no)?" { send "yes\r"; exp_continue }
        "password:" { send "$rootpass\r" }
    }
    expect eof
    EOF
    }

    # 发送公钥到远程主机
    SSH_KEY controller1
    SSH_KEY controller2
    SSH_KEY controller3
    
    # 验证
    ssh controller1 "hostname"
    ssh controller2 "hostname"
    ssh controller3 "hostname"

### 使用脚本

> 以下所有命令在 controller1 执行

> controller1 添加 HOST 解析

    cat <<EOF >>/etc/hosts
    10.0.0.10      controller    # vip
    192.168.0.11   controller1
    192.168.0.12   controller2
    192.168.0.13   controller3
    192.168.0.20   nfs
    192.168.0.21   cinder1
    192.168.0.31   computer01
    192.168.0.32   computer02
    EOF
---

>controller1 初始化系统环境

    curl http://home.onlycloud.xin/code/openstack-01_init.sh | sh
---

>controller1 免密码登录其他主机

    wget http://home.onlycloud.xin/code/SSH_KEY -O ~/SSH_KEY
    source ~/SSH_KEY
    SSH_KEY controller1
    SSH_KEY controller2
    SSH_KEY controller3
    
    # 验证
    ssh controller1 "hostname"
    ssh controller2 "hostname"
    ssh controller3 "hostname"
---

> controller2, 3 初始化系统环境

    ssh controller2 "curl http://home.onlycloud.xin/code/openstack-01_init.sh | sh"
    ssh controller3 "curl http://home.onlycloud.xin/code/openstack-01_init.sh | sh"
---

> 同步HOSTS 文件到 controller2, 3

    scp /etc/hosts controller2:/etc/
    scp /etc/hosts controller3:/etc/
