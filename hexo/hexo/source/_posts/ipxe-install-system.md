title: IPXE 网络安装系统
author: 亦 漩
abbrlink: 9a51
tags:
  - ipxe
  - linux
categories:
  - ipxe
date: 2018-06-20 16:29:00
---
### 配置环境变量

> TFTP，DHCP，NGINX IP：192.168.2.44

    SERVER_IP=192.168.2.44
    DHCP_NET=192.168.0.0
    DHCP_GWT=192.168.0.1
    DHCP_MASK=255.255.0.0
    DHCP_POOL='192.168.2.50 192.168.2.80'
    LOAD_SOURCE='192.168.2.3:8000'
    DHCP_DNS='192.168.0.9, 192.168.0.5'
    DHCP_DNS2='223.5.5.5, 223.6.6.6'


### 安装配置DHCP

    yum -y install dhcp


### 配置DHCP参数

    cat <<EOF >/etc/dhcp/dhcpd.conf
    allow booting;
    allow bootp;
    ddns-update-style interim;
    ignore client-updates;
    subnet $DHCP_NET netmask $DHCP_MASK {
        option subnet-mask $DHCP_MASK;
        option routers $DHCP_GWT;
        option domain-name "onewtech.com";
        option domain-name-servers $DHCP_DNS;
        range dynamic-bootp $DHCP_POOL;
        default-lease-time      21600;
        max-lease-time          43200;
        next-server             $SERVER_IP;
        filename                "pxelinux.0";
    }
    EOF

### 启动服务,添加到开机启

    systemctl start dhcpd
    systemctl enable dhcpd


### TFTP安装

    yum -y install tftp tftp-server xinetd
    sed -i '/disable/s/yes/no/'  /etc/xinetd.d/tftp

### 启动服务,添加到开机启

    systemctl start tftp
    systemctl enable tftp
    systemctl start xinetd
    systemctl enable xinetd

### 下载启动文件

    yum -y install wget
    wget -c -O /var/lib/tftpboot/pxelinux.0 http://202.38.93.94/boot/tftp/pxelinux.0

### 配置启动菜单

    mkdir /var/lib/tftpboot/pxelinux.cfg
    cat <<EOF >/var/lib/tftpboot/pxelinux.cfg/default
    default lpxe 
    prompt 0

    LABEL lpxe
    KERNEL menu.c32
    APPEND http://$SERVER_IP/boot/menu.cgi

    #APPEND http://202.38.93.94/boot/menu.cgi

    #LABEL lpxe
    #KERNEL pxechn.c32
    #APPEND lpxelinux.0 -c http://pxe.ustc.edu.cn/boot/menu.cgi -p http://pxe.ustc.edu.cn/boot/bin/
    EOF

### 安装 nginx

    yum install -y epel-release
    yum install -y nginx


### nginx开启列出目录

    sed -i '44a\        autoindex on;' /etc/nginx/nginx.conf
    sed -i '45a\        autoindex_exact_size off;' /etc/nginx/nginx.conf
    sed -i '46a\        autoindex_localtime on;' /etc/nginx/nginx.conf
    rm -rf /usr/share/nginx/html/*

### 启动服务添加到开机启动

    systemctl start nginx
    systemctl enable nginx

### 配置nginx启动菜单

    mkdir /usr/share/nginx/html/boot
    cat <<EOF >/usr/share/nginx/html/boot/menu.cgi
    default menu.c32

    MENU TITLE NETWORK SYSTEM Boot Service

    #MENU ROWS     15
    #MENU WIDTH     80
    #MENU HELPMSGROW 22
    #MENU TABMSGROW    23
    #MENU CMDLINEROW    23
    #MENU ALLOWOPTIONS 0
    #MENU VSHIFT    0
    #MENU HSHIFT    0

    MENU MARGIN 0
    MENU ROWS -9
    MENU TABMSG
    MENU TABMSGROW -3
    MENU CMDLINEROW -3
    MENU HELPMSGROW -4
    MENU HELPMSGENDROW -1

    MENU COLOR SCREEN 37;40
    MENU COLOR BORDER 34;40
    MENU COLOR TITLE 1;33;40
    MENU COLOR SCROLLBAR 34;46
    MENU COLOR SEL 30;47
    MENU COLOR UNSEL 36;40
    MENU COLOR CMDMARK 37;40
    MENU COLOR CMDLINE 37;40
    MENU COLOR TABMSG 37;40
    MENU COLOR DISABLED 37;40
    MENU COLOR HELP 32;40
    MENU COLOR HOTKEY 1;36;40

    # Windows PE
    MENU BEGIN ^Windows PE
    MENU LABEL ^Windows PE
    LABEL -
      MENU LABEL ..
      TEXT HELP
      <-- Back
      ENDTEXT
      MENU EXIT
    MENU SEPARATOR

    LABEL winpe31
      MENU LABEL Windows PE 3.1 x64
      TEXT HELP
      Based on Windows 7 SP1
      With GUI and base Windows components.
      ENDTEXT
      KERNEL http://$SERVER_IP/WinPE/pe31/memdisk iso raw
      INITRD http://$SERVER_IP/WinPE/pe31/pe31.iso 

    LABEL winpe51
      MENU LABEL Windows PE 5.1 x64
      TEXT HELP
      Based on Windows 8.1
      No GUI. Almost nothing...
      ENDTEXT
      KERNEL /ipxe/ipxe.lkrn
      INITRD /ipxe/pe51.ipxe

    LABEL winpe10
      MENU LABEL Windows EasyU
      TEXT HELP
      Based on Windows 10
      With GUI and base Windows components...
      ENDTEXT
      KERNEL http://$SERVER_IP/WinPE/pe31/memdisk iso raw
      INITRD http://$SERVER_IP/WinPE/pe31/EasyU_v3.3.iso
			
      MENU END
      MENU END
		
    # Linux Install
    MENU BEGIN ^Linux Install
    MENU LABEL ^Linux Install
    LABEL -
      MENU LABEL ..
      TEXT HELP
      <-- Back
      ENDTEXT
      MENU EXIT
    MENU SEPARATOR

    # CentOS
    MENU BEGIN ^CentOS Installers
    MENU LABEL ^CentOS Installers
      LABEL -
      MENU LABEL ..
      TEXT HELP
      <-- Back
      ENDTEXT
      MENU EXIT
    MENU SEPARATOR

    LABEL install-centos-7-x86_64-load
      MENU LABEL Install CentOS 7 x86_64-load
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://192.168.2.3:8000/centos/7/os/x86_64/images/pxeboot/vmlinuz net.ifnames=0 biosdevname=0
      INITRD http://192.168.2.3:8000/centos/7/os/x86_64/images/pxeboot/initrd.img ks=http://$SERVER_IP/boot/default.cfg
      APPEND method=http://192.168.2.3:8000/centos/7/os/x86_64

    LABEL install-centos-7-x86_64
      MENU LABEL Install CentOS 7 x86_64
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/centos/7/os/x86_64/images/pxeboot/vmlinuz
      INITRD http://mirrors.ustc.edu.cn/centos/7/os/x86_64/images/pxeboot/initrd.img
      APPEND method=http://mirrors.ustc.edu.cn/centos/7/os/x86_64

    LABEL install-centos-6-x86_64
      MENU LABEL Install CentOS 6 x86_64
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/centos/6/os/x86_64/images/pxeboot/vmlinuz
      INITRD http://mirrors.ustc.edu.cn/centos/6/os/x86_64/images/pxeboot/initrd.img
      APPEND method=http://mirrors.ustc.edu.cn/centos/6/os/x86_64

      MENU END

    MENU BEGIN ^Fedora Installers
    MENU LABEL ^Fedora Installers
    LABEL -
      MENU LABEL ..
      TEXT HELP
      <-- Back
      ENDTEXT
      MENU EXIT
    MENU SEPARATOR
    
    LABEL install-fedora-28-x86_64
      MENU LABEL Install Fedora 28 x86_64
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/fedora/releases/28/Everything/x86_64/os/images/pxeboot/vmlinuz
      INITRD http://mirrors.ustc.edu.cn/fedora/releases/28/Everything/x86_64/os/images/pxeboot/initrd.img
      APPEND method=http://mirrors.ustc.edu.cn/fedora/releases/28/Everything/x86_64/os/

    LABEL install-fedora-27-x86_64
      MENU LABEL Install Fedora 27 x86_64
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/fedora/releases/27/Everything/x86_64/os/images/pxeboot/vmlinuz
      INITRD http://mirrors.ustc.edu.cn/fedora/releases/27/Everything/x86_64/os/images/pxeboot/initrd.img
      APPEND method=http://mirrors.ustc.edu.cn/fedora/releases/27/Everything/x86_64/os/

    LABEL install-fedora-26-x86_64
      MENU LABEL Install Fedora 26 x86_64
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/fedora/releases/26/Everything/x86_64/os/images/pxeboot/vmlinuz
      INITRD http://mirrors.ustc.edu.cn/fedora/releases/26/Everything/x86_64/os/images/pxeboot/initrd.img
      APPEND method=http://mirrors.ustc.edu.cn/fedora/releases/26/Everything/x86_64/os/

    LABEL install-fedora-25-x86_64
      MENU LABEL Install Fedora 25 x86_64
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/fedora/releases/25/Everything/x86_64/os/images/pxeboot/vmlinuz
      INITRD http://mirrors.ustc.edu.cn/fedora/releases/25/Everything/x86_64/os/images/pxeboot/initrd.img
      APPEND method=http://mirrors.ustc.edu.cn/fedora/releases/25/Everything/x86_64/os/

      MENU END

    # Arch Linux
    MENU BEGIN ^Arch Linux Installers
    MENU LABEL ^Arch Linux Installers
    LABEL -
      MENU LABEL ..
      TEXT HELP
      <-- Back
      ENDTEXT
      MENU EXIT
    MENU SEPARATOR
    LABEL install-arch-i686
      MENU LABEL Install Arch Linux i686
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/archlinux/iso/archboot/latest/boot/vmlinuz_i686
      INITRD http://mirrors.ustc.edu.cn/archlinux/iso/archboot/latest/boot/initramfs_i686.img

    LABEL install-arch-x86_64
      MENU LABEL Install Arch Linux x86_64
      TEXT HELP
      No help for this item
      ENDTEXT
      KERNEL http://mirrors.ustc.edu.cn/archlinux/iso/archboot/latest/boot/vmlinuz_x86_64
      INITRD http://mirrors.ustc.edu.cn/archlinux/iso/archboot/latest/boot/initramfs_x86_64.img
		
      MENU END
      MENU END
    EOF

### 下载启动文件

    wget -r -c -p -np -nd -k -P /var/lib/tftpboot/ipxe http://202.38.93.94/boot/tftp/ipxe/
    wget -r -c -p -np -nd -k -P /var/lib/tftpboot http://202.38.93.94/boot/tftp/bin/
    rm -rf /var/lib/tftpboot/ipxe/index.html*
    rm -rf /var/lib/tftpboot/index.html*
    
### 下载WinPE启动文件

    mkdir -p /usr/share/nginx/html/WinPE/{pe31,pe51}
    cd /usr/share/nginx/html/WinPE/pe51
    wget -c http://202.38.93.94/boot/tftp/ipxe/wimboot
    wget -c http://202.38.93.94/boot/bin/live/Windows/pe51/BCD
    wget -c http://202.38.93.94/boot/bin/live/Windows/pe51/boot.sdi
    wget -c http://202.38.93.94/boot/bin/live/Windows/pe51/boot.wim 

    cd /usr/share/nginx/html/WinPE/pe31
    wget -c http://202.38.93.94/boot/bin/live/Windows/pe31.iso
    wget -c http://202.38.93.94/boot/tftp/bin/memdisk

### 配置WinPE镜像路径

    cat <<EOF >/var/lib/tftpboot/ipxe/pe51.ipxe
    #!ipxe

    :retry
    ifconf -c dhcp || goto retry
    kernel http://$SERVER_IP/WinPE/pe51/wimboot
    initrd http://$SERVER_IP/WinPE/pe51/BCD        BCD
    initrd http://$SERVER_IP/WinPE/pe51/boot.sdi   boot.sdi
    initrd http://$SERVER_IP/WinPE/pe51/boot.wim   boot.wim
    boot
    EOF

    cat <<EOF >/var/lib/tftpboot/ipxe/pe31.ipxe
    #!ipxe

    :retry
    ifconf -c dhcp || goto retry

    initrd http://$SERVER_IP/WinPE/pe31/pe31.iso
    kernel http://$SERVER_IP/WinPE/pe31/memdisk iso raw
    boot
    EOF

### 配置Centos自应答文件

> 全自动默认分区

    cat <<'EOF' >/usr/share/nginx/html/boot/default.cfg
    #version=DEVEL
    # System authorization information
    auth --enableshadow --passalgo=sha512
    # Use network installation
    #url --url="http://192.168.2.3:8000/centos/7/os/x86_64"
    # Use graphical install
    graphical
    # Run the Setup Agent on first boot
    firstboot --enable
    ignoredisk --only-use=sda
    # Keyboard layouts
    keyboard --vckeymap=cn --xlayouts='cn'
    # System language
    lang zh_CN.UTF-8
    #lang en_US.UTF-8
    # firewalld
    firewall --disabled
    # selinux
    selinux --disabled

    # Network information
    network  --bootproto=static --device=enp3s0 --gateway=192.168.2.254 --ip=192.168.2.18 --nameserver=202.103.24.68,192.168.0.9 --netmask=255.255.0.0 --ipv6=auto --activate
    network  --hostname=localhost.localdomain

    # Root password
    rootpw --iscrypted $6$NQNbZ6I7HZgr4Kk9$IWQb3QN355rDVnL1emQhgxRlF9DSnvjlU5ezbEhzehfwRjAOvmbWxKDtkum6tbUZr5tWvz61ZR8U7lirUapwK/
    # System services
    services --enabled="chronyd"
    # System timezone
    timezone Asia/Shanghai --isUtc
    # reboot
    reboot

    bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda --append="net.ifnames=0 biosdevname=0"
    autopart --type=lvm

    clearpart --all --initlabel --drives=sda


    ##########################################################################
    # System bootloader configuration
    #bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda

    #clearpart --all --initlabel --drives=sda

    #part pv.1888 --fstype="lvmpv" --ondisk=sda --size=304220
    #part /boot --fstype="xfs" --ondisk=sda --size=1024
    #volgroup centos --pesize=4096 pv.1888
    #logvol /  --fstype="xfs" --size=296024 --name=root --vgname=centos
    #logvol swap  --fstype="swap" --size=8192 --name=swap --vgname=centos
    ############################################################################
    %packages
    @^minimal
    @core
    chrony
    kexec-tools

    %end

    %post

    # init
    #curl http://192.168.2.3:8000/init.sh | sh
    #curl http://192.168.2.3:8000/eth.sh | sh

    %end

    %addon com_redhat_kdump --enable --reserve-mb='auto'

    %end

    %anaconda
    pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
    pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
    pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
    %end
    EOF

> 指定分区大小

    cat <<'EOF' >/usr/share/nginx/html/boot/ks.cfg
    #version=DEVEL
    # System authorization information
    auth --enableshadow --passalgo=sha512
    # Use network installation
    #url --url="http://192.168.2.3:8000/centos/7/os/x86_64"
    # Use graphical install
    graphical
    # Run the Setup Agent on first boot
    firstboot --enable
    ignoredisk --only-use=sda
    # Keyboard layouts
    keyboard --vckeymap=cn --xlayouts='cn'
    # System language
    lang zh_CN.UTF-8
    #lang en_US.UTF-8
    # firewalld
    firewall --disabled
    # selinux
    selinux --disabled

    # Network information
    network  --bootproto=static --device=enp3s0 --gateway=192.168.2.254 --ip=192.168.2.18 --nameserver=202.103.24.68,192.168.0.9 --netmask=255.255.0.0 --ipv6=auto --activate
    network  --hostname=localhost.localdomain

    # Root password
    rootpw --iscrypted $6$NQNbZ6I7HZgr4Kk9$IWQb3QN355rDVnL1emQhgxRlF9DSnvjlU5ezbEhzehfwRjAOvmbWxKDtkum6tbUZr5tWvz61ZR8U7lirUapwK/
    # System services
    services --enabled="chronyd"
    # System timezone
    timezone Asia/Shanghai --isUtc
    # reboot
    reboot

    #bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
    #autopart --type=lvm

    #clearpart --all --initlabel --drives=sda

    ##########################################################################
    # System bootloader configuration
    bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda

    clearpart --all --initlabel --drives=sda

    part pv.1888 --fstype="lvmpv" --ondisk=sda --size=304220
    part /boot --fstype="xfs" --ondisk=sda --size=1024
    volgroup centos --pesize=4096 pv.1888
    logvol /  --fstype="xfs" --size=296024 --name=root --vgname=centos
    logvol swap  --fstype="swap" --size=8192 --name=swap --vgname=centos
    ############################################################################
    %packages
    @^minimal
    @core
    chrony
    kexec-tools

    %end

    %post

    # init
    #curl http://192.168.2.3:8000/init.sh | sh
    #curl http://192.168.2.3:8000/eth.sh | sh

    %end

    %addon com_redhat_kdump --enable --reserve-mb='auto'

    %end

    %anaconda
    pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
    pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
    pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
    %end
    EOF


### 重启服务,添加到开机启动

    systemctl restart {dhcpd,tftp,xinetd,nginx}
    systemctl enable {dhcpd,tftp,xinetd,nginx}

---

### 从网络启动LiveCD

> 配置NFS环境系统启动时自动加载NFS存储中的文件来初始化系统 NFS Server IP: 192.168.2.18

#### 下载启动文件

>此操作在 192.168.2.44 服务器执行

    # Ubuntu64
    mkdir /usr/share/nginx/html/ubuntu64
    cd /usr/share/nginx/html/ubuntu64
    wget -c http://pxe.ustc.edu.cn/boot/bin/live/ubuntu64/casper/initrd.lz
    wget -c http://pxe.ustc.edu.cn/boot/bin/live/ubuntu64/casper/vmlinuz.efi
    wget -c http://pxe.ustc.edu.cn/boot/conf/preseed.ubuntu.ustc.cfg

    # Ubuntu32
    mkdir /usr/share/nginx/html/ubuntu32
    cd /usr/share/nginx/html/ubuntu32
    wget -c http://pxe.ustc.edu.cn/boot/bin/live/ubuntu32/casper/vmlinuz
    wget -c http://pxe.ustc.edu.cn/boot/bin/live/ubuntu32/casper/initrd.lz
    wget -c http://pxe.ustc.edu.cn/boot/conf/preseed.ubuntu.ustc.cfg
    


#### 安装配置NFS

    # 配置变量
    NFS_SERVER=192.168.2.18
    INIT_BOOT=192.168.2.44

    # 安装
    yum -y install nfs-utils

    # 创建数据目录,设置权限
    mkdir /SSD
    chmod -Rf 777 /SSD
    
    # 配置NFS 权限
    echo '/SSD 192.168.2.*(rw,sync,root_squash)' >/etc/exports
    
    # 启用服务,开机自启动
    systemctl restart rpcbind
    systemctl enable rpcbind
    systemctl start nfs-server
    systemctl enable nfs-server

#### IPXE启动菜单添加条目

> 此操作在 192.168.2.44 服务器执行

    cat <<EOF >>/usr/share/nginx/html/boot/menu.cgi
    MENU BEGIN ^Live Systems
    MENU LABEL ^Live Systems
    LABEL -
      MENU LABEL ..
      TEXT HELP
      <-- Back
      ENDTEXT
      MENU EXIT
    MENU SEPARATOR

    LABEL -
      MENU LABEL Ubuntu 16.04 amd64
      TEXT HELP
      NFS Server Ubuntu 16.04 amd64
      ENDTEXT
      KERNEL http://$INIT_BOOT/ubuntu64/vmlinuz.efi
      INITRD http://$INIT_BOOT/ubuntu64/initrd.lz
      APPEND boot=casper netboot=nfs nfsroot=$NFS_SERVER:/SSD/ubuntu64/ url=http://$INIT_BOOT/ubuntu64/preseed.ubuntu.ustc.cfg quiet splash locale=zh_CN

    LABEL -
      MENU LABEL Ubuntu 16.04 i386
      TEXT HELP
      NFS Server Ubuntu 16.04 i386
      ENDTEXT
      KERNEL http://$INIT_BOOT/ubuntu32/vmlinuz
      INITRD http://$INIT_BOOT/ubuntu32/initrd.lz
      APPEND boot=casper netboot=nfs nfsroot=$NFS_SERVER:/SSD/ubuntu32/ url=http://$INIT_BOOT/ubuntu32/preseed.ubuntu.ustc.cfg quiet splash locale=zh_CN
		
      MENU END
      MENU END
    EOF

### 下载NFS启动数据

> NFS Server 操作

    mount 202.38.93.94:/nfsroot /mnt/
    cp -a /mnt/ubuntu64/ /SSD/ubuntu64/
    cp -a /mnt/ubuntu32/ /SSD/ubuntu32/
