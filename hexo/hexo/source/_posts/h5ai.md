---
title: h5ai目录列表程序完整安装使用教程
tags:
  - h5ai
  - php
categories:
  - h5ai
abbrlink: 77a9
date: 2018-03-28 20:37:10
copyright: true
password: redhat
---

### 什么是 h5ai

H5ai是一款功能强大 php 文件目录列表程序，由德国开发者 Lars Jung 主导开发，它提供多种文件目录列表呈现方式，支持多种主流 Web 服务器，例如 Nginx、Apache、Cherokee、Lighttpd 等，支持多国语言，可以使用本程序在线预览文本、图片、音频、视频等。

> 请注意，默认情况下，放到目录下的 .php 文件将会被直接执行，并不以文本显示(本环境基于CentOS 7)



### 安装

**1. **PHP 运行环境 (apache+php/nginx+php)

**2. **[官网](https://larsjung.de/h5ai/); 下载 h5ai 安装包 [下载地址](https://release.larsjung.de/h5ai/h5ai-0.29.0.zip)

**3. **添加`/_h5ai/public/index.php`到默认索引文件列表的末尾

***3.1. *** Apache httpd 2.2 / 2.4在 httpd.conf 或在根目录的 .htaccess 文件中，例如：

    DirectoryIndex  index.html  index.php  /_h5ai/public/index.php

***3.2. ***lighttpd 1.4 配置文件：lighttpd.conf 例如：

    index-file.names += ("index.html", "index.php", "/_h5ai/public/index.php")

***3.3. ***nginx 1.2：配置文件：nginx.conf 例如：

    index  index.html  index.php  /_h5ai/public/index.php;

***3.4. ***Cherokee 1.2: 配置文件：cherokee.conf 例如：

    vserver!1!directory_index = index.html,index.php,/_h5ai/public/index.php

**4. **去除被禁用的 PHP 函数：(然后重启服务)
> 编辑`/usr/local/php/etc/php.ini` 删除 `disable_functions` 中被禁用的函数 `scandir、exec、passthru`

**5. **目录结构

> 需要注意的是，在文件目录下，如果有 index.html,index.php 可能会优先显示

    wwwroot
    ├── directory1
    │   ├── file1
    │   ├── file2
    │   └── file3
    ├── directory2
    │   ├── file1
    │   ├── file2
    │   └── file3
    └── _h5ai
        ├── CHANGELOG.md
        ├── private
        │   ├── cache
        │   │   └── README.md
        │   ├── conf
        │   │   ├── l10n
        │   │   │   ├── af.json
        │   │   │   ├── bg.json
        │   │   │   ├── cs.json
    .... ... ... ... ........

### 配置功能

> 到目前为止，h5ai 可以正常使用了，但是我们可以开启 _h5ai 更多功能。通过 http(s)://your_domain/_h5ai/public/index.php 可以查看 _h5ai 的功能开启情况，默认密码是空的。

**1. **编译安装 FFmpeg

> [最新版本下载](http://www.ffmpeg.org/releases/?C=M;O=D)

    wget http://www.ffmpeg.org/releases/ffmpeg-3.0.11.tar.gz
    tar -xvf ffmpeg-3.0.11.tar.gz
    cd ffmpeg-3.0.11
    ./configure --prefix='/usr/local/ffmpeg' && make && make install


**2. **编译安装 libav

> [最新版本下载](https://libav.org/releases/)

    wget https://libav.org/releases/libav-12.3.tar.gz
    tar -xvf libav-12.3.tar.gz
    cd libav-12.3
    ./configure --prefix='/usr/local/libav' && make && make install

**3. **略缩图功能

> 将 _h5ai 中，private 与 public 文件夹中的 cache 目录设置权限为 755

    chmod 755 /wwwroot/_h5ai/private _h5ai/public/cache -R

> PDF 略缩图

    yum install ImageMagick -y

### options.json 中的更多功能

> 位于 _h5ai/private/conf 目录下

**1. **打包下载：

    搜索 “download”
    127 行，enabled 由 false 改为 true。

**2. **文件信息及二维码：

    搜索 “info”
    185 行，enabled 由 false 改为 true。

**3. **默认简体中文：

    搜索 “l10n”
    202 行，enabled 由 false 改为 true。

**4. **文件及文件夹多选：

    搜索 “select”
    323 行，enabled 由 false 改为 true。

**4. **开启全局搜索：

    搜索 “search”
    307 行，enabled 由 false 改为 true。

**5. **默认密码：

> 首先生成自定义 sha512 密码：[在线生成 sha512 密码](http://www.ttmd5.com/hash.php?type=A)

    然后搜索 “passhash”
    10 行，将其密码改成自己生成的。

###  在目录头部或尾部显示 HTML 内容

> 在需要显示自定义 HTML 的目录下，添加 _h5ai.headers.html 和/或 _h5ai.footers.html。这个通常用于对该目录的一些说明。支持 HTML 标签。

**1. **头部示例：(文件名：_h5ai.header.html)

    <div id="content-header">
        <h1 style="text-align: center">这是一个标题消息</h1>
        <p style="text-align: center">头文件是从文件中读取的_h5ai.header.html这个文件的内容将被<code>div</code>标签包围</p>
    </div>


**1. **尾部示例：(文件名：__h5ai.footer.html)

    <div id="content-footer">
        <p style="text-align: center">页脚从文件中读取<code>_h5ai.footer.html</code>这个文件的内容将被<code>div</code>标签包围。</p>
        <p style="text-align: center"><strong>注意：</strong><code>_h5ai*</code> 默认情况下，名称的所有文件和目录都隐藏在索引中。</p>
    </div>

[本文参考内容](https://www.htcp.net/3643.html)
