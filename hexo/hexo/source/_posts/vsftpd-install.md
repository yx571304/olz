title: Vsftpd 安装配置
author: 亦 漩
abbrlink: 2eed
tags:
  - linux
  - Vsftpd
categories:
  - Vsftpd
date: 2018-06-14 20:42:00
---
## Vsftpd 安装

### 安装说明

#### 协议模式：
    
    主动模式：FTP服务器主动向客户端发起连接请求。
        
    被动模式：FTP服务器等待客户端发起连接请求（FTP的默认工作模式）。
    
#### 认证模式：
    
    匿名开放模式：是一种最不安全的认证模式，任何人都可以无需密码验证而直接登录到FTP服务器。

    本地用户模式：是通过Linux系统本地的账户密码信息进行认证的模式，相较于匿名开放模式更安全，而且配置起来也很简单。但是如果被黑客破解了账户的信息，就可以畅通无阻地登录FTP服务器，从而完全控制整台服务器。

    虚拟用户模式：是这三种模式中最安全的一种认证模式，它需要为FTP服务单独建立用户数据库文件，虚拟出用来进行口令验证的账户信息，而这些账户信息在服务器系统中实际上是不存在的，仅供FTP服务程序进行认证使用。这样，即使黑客破解了账户信息也无法登录服务器，从而有效降低了破坏范围和影响。

#### 安装参数说明
    
    # vsftpd参数详解
    # 参数	               作用
    # listen=[YES|NO]	是否以独立运行的方式监听服务
    # listen_address=IP地址	设置要监听的IP地址
    # listen_port=21	设置FTP服务的监听端口
    # download_enable＝[YES|NO]	是否允许下载文件
    # userlist_enable=[YES|NO]
    # userlist_deny=[YES|NO]	设置用户列表为“允许”还是“禁止”操作
    # max_clients=0	最大客户端连接数，0为不限制
    # max_per_ip=0	同一IP地址的最大连接数，0为不限制
    # anonymous_enable=[YES|NO]	是否允许匿名用户访问
    # anon_upload_enable=[YES|NO]	是否允许匿名用户上传文件
    # anon_umask=022	匿名用户上传文件的umask值
    # anon_root=/var/ftp	匿名用户的FTP根目录
    # anon_mkdir_write_enable=[YES|NO]	是否允许匿名用户创建目录
    # anon_other_write_enable=[YES|NO]	是否开放匿名用户的其他写入权限（包括重命名、删除等操作权限）
    # anon_max_rate=0	匿名用户的最大传输速率（字节/秒），0为不限制
    # local_enable=[YES|NO]	是否允许本地用户登录FTP
    # local_umask=022	本地用户上传文件的umask值
    # local_root=/var/ftp	本地用户的FTP根目录
    # chroot_local_user=[YES|NO]	是否将用户权限禁锢在FTP目录，以确保安全
    # local_max_rate=0	本地用户最大传输速率（字节/秒），0为不限制

### 安装配置

#### 安装

    # 安装vsftpd, ftp
    yum -y install vsftpd ftp
    
    # 备份默认配置文件
    mv /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.conf_bak

#### 配置匿名访问模式
    
##### 匿名访问模式参数

    # 参数	               作用
    # anonymous_enable=YES	      允许匿名访问模式
    # anon_umask=022           匿名用户上传文件的umask值
    # anon_upload_enable=YES	   允许匿名用户上传文件
    # anon_mkdir_write_enable=YES	允许匿名用户创建目录
    # anon_other_write_enable=YES	允许匿名用户修改目录名称或删除目录
    
    
##### vsftpd 匿名访问模式配置文件

    cat <<'EOF' >/etc/vsftpd/vsftpd.conf
    anonymous_enable=YES
    anon_umask=022
    anon_upload_enable=YES
    anon_mkdir_write_enable=YES
    anon_other_write_enable=YES
    local_enable=YES
    write_enable=YES
    local_umask=022
    dirmessage_enable=YES
    xferlog_enable=YES
    connect_from_port_20=YES
    xferlog_std_format=YES
    listen=NO
    listen_ipv6=YES
    pam_service_name=vsftpd
    userlist_enable=YES
    tcp_wrappers=YES
    EOF
    
##### 修改数据目录权限

    ls -ld /var/ftp/pub
    chown -Rf ftp /var/ftp/pub
    ls -ld /var/ftp/pub
    
##### 启动服务, 添加到开机启动

    systemctl restart vsftpd
    systemctl enable vsftpd

##### 验证安装

    # 连接用户：anonymous 密码为空
    # 匿名访问模式默认访问的是:：/var/ftp 目录
    ftp 192.168.2.18
    cd pub
    mkdir files

#### 配置本地用户模式
    
##### 本地用户模式参数

    # 参数               作用
    # anonymous_enable=NO     禁止匿名访问模式
    # local_enable=YES       允许本地用户模式
    # write_enable=YES       设置可写权限
    # local_umask=022        本地用户模式创建文件的umask值
    # userlist_deny=YES       启用“禁止用户名单”，名单文件为ftpusers和user_list
    # userlist_enable=YES      开启用户作用名单文件功能
    
##### vsftpd 本地用户模式配置文件
    cat <<'EOF' >/etc/vsftpd/vsftpd.conf
    anonymous_enable=NO
    local_enable=YES
    write_enable=YES
    local_umask=022
    dirmessage_enable=YES
    xferlog_enable=YES
    connect_from_port_20=YES
    xferlog_std_format=YES
    listen=NO
    listen_ipv6=YES
    pam_service_name=vsftpd
    userlist_enable=YES
    tcp_wrappers=YES
    EOF
    
##### 启动服务, 添加到开机启动

    systemctl restart vsftpd
    systemctl enable vsftpd
    
##### 禁止登陆名单

> 为了保证服务器的安全性默认禁止了root

    cat /etc/vsftpd/user_list    # 禁止登陆名单
    cat /etc/vsftpd/ftpusers     # 禁止登陆名单

##### 验证安装

> 连接用户：本地账户(注意账户名是否存在于禁止登陆名单中) 匿名访问模式默认访问的是该用户的家目录：/home/username or /root 目录

    ftp 192.168.2.18
    cd pub
    mkdir files

#### 配置虚拟用户模式

##### 创建认证文件

> 创建用于进行FTP认证的用户数据库文件，其中奇数行为账户名，偶数行为密码，创建测试用户：zhangsan，lisi  密码：redhat

    cat <<'EOF' >/etc/vsftpd/vuser.list
    zhangsan
    redhat
    lisi
    redhat
    EOF
    
##### 加密数据库文件, 设置权限

    db_load -T -t hash -f /etc/vsftpd/vuser.list /etc/vsftpd/vuser.db
    file vuser.db
    chmod 600 /etc/vsftpd/vuser.db
    rm -f /etc/vsftpd/vuser.list
    
##### 创建映射用户

> 创建vsftpd 服务程序用于存储文件的根目录以及虚拟用户映射的系统本地用户

    useradd -d /var/ftproot -s /sbin/nologin virtual
    chmod -Rf 755 /var/ftproot/
    ls -ld /var/ftproot/
    
##### 创建认证PAM文件

> 用于支持虚拟用户的PAM文件，新建一个用于虚拟用户认证的PAM文件vsftpd.vu，其中PAM文件内的“db=”参数为使用db_load命令生成的账户密码数据库文件的路径，不用写数据库文件的后缀：

    cat <<'EOF' >/etc/pam.d/vsftpd.vu
    auth      required     pam_userdb.so   db=/etc/vsftpd/vuser
    account    required     pam_userdb.so   db=/etc/vsftpd/vuser

##### PAM认证参数

    # 利用PAM文件进行认证时使用的参数以及作用
    # 参数	作用
    # anonymous_enable=NO	禁止匿名开放模式
    # local_enable=YES	允许本地用户模式
    # guest_enable=YES	开启虚拟用户模式
    # guest_username=virtual	指定虚拟用户账户
    # pam_service_name=vsftpd.vu	指定PAM文件
    # allow_writeable_chroot=YES	允许对禁锢的FTP根目录执行写入操作，而且不拒绝用户的登录请求
    
##### vsftpd 虚拟用户模式配置文件

    cat <<'EOF' >/etc/vsftpd/vsftpd.conf
    anonymous_enable=NO
    local_enable=YES
    guest_enable=YES
    guest_username=virtual
    allow_writeable_chroot=YES
    write_enable=YES
    local_umask=022
    dirmessage_enable=YES
    xferlog_enable=YES
    connect_from_port_20=YES
    xferlog_std_format=YES
    listen=NO
    listen_ipv6=YES
    pam_service_name=vsftpd.vu
    userlist_enable=YES
    tcp_wrappers=YES
    EOF

#### 为虚拟用户设置不同的权限

> 为虚拟用户设置不同的权限。虽然账户zhangsan和lisi都是用于vsftpd服务程序认证的虚拟账户，但是我们依然想对这两人进行区别对待。比如，允许张三上传、创建、修改、查看、删除文件，只允许李四查看文件。这可以通过vsftpd服务程序来实现。只需新建一个目录，在里面分别创建两个以zhangsan和lisi命名的文件，其中在名为zhangsan的文件中写入允许的相关权限
    
##### 创建数据目录

    mkdir /etc/vsftpd/vusers_dir/
    
##### 创建访问权限

    touch /etc/vsftpd/vusers_dir/lisi  /etc/vsftpd/vusers_dir/zhangsan
    
    # 配置张三权限
    cat <<'EOF' >/etc/vsftpd/vusers_dir/zhangsan
    anon_upload_enable=YES
    anon_mkdir_write_enable=YES
    anon_other_write_enable=YES
    EOF
    
    
##### vsftpd 虚拟用户多目录模式配置文件

> 添加user_config_dir参数来定义这两个虚拟用户不同权限的配置文件所存放的路径

    cat <<'EOF' >/etc/vsftpd/vsftpd.conf
    anonymous_enable=NO
    local_enable=YES
    guest_enable=YES
    guest_username=virtual
    allow_writeable_chroot=YES
    write_enable=YES
    local_umask=022
    dirmessage_enable=YES
    xferlog_enable=YES
    connect_from_port_20=YES
    xferlog_std_format=YES
    listen=NO
    listen_ipv6=YES
    pam_service_name=vsftpd.vu
    userlist_enable=YES
    tcp_wrappers=YES
    user_config_dir=/etc/vsftpd/vusers_dir
    EOF
    
    # 启动服务, 添加到开机启动
    systemctl restart vsftpd
    systemctl enable vsftpd
    
##### 验证

> 此时，不但可以使用虚拟用户模式成功登录到FTP服务器，还可以分别使用账户zhangsan和lisi来检验他们的权限

    ftp 192.168.2.18
    cd pub
    mkdir files
