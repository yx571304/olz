title: CentOS 7-GitLab安装
tags:
  - linux
  - git
  - gitlab
categories:
  - gitlab
abbrlink: 89fe
date: 2018-03-14 19:03:26
---
### Omnibus软件包安装

#### 



> 配置必要的依赖关系

    yum install -y wget curl policycoreutils-python

> 下载RPM安装包

    wget https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/gitlab-ce-10.4.4-ce.0.el7.x86_64.rpm

> 安装

    yum install -y gitlab-ce-10.4.4-ce.0.el7.x86_64.rpm

> 初始化

    gitlab-ctl reconfigure

> 汉化

    # 获取当前版本
    gitlab_version=$(sudo cat /opt/gitlab/embedded/service/gitlab-rails/VERSION)

    # 克隆汉化版本库
    git clone https://gitlab.com/xhang/gitlab.git
    # 如果已经克隆过，则进行更新
    cd gitlab
    git fetch
    
    # 生成版本的汉化补丁
    git diff v${gitlab_version} v${gitlab_version}-zh > ../${gitlab_version}-zh.diff
    cd ../

    # 导入汉化补丁
    gitlab-ctl stop
    yum -y install patch
    patch -d /opt/gitlab/embedded/service/gitlab-rails -p1 < ${gitlab_version}-zh.diff
    gitlab-ctl start

> 配置邮件

    gitlab_rails['smtp_enable'] = true 
    gitlab_rails['smtp_address'] = "onewserverdc.onewtech.com"
    gitlab_rails['smtp_port'] = 25 
    gitlab_rails['smtp_user_name'] = "gitlab@onewtech.com"
    gitlab_rails['smtp_password'] = "passwd"
    gitlab_rails['smtp_domain'] = "onewtech.com"
    gitlab_rails['smtp_authentication'] = "login"
    gitlab_rails['smtp_enable_starttls_auto'] = true
    gitlab_rails['gitlab_email_from'] = "gitlab@onewtech.com"
    user["git_user_email"] = "gitlab@onewtech.com"


    # 命令参考
    sed -i "423 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "424 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "424 s/smtp.server/onewserver.onewtech.com/g" /etc/gitlab/gitlab.rb
    sed -i "425 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "425 s/465/25/g" /etc/gitlab/gitlab.rb
    sed -i "426 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "426 s/smtp user/gitlab@onewtech.com/g" /etc/gitlab/gitlab.rb
    sed -i "427 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "427 s/smtp password/******/g" /etc/gitlab/gitlab.rb
    sed -i "428 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "428 s/example.com/onewtech.com/g" /etc/gitlab/gitlab.rb
    sed -i "429 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "430 s/^# gitlab/gitlab/g" /etc/gitlab/gitlab.rb
    sed -i "430a gitlab_rails['gitlab_email_from'] = \"gitlab@onewtech.com\"" /etc/gitlab/gitlab.rb
    sed -i '431a user["git_user_email"] = \"gitlab@onewtech.com\"' /etc/gitlab/gitlab.rb

    gitlab-ctl stop
    gitlab-ctl status
    gitlab-ctl reconfigure
    gitlab-ctl start

> 修改仓库路径

    git_data_dirs({ "default" => { "path" => "/data/gitlab-data" } })

> 管理命令

    gitlab-ctl stop       # 停止服务
    gitlab-ctl start      # 启动服务
    gitlab-ctl status     # 查看服务状态
    gitlab-ctl reconfigure    # 根据/etc/gitlab/gitlab.rd文件初始化

### 编译安装