title: 软件收藏
tags:
  - null
  - software
copyright: true
categories:
  - software
abbrlink: bd77
date: 2018-07-04 17:17:43
---
### U盘写入镜像工具

    Etcher
        支持系统：Windows, Linux, macOS
        官网地址：https://etcher.io
        软件下载源在 GitHub 下载速度慢

---

### 记事本

    NOTEPAD2
        支持系统：Windows
        官网地址：https://notepad2.com
        官方下载有点坑, 建议第三方站点下载

---

### Win系统优化工具

    Dism++
        支持系统：Windows
        官网地址：https://www.chuyu.me/zh-Hans/index.html
        功能：系统优化, 备份, 恢复, 更新, 清理, ESD转ISO

---

### Win系统封装工具

    Easy Sysprep
        支持系统：Windows [XP(x86), 7(x86/x64), 8(x86/x64), 8.1(x86/x64), 10(x86/x64)]
        官网地址：https://www.itsk.com/redirect.php?id=easysysprep
        功能：系统封装 将机械重复的系统安装流程化繁为简。ES很大程度上缩减了系统部署时间，加强了系统部署效率，节约了人力与时间成本

    万能驱动
        支持系统：Windows 7/10 (注意下载对应版本)
        官网地址：https://www.itsk.com/redirect.php?id=easydrv
        功能：一键安装系统驱动

    优启通
        官网地址：https://www.itsk.com/redirect.php?id=easyu
        功能：U盘启动

    Easy Image X
        支持系统：PE环境
        官网地址：https://www.itsk.com/redirect.php?id=easyimagex
        功能：统映像工具, 备份系统, 映像恢复, 驱动调用等功能, 用于系统安装

    SkyIAR
        支持系统：Windows
        官网地址：
        功能：整合大量IDE/AHCI/RAID驱动同时, SkyIAR还可以有效解决'换硬件不换系统', 'IDE改AHCI'等与磁盘控制器工作模式息息相关的问题.
        SkyIAR具备'自动检测', '自动筛选', '一键完成'等特性

    GoRuntime
        支持系统：Windows
        官网地址：https://www.itsk.com/redirect.php?id=goruntime
        功能：安装系统常用运行库的工具。多种运行库一键安装，也可使用/S参数自动安装

---

### SSH工具

    Xshell/Xftp
        开源版下载地址：https://www.netsarang.com/download/free_license.html
        功能：TELNET/SSH客户端 FTP/SFTP客户端

---

### Win系统下载

    Win7
        站点：http://www.newxitong.com
        特点：已封装好的镜像, 还原到系统盘即可自动开始安装系统,驱动

    ISO镜像下载
        站点：https://msdn.itellyou.cn
        特点：微软产品镜像下载, 系统, 软件
        下载地址为 ed2k 需要用迅雷下载或离线到百度网盘


---

### 下载

    WebUI-Aria2
        站点：https://github.com/ziahamza/webui-aria2
        
    Aria2
        站点：http://aria2c.com/usage.html
    
    Pan Download
        说明：百度网盘资源下载
        站点：http://pandownload.com


### 查看Docker镜像的介绍

    php
        https://hub.docker.com/_/php

    nginx
        https://hub.docker.com/_/nginx

    vimagick/aria2
        https://hub.docker.com/r/vimagick/aria2


### PHP

    X 探针
        介绍：https://inn-studio.com/prober/
        下载链接：https://api.inn-studio.com/download?id=xprober
    
    壁纸
        站点：https://picsum.photos
    
    h5ai
        站点：https://larsjung.de/h5ai/


### Chrome插件

    Charset
        功能：更改页面字符编码
        安装方法：https://jingyan.baidu.com/article/b0b63dbf0590db4a483070ee.html
        下载地址：http://pan.baidu.com/s/1c13eOe8
    
    BaiduExporter
        功能：下载百度网盘资源
        下载地址：https://github.com/acgotaku/BaiduExporter/releases


### MyCoding

    Kubernetes
        https://git.coding.net/yx571304/kubernetes.git

    Docker
        https://git.coding.net/yx571304/Docker.git