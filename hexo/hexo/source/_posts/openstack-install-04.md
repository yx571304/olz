title: OpenStack集群安装-04_RabbitMQ-cluster集群
tags:
  - openstack
  - rabbitmq
copyright: true
categories:
  - openstack
abbrlink: openstack-04
date: 2018-07-20 05:45:00
---
### 安装配置RabbitMQ

#### 安装软件

    for HOST in controller{1..3}; do
        echo "--------------- $HOST ---------------"
        ssh -T $HOST <<'EOF'
        # 安装 rabbitmq
        yum -y install rabbitmq-server erlang socat
        
        # 跟随系统启动
        systemctl enable rabbitmq-server
        
        # 启用web插件(端口15672)
        rabbitmq-plugins enable rabbitmq_management
    EOF
    done

#### controller1启动服务

    systemctl start rabbitmq-server

#### 同步认证 Cookie 文件到其他节点

    scp /var/lib/rabbitmq/.erlang.cookie controller2:/var/lib/rabbitmq/
    scp /var/lib/rabbitmq/.erlang.cookie controller3:/var/lib/rabbitmq/


#### controller2,3启动服务

    ssh controller2 "systemctl restart rabbitmq-server"
    ssh controller3 "systemctl restart rabbitmq-server"

### 配置集群

#### 使用Disk模式

    systemctl stop rabbitmq-server
    pkill beam.smp
    rabbitmqctl stop
    rabbitmq-server -detached 
    rabbitmqctl cluster_status     #查看状态

#### 加入节点controller1

    for HOST in  controller{1..3}; do
        ssh -T $HOST <<EOF
        systemctl stop rabbitmq-server
        pkill beam.smp
        rabbitmq-server -detached 
        rabbitmqctl stop_app
        rabbitmqctl join_cluster rabbit@controller1
        rabbitmqctl start_app
        rabbitmqctl cluster_status
        sleep 2
    EOF
    done

    rabbitmqctl set_policy ha-all "^" '{"ha-mode":"all"}'  # 设置镜像队列
    rabbitmqctl set_cluster_name RabbitMQ-Cluster          # 更改群集名称
    rabbitmqctl cluster_status                             # 查看群集状态

#### 验证

    # 查看群集状态(验证有3台节点)
    rabbitmqctl cluster_status
    
    #[root@controller1 ~]# rabbitmqctl cluster_status               
    #Cluster status of node rabbit@controller1 ...
    #[{nodes,[{disc,    [rabbit@controller1,rabbit@controller2,rabbit@controller3]}]},
    # {running_nodes,[rabbit@controller3,rabbit@controller2,rabbit@controller1]},
    # {cluster_name,<<"RabbitMQ-Cluster">>},
    # {partitions,[]},
    # {alarms,[{rabbit@controller3,[]},
    #          {rabbit@controller2,[]},
    #          {rabbit@controller1,[]}]}]

#### 创建RabbitMQ用户及密码

    rabbitmqctl add_user admin admin
    rabbitmqctl set_user_tags admin administrator
    rabbitmqctl add_user openstack openstack
    rabbitmqctl set_permissions openstack ".*" ".*" ".*"
    rabbitmqctl set_user_tags openstack administrator

### Haproxy 配置

#### 添加 RabbitMQ 代理

    cat <<EOF >>/etc/haproxy/haproxy.cfg
    
    ########## RabbitMQ ##########
    listen RabbitMQ-Server
      bind controller:5673
      mode tcp
      balance roundrobin
      option tcpka
      timeout client 3h
      timeout server 3h
      option clitcpka
      server controller1 controller1:5672 check inter 5s rise 2 fall 3
      server controller2 controller2:5672 check inter 5s rise 2 fall 3
      server controller3 controller3:5672 check inter 5s rise 2 fall 3

    listen RabbitMQ-Web
      bind controller:15673
      mode tcp
      balance roundrobin
      option tcpka
      server controller1 controller1:15672 check inter 5s rise 2 fall 3
      server controller2 controller2:15672 check inter 5s rise 2 fall 3
      server controller3 controller3:15672 check inter 5s rise 2 fall 3
    EOF

#### 同步 haproxy 配置文件

    scp /etc/haproxy/haproxy.cfg controller2:/etc/haproxy/haproxy.cfg
    scp /etc/haproxy/haproxy.cfg controller3:/etc/haproxy/haproxy.cfg


#### 重启服务

    systemctl restart haproxy
    ssh controller2 "systemctl restart haproxy"
    ssh controller3 "systemctl restart haproxy"

### 验证

> 浏览器打开 http://192.168.0.11:1080/admin 查看RabbitMQ-Server,RabbitMQ-Web 状态

>浏览器打开 http://10.0.0.10:15673 登录WebUI管理页面,查看服务状态 用户名:密码 / admin:admin

>命令行执行查看集群节点：
rabbitmqctl cluster_status


### 使用脚本

    site='http://home.onlycloud.xin'
    wget $site/code/openstack-04_rabbitmq-cluster.sh -O openstack-rabbitmq.sh
    sh openstack-rabbitmq.sh
