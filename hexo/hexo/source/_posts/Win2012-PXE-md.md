title: Windows 网络安装系统
tags:
  - PXE
  - Windows 2012
copyright: true
categories:
  - PXE
abbrlink: '5304'
date: 2018-05-30 13:34:00
---
## Win2012网络安装操作系统

#### 虚拟机安装 Windows Server 2012

> 软件版本：VMware Workstation 14.1.1
> 系统：[Windows Server 2012 R2 VL with Update (x64) - DVD (Chinese-Simplified)](https://msdn.itellyou.cn/)
    
    :: 自定义方式创建一个 Windows Server 2012 虚拟机
        :: 网络类型：桥接
        :: 磁盘大小：100G
        :: 自定义硬件：移除打印机, 声卡, USB控制器
        :: CD/DVD：使用ISO映像文件 下载好的[cn_windows_server_2012_r2_vl_with_update_x64_dvd_6052729.iso]
        :: 选择(带有 GUI 的服务器)
        :: 自定义安装, 点击 新建输入大小 31076 剩下的空间新建另一个分区 点击 [驱动器 0 分区 3] 点击 格式化 然后点击 [驱动器 0 分区 2] 下一步
        :: 初始化系统后 右键点击 任务栏右下角 网络图标 → 网络和共享中心 → Windows防火墙 →  启用或关闭Windows防火墙 关闭 公用, 专用网络防火墙
        :: 查看当前IP 地址并配置一个固定IP(与自动获取IP同一网段) IP: 192.168.0.30
        :: 更改计算机名：点击 状态栏 开始 → 这台电脑(右键) → 属性 → 高级系统设置 → 计算机名 → 更改 输入 Install 作为计算机名 (后面安装系统验证时需要使用到) 重启
        
     
#### 安装配置 DHCP

    :: 点击 状态栏 服务器管理器 → 管理 → 添加角色和功能 勾选 DHCP
    :: 点击 仪表盘上 管理 左侧 通知 → 完成DHCP配置 → 提交
    :: 点击 仪表盘上 工具 → DHCP → Install → IPv4(右键) → 新建作用域
        :: 输入名称：192.168.0.0
        :: 起始IP：192.168.0.31    # 需要注意如果局域网有开启DHCP功能请将192.168.0.30-192.168.0.40 从DHCP地址池排除
        :: 结束IP: 192.168.0.40

#### 安装系统部署

    :: 点击 状态栏 服务器管理器 → 添加角色和功能 勾选 Windows部署服务
    :: 点击 仪表盘上 工具 → Windows部署服务 点击 服务器 → Install(右键) → 配置服务 选择：独立服务器  远程安装文件夹路径：D:\RemoteInstall 勾选：响应所有客户端计算机

#### 配置Win10镜像

    :: 点击当前虚拟机 设置 → CD/DVD → 使用 ISO映像文件 → 浏览 选择 Win10 系统镜像
    :: 点击 仪表盘上 工具 → Windows部署服务 → 服务器 → Install → 安装映像(右键) → 添加安装映像 创建已命名的映像组：Windows 10 1803 文件位置：E:\sources\install.wim
    :: 点击 启动映像(右键) → 添加启动映像 文件位置：E:\sources\boot.wim 映像名称：Microsoft Windows 10 1803 Setup (x64)
    :: 点击 服务器 → Install(右键) → 所有任务 → 启动
    :: 点击 服务器 → Install(右键) → 属性 → 启动 配置PXE启动策略 (此处默认未做修改)
    
#### 虚拟机测试

    :: 自定义方式创建一个 Windows 7 x64 虚拟机    网络类型：桥接
    :: 开启虚拟机 开启后根据开机提示 按F12 (如果错过重启虚拟机)
    :: 连接到 Install  用户名：Install\administrator  密码：(Install服务器登录密码)

#### 封装系统

> 下载一个纯净系统镜像 安装当前系统补丁, 安装办公软件, 常用软件 使用封装软件进行封装, 调用万能驱动自动安装驱动程序

##### 创建Win10虚拟机

    :: 自定义方式创建一个 Windows 7 X64 虚拟机
        :: 网络类型：桥接
        :: 磁盘大小：100G
        :: 自定义硬件：移除打印机, 声卡, USB控制器
        :: CD/DVD：使用ISO映像文件 下载好的[cn_windows_10_business_editions_version_1803_updated_march_2018_x64_dvd_12063730]
        :: 点击 菜单栏 右侧 三角形向下按钮 选择 打开电源时进入固件 使用键盘方向键 选择 Advanced → T/O Device Configuration 回车 把所有选项全部设置为 Disabled 然后按 ESC
        :: 选择 Boot 将光标移动至 CD-ROM Drive 然后按+ 将它移动到第一行 按 F10 → 回车
        :: 选择(Windows 10 专业版)
        :: 自定义安装, 点击 新建输入大小 31275 剩下的空间新建另一个分区 点击 [驱动器 0 分区 3] 点击 格式化 然后点击 [驱动器 0 分区 2] 下一步
        :: 到了区域设置(注意到了此处请不要做任何操作)按下 Ctrl + Shift + F3 自动重启后进入 Sysprep 阶段 进入系统后关闭 系统准备工具(不要点确认 !)
        :: 关闭虚拟机

##### 准备软件

> [封装部署工具](https://www.itsk.com/topic-software.html)
> [U盘启动工具](https://www.itsk.com/thread-387418-1-1.html)
> [万能驱动](https://www.itsk.com/thread-387419-1-1.html)
> [系统运行库](https://www.itsk.com/thread-387375-1-1.html)
> [办公软件](https://msdn.itellyou.cn/), 常用软件, 激活软件
> [Dism++](https://www.chuyu.me/zh-Hans/index.html)

    :: 制作U盘启动镜像 解压优启通 运行 EasyU_v3.3.exe 点击生成 ISO 浏览保存位置到桌面 EasyU_v3.3.iso
    :: 开启虚拟机 配置IP 使虚拟机能上外网 或者改NAT模式(由于Install 服务器开启了PXE 没有指定DHCP 地址池的网关及 DNS 导致本机无网络)
    :: 将准备好的软件放到一个文件内,并开启文件共享, 虚拟机内访问文件共享 将文件下载到D 盘

##### 封装系统
    :: 激活系统
    :: 系统更新补丁 点击 开始 → 设置 → 更新和安全 → 检测更新  等待安装完毕 点击开始 更新并重启
    :: 安装系统运行库(解压四个文件后依次安装), 再次更新系统补丁 更新并重启
    :: 安装 office 更新并重启
    :: 使用Dism++ 优化系统, 安装常用软件, 配置系统默认软件, 软件去广告, 软件的默认设置
    :: 关闭虚拟机 设置 CD/DVD：使用ISO映像文件 选择 桌面的 EasyU_v3.3.iso
    :: 运行 EasySysprep
        :: 网络 域：tset.com(有域环境可以选择这个)  输入有权限加入域的 用户名和密码 勾选跳过无线配置 虚拟机自动重启 注意系统开机后立刻按任意键进入ISO 启动
        :: 选择 Windows 10 PE x64
        :: 进入 PE 系统后 运行EasySysprep 在部署设置 项 勾选 使用部署插件 网络设置 模式：DHCP 网络环境  勾选 域：tset.com(有域环境可以选择这个)  输入有权限加入域的 用户名和密码
        :: 网络位置：工作网络 将下载的万能驱动移动到C:\Sysprep\ 更名为EasyDrv7_Win10.x64 在专用调用接口处 勾选万能驱动助理 选择路径 C:\Sysprep\EasyDrv7_Win10.x64\EasyDrv7(Win10.x64).exe
        :: 待 EasySysprep 运行完毕自动退出后 点击桌面 EIX系统安装 点击分区备份 点击 点此新建映像文件 选择 D 盘 输入文件名 Win10-pro-1803.wim 点击保存 点击本地磁盘C  一键备份
        :: 备份完成后 重启等待系统安装完毕 将D 盘映像文件复制出来 这个就是封装好的映像 当前安装好的虚拟机就是映像安装的结果

#### 自定义映像网络安装

    :: 将 Windows 10镜像文件中 sources\boot.wim 和制作的映像文件 Win10-pro-1803.wim 复制到 install服务器的D 盘
    :: 点击 服务器管理器 仪表盘上 工具 → Windows部署服务 → 服务器 → Install → 安装映像(右键) → 添加安装映像 创建已命名的映像组：Windows 10 1803 V2 文件位置：E:\sources\install.wim
    :: 点击 启动映像(右键) → 添加启动映像 文件位置：E:\sources\boot.wim 映像名称：Microsoft Windows 10 1803 V2 Setup (x64)
    :: 新建一个 Windows 7 x64 虚拟机 PXE启动后即可看到新添加的映像 选择新添加的映像 在安装的过程中会自动调用 驱动精灵
