---
title: OpenStack Pike版安装部署汇总
tags:
  - openstack
copyright: true
categories:
  - openstack
abbrlink: openstack-readme
date: 2018-07-31 19:12:13
---


### 环境

    # 系统环境 CentOS-7.4 最小化安装(CentOS-7-x86_64-Minimal-1708.iso)
    # controller1, controller2, controller3 为控制节点(高可用,负载均衡)
    # nfs 主机本地磁盘提供给 glance 镜像存储
    # cinder1 主机本地磁盘 nfs,lvm 提给 openstac 卷
    # computer1 提供计算服务

<style>
/* 第一列表格宽度 */
table th:nth-of-type(1){
width: 26%;

}
/* 第二列表格宽度 */
table th:nth-of-type(2){
width: 74%;
}
</style>

### 安装说明

|                                     目录                                        |     服务                                                                                        |
|:--------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------|
|00[虚拟机初始化](http://home.onlycloud.xin/posts/openstack-00.html)              | 所有主机：虚拟机创建, 网络参数配置, 更改主机名                                                  |
|01[系统环境初始化](http://home.onlycloud.xin/posts/openstack-01.html)            | 所有主机：关闭 selinux, firewalld; 配置软件安装源, 时间同步, 常用工具                           |
|02[Haproxy集群高可用](http://home.onlycloud.xin/posts/openstack-02.html)         | controller1, controller2, controller3 安装 pacemaker, haproxy 集群高可用                        |
|03[MariaDB-Galera-Cluster集群](http://home.onlycloud.xin/posts/openstack-03.html)| controller1, controller2, controller3 安装 MariaDB Galera 集群高可用                            |
|04[RabbitMQ-cluster集群](http://home.onlycloud.xin/posts/openstack-04.html)      | controller1, controller2, controller3 安装 RabbitMQ 集群高可用                                  |
|05[Keystone验证服务群集](http://home.onlycloud.xin/posts/openstack-05.html)      | controller1, controller2, controller3 安装 Keystone 集群                                        |
|06[Glance镜像服务集群](http://home.onlycloud.xin/posts/openstack-06.html)        | controller1, controller2, controller3 安装 Glance-控制节点集群; nfs 提供 镜像存储               |
|07[Nova-控制节点集群](http://home.onlycloud.xin/posts/openstack-07.html)         | controller1, controller2, controller3 安装 Nova-控制节点集群                                    |
|08[Neutron控制节点集群](http://home.onlycloud.xin/posts/openstack-08.html)       | controller1, controller2, controller3 安装 Neutron控制节点集群                                  |
|09[dashboard集群](http://home.onlycloud.xin/posts/openstack-09.html)             | controller1, controller2, controller3 安装 dashboard集群                                        |
|10[cinder存储安装](http://home.onlycloud.xin/posts/openstack-10.html)            | controller1, controller2, controller3 安装 cinder存储控制节点集群; cinder 提供本地 lvm,nfs 存储 |
|11[nova计算节点安装](http://home.onlycloud.xin/posts/openstack-11.html)          | computer1, computer2 安装 nova计算节点                                                          |
|12[创建虚拟机](http://home.onlycloud.xin/posts/openstack-12.html)                | 任意有管理权限的节点(或使用 dashboard)                                                          |


