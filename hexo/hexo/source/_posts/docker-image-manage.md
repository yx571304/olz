---
title: Docker镜像容器管理
tags:
  - docker
  - linux
copyright: true
categories:
  - docker
abbrlink: 148d
date: 2018-05-17 17:55:19
---

### Docker 镜像管理

#### 搜索镜像

```sh
docker search <image>
    --no-trunc    # 显示完整镜像描述信息
    -s 40    # 仅列出收藏数不小于40的镜像
```

#### 下载镜像

```sh
docker pull <image>
```

#### 查看镜像

```sh
# 查看本地所有镜像
docker images -a 

# 查看镜像详细信息
docker images --digests
```

#### 删除镜像

```sh
docker rmi <image>
```

#### 镜像导出

```sh
docker save -o FileName.tar <image>
```

#### 镜像导入

```sh
docker load -i FileName.tar
```

### Docker 容器管理

#### 运行容器

```sh
# 启动一个守护态的容器
docker run -itd <image> /bin/bash

# 查看容器中运行的进程
docker top <image>
```

#### 常用参数

```sh
# 端口映射
docker run -it
    -p 80:8080    # 将容器中的8080端口映射到主机80端口
    -p 67:67/tcp    # 将容器中67端口udp映射到主机67端口
    -p 67-80/udp    # 映射一个端口范围

# 查看指定容器的端口映射
docker port <image>

# 映射文件目录
docker run -it
    -v   /home/docker:/data    # 将本地/home/docker映射到docker中/data目录

# 指定docker运行时的名称
docker run -it
    --name NAME    # 指定名称便于管理


# 容器的自启动
docker run -itd 
    --restart  
        no    # 容器退出时，不重启容器；
        on-failure    # 只有在非0状态退出时才从新启动容器；
        always    # 论退出状态是如何，都重启容器


# 运行中的容器添加自启动
docker update --restart=always <image>

# 运行一个容器
docker run -it -p 80:8080 -v /root/data:/data --name=docker1  /bin/bash
```

#### 容器管理

```sh
# 查看正在运行的容器
docker ps

# 查看全部的容器,包括运行中和已经停止的容器
docker ps -a

# 查看容器的详细信息
docker inspect <image>

# 进入容器
docker attach <image>

docker exec -it <image> /bin/bash

# 查看容器标准输出 
docker logs -f <image>
```

#### 容器保存为镜像

##### 保存容器为镜像

> 实际环境中不建议这样使用,此方法不便于后期维护,容易导致镜像臃肿

```sh
docker commit \
    --author "Tao Wang <twang2218@gmail.com>" \
    --message "修改了默认网页" \
    webserver \
    nginx:v2
```

##### 查看镜像内的历史记录

```sh
docker history <image>
```

> 其中 --author 是指定修改的作者，而 --message 则是记录本次修改的内容

#### 删除容器

```sh
# 只能删除已经停止的容器
docker rm <image>

# 强行删除运行中的容器
docker rm -f <image>
```

#### 容器导出

```sh
docker export <ID/image> > FileName.tar
```

#### 容器导入

```sh
cat FileName.tar | docker import - <image>:<version>
```

### 示例

#### 运行 Httpd

```sh
docker search httpd
docker pull httpd
docker run -itd  -p 80:80 --name httpd httpd /bin/sh -c "httpd -k start;/bin/bash"
```

#### 运行 Owncloud

```sh
docker search owncloud
docker pull owncloud
docker run -itd  -p 81:80 --name owncloud owncloud
```

#### 运行 Aria2

```sh
docker run -itd -p 6800:6800 -p 82:80 aria2 xujinkai/aria2-with-webui
```
