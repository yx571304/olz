
### shell 简单 httpd server

Fork: [liamjack/BashHTTPd](https://github.com/liamjack/BashHTTPd)

#### 用法:

    curl -sSL https://dwz.cn/JFWear6b -o httpd.sh
    chmod +x httpd.sh

    mkdir www
    echo '<h1>Test page</h1>' > www/test.html
    echo '<h1>Home page</h1>' > www/index.html
    echo '<h1>page 403 - Forbidden</h1>' > www/403.html
    echo '<h1>page 403 - Forbidden</h1>' > www/404.html


> Example

    # port 监听端口
    ./httpd.sh [port]

#### 测试:

> 1.服务端

    ./httpd.sh 9999

> 2.客户端

    curl http://服务端IP:9999
    curl http://服务端IP:9999/403.html
    curl http://服务端IP:9999/404.html
    curl http://服务端IP:9999/test.html
    curl http://服务端IP:9999/index.html

#### 安装依赖

> CentOS

    yum install -y socat

> debian / ubuntu

    apt-get install -y socat

#### 配置

- PROTO 可以使用的网络协议 (例如: UDP4 / UDP6 / TCP4 / TCP6)
- SERVERTOKEN ServerHTTP响应中发送的标头的内容
- HTTPVERSION HTTP响应中发送的HTTP版本
- WWWPATH 绝对路径到HTML文件存储的目录，末尾无斜杠 (例如: /tmp/www)
- FOUROFOURFILE 是路径到在网页404错误，相对于WWWPATH路径 (Example: /404.html)