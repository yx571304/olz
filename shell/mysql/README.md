# Xtrabackup备份MySql脚本

#### 介绍

Xtrabackup备份MySql脚本

**功能需求：**

- 1.每天备份一次周末做下周的全量备份, 周一到周六做周末的全量备份
- 2.每天备份一次周末做下周的全量备份, 周一到周六每天做上一天的增量备份
- 3.每天第一次备份为全量备份之后的备份为全量备份的增量备份
- 4.备份前检测全量备份是否存在 如果不存在自动创建全量备份
- 5.将数据备份到远程服务器

**脚本使用注意事项**

- 1.下载innobackup.sh脚本后更改脚本中root登陆密码
- 2.如果启用远程备份
   - 2.1.从本机到远程服务器的免密码登陆(密钥登陆)
   - 2.2.需要在两台主机分别安装rsync
   - 2.3.在远程主机创建备份目录

- 3.配置计划任务前请先在命令行测试
- 4.本地备份默认只保留 7 天 远程服务器默认保留 28 天 (不小于这个时间 每周六才执行历史备份清理)
- 5.查看历史定时任务备份输出信息 cat /var/spool/mail/root (需要后台执行过定时备份任务 例如计划任务)

#### 软件版本

    mysql-5.7
    percona-xtrabackup-24-2.4.15

#### 安装 MySql(如果已安装数据库请跳过)

> 下载脚本

    curl -sSL https://dwz.cn/gfcnHqGS -o install-mysql.sh
    chmod +x install-mysql.sh

> 安装 MySql

    ./install-mysql.sh --active install --version 5.7.23 --root-pass 456abc@DEF --data-dir /home/hadoop/mysql

    # --help                             # 查看帮助信息
    # --version 5.7.23                   # 指定mysql版本号为5.7.23
    # --root-pass 123abc@DEF             # 指定root用户密码
    # --data-dir /home/hadoop/mysql      # mysql数据库存储目录
    # --bin-log true                     # 启用二进制日志
    # --get-version, -V                  # 查看软件仓库软件版本

#### 安装 Xtrabackup

> 1.安装 xtrabackup

    curl -O http://dl.hc-yun.com/soft/percona-xtrabackup-24-2.4.15-1.el7.x86_64.rpm
    yum localinstall -y percona-xtrabackup-24-2.4.15-1.el7.x86_64.rpm

> 2.下载脚本

    mkdir /script && cd $_
    curl -sSL https://dwz.cn/pYZqbPiq -o send_mail.py
    curl -sSL https://dwz.cn/8oTDN52s -o innobackup.sh
    chmod +x innobackup.sh send_mail.py

> 3.替换数据库登陆参数

    # 替换password值为你的MySql数据库root用户密码
    password=456abc@DEF
    sed -i "s/--password=.*'/--password=\'$password\'/" innobackup.sh

#### 使用说明

    ./innobackup.sh [OPTION]

        --help, -h             # 查看帮助信息
        --data-dir, -d         # 数据备份存储目录
        --mode, -m {1|2|3}     # 备份模式 备份必选参数
          备份模式: 1.周日做全量备份 周一到周六每天做上周日的增量备份
          备份模式: 2.周日做全量备份 周一到周六每天做上一天的增量备份
          备份模式: 3.循环 每天第一次做全量备份之后的备份做基于当天的全量备份的增量备份

        --mail true                          # 备份失败邮件告警 启用: --mail treu 默认禁用
        --mail-addr example@example.com      # 邮件告警邮箱地址
          如果启用邮件告警: 请手动测试邮件告警脚本确定能正常发送邮件

        --remoute-bak true                      # 远程备份默认禁用
        --remoute-server root@192.168.0.71      # 远程服务器地址(需要配置本地到远程服务器的密钥登陆)
        --remoute-dir /home/data                # 远程服务器路径(需要在远程服务器手动创建)
          # 注意如果启用远程备份: 1.配置本机到远程服务器的秘钥登录 2.登录远程服务器创建备份路径

    示例(可将多条参数合并):
        # 1. 使用备份模式1备份路径为 /home/backup
        ./innobackup.sh --mode 1 --data-dir /home/backup

        # 2. 使用备份模式1使用脚本中默认备份路径(/home/backup)启用备份失败邮件告警
        ./innobackup.sh --mode 1 --mail true --mail-addr example@domain.com

        # 3. 使用备份模式1启用远程备份(备份完成后压缩并使用rsync将压缩文件上传到远程服务器指定目录)
        ./innobackup.sh --mode 1 --remoute-bak true --remoute-server root@192.168.0.71 --remoute-dir /home/backupmysql

#### 配置计划任务

> 需求1. 每天凌晨 1:10 分执行备份 备份策略: 周日做全量备份 周一到周六每天做上周日的增量备份 启用远程备份

    crontab -l > /tmp/crontab.tmp
    echo "10 1 * * * /script/innobackup.sh --mode 1 --remoute-bak true --remoute-server root@192.168.2.72 --remoute-dir /home/backupmysql" >> /tmp/crontab.tmp
    cat /tmp/crontab.tmp | uniq > /tmp/crontab
    crontab /tmp/crontab


> 需求2. 每天凌晨零点做昨天的全量备份 备份策略: 每半小时做基于全量备份的增量备份 启用远程备份

    crontab -l > /tmp/crontab.tmp
    echo "*/30 * * * * /script/innobackup.sh --mode 3 --remoute-bak true --remoute-server root@192.168.2.72 --remoute-dir /home/backupmysql" >> /tmp/crontab.tmp
    cat /tmp/crontab.tmp | uniq > /tmp/crontab
    crontab /tmp/crontab