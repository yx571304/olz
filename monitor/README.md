### prometheus+grafana+alertmanager

### Server端运行

> 创建数据目录 && 授权

    mkdir -p ./grafana/data ./alertmanager/data ./prometheus/data
    chown -Rf 472:472 ./grafana/data
    chown -Rf 65534:65534 ./alertmanager/data ./prometheus/data

> 启动

    docker-compose -f docker-compose.yml up -d

### 被监控端(docker)

    docker-compose -f node-exporter-cadvisor.yml up -d

### Dashboard

    # node-exporter: 8919 
