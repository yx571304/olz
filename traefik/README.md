### Traefik

#### swarm cluster test

> Run Traafik 

    docker stack deploy -c stack-traefik.yml test
    # Browser http://0.0.0.0:8080

> Run whoami

    docker stack deploy -c stack-whoami.yml test

> Test whoami

    curl -H Host:whoami.test.co http://127.0.0.1

> Run whoami-2

    docker stack deploy -c stack-whoami-2.yml test

> Test whoami-2

    curl -H Host:test.co http://127.0.0.1/test


https://docs.traefik.io/v1.7/user-guide/docker-and-lets-encrypt/
https://docs.traefik.io/v1.7/user-guide/grpc/
https://docs.traefik.io/v1.7/user-guide/cluster-docker-consul/#full-docker-compose-file_1
https://docs.traefik.io/v1.7/#the-official-docker-image